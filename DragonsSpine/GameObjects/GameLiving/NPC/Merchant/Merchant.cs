namespace DragonsSpine
{
    using System;
	using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Summary description for Merchant.
    /// </summary>
	public class Merchant : NPC
	{
		public const int MAX_STORE_INVENTORY = 12; // maximum store inventory amount

        public enum MerchantType
        {
            None,
            Pawn,
            Barkeep,
            Weapon,
            Armor,
            Apothecary,
            Book,
            Jewellery,
            General,
            Magic
        }

        public enum TrainerType
        { 
            None,
            Spell,
            Weapon,
            Martial_Arts,
            Knight,
            Sage,
            HP_Doctor
        }

        public enum InteractiveType
        {
            None,
            Banker,
            Tanner,
            Balm,
            Recall_Ring,
            Mugwort,
            Confessor
        }


		public ArrayList pendingQuests = new ArrayList();
		public ArrayList acceptedQuest = new ArrayList();
		// note on UW Quests:
		// <name>, TRAIN : requests quest or gets item in return for item
		// <name>, YES : starts quest
		// PET <name> : makes creature follow
		// <name>, FOLLOW : makes creature follow
		// <name>, CLIMB UP : makes creature climb up
		// <name>, CLIMB DOWN : makes creature climb down

        public Merchant(NPC npc) : base()
        {
            this.npcType = NPCType.Merchant;
            this.NPCIdent = npc.NPCIdent;
            this.Name = npc.Name;
            this.attackSound = npc.attackSound;
            this.deathSound = npc.deathSound;
            this.idleSound = npc.idleSound;
            this.npcID = npc.npcID;
            this.worldNpcID = World.GetNextWorldNpcID();
            this.merchantType = npc.merchantType;
            this.MerchantMarkup = npc.MerchantMarkup;
            this.baseArmorClass = npc.baseArmorClass;
            this.THAC0Adjustment = npc.THAC0Adjustment;
            this.trainerType = npc.trainerType;
            this.interactiveType = npc.interactiveType;
            this.MoveString = npc.MoveString;
            this.shortDesc = npc.shortDesc;
            this.longDesc = npc.longDesc;
            this.visualKey = npc.visualKey;
            this.special = npc.special;
            this.npcType = npc.npcType;
            this.aiType = npc.aiType;
            this.species = npc.species;
            this.BaseProfession = npc.BaseProfession;
            this.classFullName = npc.classFullName;
            this.Experience = npc.Experience;
            this.HitsMax = npc.HitsMax;
            this.Hits = npc.HitsMax;
            this.Alignment = npc.Alignment;
            this.StaminaMax = npc.StaminaMax;
            this.Stamina = npc.StaminaMax;
            this.ManaMax = npc.ManaMax;
            this.Mana = this.ManaMax;
            this.Strength = npc.Strength;
            this.Dexterity = npc.Dexterity;
            this.Intelligence = npc.Intelligence;
            this.Wisdom = npc.Wisdom;
            this.Constitution = npc.Constitution;
            this.Charisma = npc.Charisma;
            this.Speed = npc.Speed;

            #region Loot
            this.lootVeryCommonAmount = npc.lootVeryCommonAmount;
            this.lootVeryCommonArray = npc.lootVeryCommonArray;
            this.lootVeryCommonOdds = npc.lootVeryCommonOdds;
            this.lootCommonAmount = npc.lootCommonAmount;
            this.lootCommonArray = npc.lootCommonArray;
            this.lootCommonOdds = npc.lootCommonOdds;
            this.lootRareAmount = npc.lootRareAmount;
            this.lootRareArray = npc.lootRareArray;
            this.lootRareOdds = npc.lootRareOdds;
            this.lootVeryRareAmount = npc.lootVeryRareAmount;
            this.lootVeryRareArray = npc.lootVeryRareArray;
            this.lootVeryRareOdds = npc.lootVeryRareOdds;
            this.lootLairAmount = npc.lootLairAmount;
            this.lootLairArray = npc.lootLairArray;
            this.lootLairOdds = npc.lootLairOdds;
            this.lootAlwaysArray = npc.lootAlwaysArray;
            this.lootBeltAmount = npc.lootBeltAmount;
            this.lootBeltArray = npc.lootBeltArray;
            this.lootBeltOdds = npc.lootBeltOdds; 
            #endregion

            #region Spawn Items
            this.spawnArmor = npc.spawnArmor;
            this.spawnLeftHand = npc.spawnLeftHand;
            this.spawnLeftHandOdds = npc.spawnLeftHandOdds;
            this.spawnRightHand = npc.spawnRightHand; 
            #endregion

            #region Skills
            this.unarmed = npc.unarmed;
            this.mace = npc.mace;
            this.bow = npc.bow;
            this.dagger = npc.dagger;
            this.flail = npc.flail;
            this.rapier = npc.rapier;
            this.twoHanded = npc.twoHanded;
            this.staff = npc.staff;
            this.shuriken = npc.shuriken;
            this.sword = npc.sword;
            this.threestaff = npc.threestaff;
            this.halberd = npc.halberd;
            this.thievery = npc.thievery;
            this.magic = npc.magic;
            this.bash = npc.bash;
            #endregion

            this.animal = npc.animal;
            this.tanningResult = npc.tanningResult;
            this.IsUndead = npc.IsUndead;
            this.IsSpectral = npc.IsSpectral;
            this.poisonous = npc.poisonous;
            this.IsWaterDweller = npc.IsWaterDweller;
            this.IsMobile = npc.IsMobile;
            this.Gold = npc.Gold;
            this.Level = npc.Level;
            this.BaseProfession = npc.BaseProfession;
            this.gender = npc.gender;
            this.race = npc.race;
            this.Age = npc.Age;
            this.canCommand = npc.canCommand;

            this.patrolRoute = npc.patrolRoute;
            if (this.patrolRoute.Length > 0)
            {
                this.patrolKeys = new List<string>();
                string[] route = this.patrolRoute.Split(" ".ToCharArray());
                foreach (string nrt in route)
                {
                    this.patrolKeys.Add(nrt);
                }
            }

            this.lairCritter = npc.lairCritter;

            this.lairCells = npc.lairCells;

            this.spellList = npc.spellList;
            this.abjurationSpells = npc.abjurationSpells; // abjuration spells
            this.alterationSpells = npc.alterationSpells; // beneficial alteration spells
            this.alterationHarmfulSpells = npc.alterationHarmfulSpells; // harmful alteration spells
            this.conjurationSpells = npc.conjurationSpells; // conjuration spells
            this.divinationSpells = npc.divinationSpells; // divination spells
            this.evocationSpells = npc.evocationSpells; // evocation spells
            this.evocationAreaEffectSpells = npc.evocationAreaEffectSpells; // area effect evocation spells

            this.HasRandomName = npc.HasRandomName;
            this.castMode = npc.castMode;
            this.attackString1 = npc.attackString1;
            this.attackString2 = npc.attackString2;
            this.attackString3 = npc.attackString3;
            this.attackString4 = npc.attackString4;
            this.attackString5 = npc.attackString5;
            this.attackString6 = npc.attackString6;
            this.blockString1 = npc.blockString1;
            this.blockString2 = npc.blockString2;
            this.blockString3 = npc.blockString3;
            this.Alignment = npc.Alignment;

            this.immuneBlind = npc.immuneBlind;
            this.immuneCold = npc.immuneCold;
            this.immuneCurse = npc.immuneCurse;
            this.immuneDeath = npc.immuneDeath;
            this.immuneFear = npc.immuneFear;
            this.immuneFire = npc.immuneFire;
            this.immuneLightning = npc.immuneLightning;
            this.immunePoison = npc.immunePoison;
            this.immuneStun = npc.immuneStun;

            this.effectList = npc.effectList;
            this.questList = npc.questList;
            this.GroupAmount = npc.GroupAmount;
            this.GroupMembers = npc.GroupMembers;
            this.WeaponRequirement = npc.WeaponRequirement;
        }

		public override void doAI()
		{
            ArtificialIntel.CreateContactList(this);

			switch(this.interactiveType)
			{
				case InteractiveType.Balm:
					this.MerchantBalmSeller();
					break;
				case InteractiveType.Mugwort:
					this.MerchantMugwortSeller();
					break;
				case InteractiveType.Recall_Ring:
					this.MerchantRecallSeller();
					break;
				case InteractiveType.Tanner:
					this.MerchantTanner();
					break;
				default:
					break;
            }

            if (this.mostHated == null)
            {
                if (this.idleSound != "" && Rules.RollD(1, 100) < 8 )
                {
                    this.EmitSound(this.idleSound);
                }
            }
		}

		public void LengQueenIantaQuest()
		{
            //Quests.NPC_Quest(null,this,Quests.quest("Ianta","1905","20040",-1,0,0,
            //    "Please find my daughter, Ianta.",
            //    "Thank you finding my daughter, here is your reward.",
            //    "My poor daughter, please find her."));
//			if(this.cell.cellCharList.Count > 1)
//			{
//				foreach(Character ch in this.cell.cellCharList)
//				{
//					if(ch.npcID==1905)
//					{
//						Command.parseCommand(this,"/Thank you for finding my daughter! Here is your reward.","");
//						Item dp = Item.copyItemFromItemCatalog(20040);
//						this.cell.addItemToList(dp);
//						foreach(SpawnZoneLink szl in SpawnZoneLink.szlList) 
//						{
//							if(szl.SpawnZoneID==ch.myZone)
//							{
//								szl.NumInZone-=1;
//							}
//						}
//						this.cell.removeCharFromList(ch);
//						ch.removeFromWorld();
//						ch.removeFromLimbo();
//					}
//				}
//			}
//			if(Rules.dice.Next(100) < 15)
//			{
//				Command.parseCommand(this,"/My daughter is lost and needs to be found.","");
//			}
		}

		public void MerchantTanner()
		{
			Item coin = new Item();
			string name = this.Name;
			if(Rules.RollD(1, 100) < 10)
			{
				if(Rules.RollD(1, 10) > 5)
				{
					Command.ParseCommand(this,"say","Bring your kills to me for the finest leatherwork this side of Mu.");
				}
				else
				{
					Command.ParseCommand(this,"say","You kill 'em and I make 'em pretty. Impress your significant others with a new elk-hide vest!");
				}
			}
            try
            {
                if (this.CurrentCell.Items.Count > 0)
                {
                    List<Item> corpses = new List<Item>();
                    for (int a = 0; a < this.CurrentCell.Items.Count; a++)
                    {
                        Item cellItem = this.CurrentCell.Items[a];
                        if (cellItem.itemType == Globals.eItemType.Corpse && cellItem.itemID < 600000) // type corpse and itemID less than player's corpse itemID
                        {
                            if (cellItem.contentList.Count > 0)
                            {
                                for (int b = 0; b < cellItem.contentList.Count; b++) // tanner will rummage through corpses and drop items in the corpse contentList
                                {
                                    this.CurrentCell.Add(cellItem.contentList[b]);
                                } // end b loop
                            }
                            if (cellItem.vRandHigh == 66666 && cellItem.special.Length > 1)
                            {
                                string[] tanningList = cellItem.special.Split(" ".ToCharArray());
                                for (int b = 0; b < tanningList.Length; b++)
                                {
                                    Item tannedItem = Item.CopyItemFromDictionary(Convert.ToInt32(tanningList[b]));
                                    Utils.Log(this.GetLogString() + " tanned " + tannedItem.notes + " from " + cellItem.longDesc + ".", Utils.LogType.MerchantTanning);
                                    this.CurrentCell.Add(tannedItem);
                                } // end b loop 2
                                this.SendToAllInSight(this.Name + ": There you go!");
                            } // end if
                            corpses.Add(cellItem);
                        } // end if corpse
                    } // end a loop
                    if (corpses.Count > 0) // remove the corpses from this merchant's cellItemList
                    {
                        for (int a = 0; a < corpses.Count; a++)
                        {
                            this.CurrentCell.Remove(corpses[a]);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
		}

        public void MerchantRecallSeller()
        {
            Item coin = null; // coin found on the ground

            int rings = 0; // # of recall rings on the ground

            if (Rules.RollD(1, 100) < 10)
            {
                if (Rules.RollD(1, 100) > 50)
                {
                    Command.ParseCommand(this, "say", "Buy a recall ring, gets you out of trouble every time.");
                }
                else
                {
                    Command.ParseCommand(this, "say", "All I ask is 150 coins to defray my modest expenses.");
                }
            }

            foreach (Item item in this.CurrentCell.Items)
            {
                if (item.itemID == Item.ID_RECALLRING) { rings++; }

                if (item.itemType == Globals.eItemType.Coin)
                {
                    coin = item;
                }
            }

            if (coin != null && rings <= 9)
            {
                Item recallRing = Item.CopyItemFromDictionary(Item.ID_RECALLRING);

                if (coin.coinValue >= recallRing.coinValue)
                {
                    int available = (int)Math.Truncate(coin.coinValue / recallRing.coinValue);
                    do
                    {
                        recallRing = Item.CopyItemFromDictionary(Item.ID_RECALLRING); // copy recall ring from dictionary
                        coin.coinValue -= recallRing.coinValue; // subtract ring cost from coins
                        this.CurrentCell.Add(recallRing); // add balm to cell
                        available--; // subtract from available
                        rings++; // add to rings amount on ground
                        if (coin.coinValue <= 0)
                        {
                            this.CurrentCell.Remove(coin); // if all the coins are gone, remove the coins from the cell
                            break; // break the do loop
                        }
                    }
                    while (available > 0 && rings <= 9);
                }
            }
        }

		public void MerchantMugwortSeller()
		{
			Item coin = new Item();
			//string name = this.Name;
			int mugworts = 0;
            if (Rules.RollD(1, 100) < 10)
            {
                if (Rules.RollD(1, 10) > 5)
                {
                    Command.ParseCommand(this, "say", "Get your Snake Oil and Hair Tonic here! Just 25 coins and you will see the light.");
                }
                else
                {
                    Command.ParseCommand(this, "say", "Only 25 coins and you too can have a head of hair like this!");
                }
            }
            coin = Item.FindItemOnGround("coins", this.FacetID, this.LandID, this.MapID, this.X, this.Y, this.Z);
			foreach(Item itm in this.CurrentCell.Items)
			{
				if(itm.itemID == Item.ID_MUGWORT){mugworts++;}

				if(itm.itemType == Globals.eItemType.Coin)
				{
					coin = itm;
				}
			}
			if(coin != null && mugworts <= 9)
			{
				if(coin.coinValue >= 25)
				{
					Command.ParseCommand(this,"take","25 coins");
					this.RightHand = null;
					Item mugwort = Item.CopyItemFromDictionary(Item.ID_MUGWORT);
					this.CurrentCell.Add(mugwort);
				}
			}
		}

		public void MerchantBalmSeller()
		{
            Item coin = null;

            int balms = 0; // # of balms on the ground

            if (Rules.RollD(1, 100) < 10)
            {
                Command.ParseCommand(this, "say","Fresh Balm of Gilead here!");
            }

            //coin = Item.findItemOnGround("coins", this.FacetID, this.LandID, this.MapID, this.X, this.Y, this.Z);

            foreach (Item item in this.CurrentCell.Items)
            {
                if (item.itemID == Item.ID_BALM) { balms++; }

                if (item.itemType == Globals.eItemType.Coin)
                {
                    coin = item;
                }
            }

            if (coin != null && balms <= 9)
            {
                Item balm = Item.CopyItemFromDictionary(Item.ID_BALM);

                if (coin.coinValue >= balm.coinValue)
                {
                    int available = (int)Math.Truncate(coin.coinValue / balm.coinValue);
                    do
                    {
                        balm = Item.CopyItemFromDictionary(Item.ID_BALM); // copy balm from dictionary
                        coin.coinValue -= balm.coinValue; // subtract balm cost from coins
                        this.CurrentCell.Add(balm); // add balm to cell
                        available--; // subtract from available
                        balms++; // add to balms amount on ground
                        if (coin.coinValue <= 0)
                        {
                            this.CurrentCell.Remove(coin); // if all the coins are gone, remove the coins from the cell
                            break; // break the do loop
                        }
                    }
                    while (available > 0 && balms <= 9);
                }
            }
		}

        public static string GetNumberEnd(int number)
        {
            switch (number)
            {
                case 1:
                    return "st";
                case 2:
                    return "nd";
                case 3:
                    return "rd";
                default:
                    return "th";
            }
        }

		public override void SendMerchantList(Character chr) 
		{
            if (this.PlayersFlagged.Contains(chr.PlayerID))
            {
                chr.WriteToDisplay(this.Name + ": I am quite busy. You will have to come back later.");
                return;
            }

			List<StoreItem> storeInventory = DAL.DBWorld.LoadStoreItems(this.NPCIdent); // storeInventory of StoreItem objects

			if(storeInventory.Count <= 0) 
			{
				chr.WriteToDisplay(this.Name+": I have been cleaned out! It's a glorious day indeed. Come back later and I might have something for you.");
				return;
			}

			string header = "Name          Price Stocked   Name          Price Stocked";

            bool devRequest = false;

            if (chr.ImpLevel >= Globals.eImpLevel.GM)
            {
                header = "Name          Price Stocked Seller            Name          Price Stocked Seller";
                devRequest = true;
            }

            if (chr.protocol == "old-kesmai")
            {
                chr.Write(Map.KP_NEXT_LINE + header + Map.KP_ERASE_END_LINE);
            }
            else
            {
                chr.WriteToDisplay(header);
            }
	
			int i = 0, j = 6;
			System.Text.StringBuilder sb = new System.Text.StringBuilder(60);
			while (i < 6)
			{
				sb = new System.Text.StringBuilder(60);
				if (i < storeInventory.Count) 
					sb.Append(this.writePriceline(chr.protocol, storeInventory[i], "left", devRequest));
				
				if (j < storeInventory.Count) 
					sb.Append(this.writePriceline(chr.protocol, storeInventory[j], "right", devRequest));
				
				if (chr.protocol=="old-kesmai") sb.Append(Map.KP_ERASE_END_LINE);

				if (i < storeInventory.Count || j < storeInventory.Count) 
				{
					if (chr.protocol=="old-kesmai") chr.Write(sb.ToString());
					else chr.WriteToDisplay(sb.ToString());
				}					
				i++; j++;
			}				
		}
		
		// Write an item name and its price to the stream with KP formatting
		// align should be "left" or "right"
        private string writePriceline(string proto, StoreItem storeItem, string align, bool devRequest)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (align == "right") { sb.Append("   "); }

            sb.Append(Item.GetItemNameFromItemDictionary(storeItem.itemID).PadRight(13, ' '));
            sb.Append(storeItem.sellPrice.ToString().PadLeft(6, ' '));
            sb.Append(storeItem.stocked.ToString().PadLeft(8, ' '));
            if (devRequest)
            {
                if (storeItem.original)
                {
                    sb.Append(" Original Stock ");
                }
                else
                {
                    if (storeItem.seller != null)
                    {
                        sb.Append(" " + storeItem.seller.ToString().PadRight(15, ' '));
                    }
                    else
                    {
                        sb.Append(" Unknown        ");
                    }
                }
            }
            return sb.ToString();
        }

        public override bool MerchantShowItem(Character viewer, string args)
        {
            if (this.PlayersFlagged.Contains(viewer.PlayerID))
            {
                viewer.WriteToDisplay(this.Name + ": I am quite busy. You will have to come back later.");
                return false;
            }

            if (args == null)
                return false;

            List<StoreItem> storeList = DAL.DBWorld.LoadStoreItems(this.NPCIdent);            

            Item item = null;
            StoreItem sItem = null;

            string[] sArgs = args.Split(" ".ToCharArray());

            if (sArgs.Length == 2) // name, show item 
            {
                foreach (StoreItem storeItem in storeList)
                {
                    if (Item.GetItemNameFromItemDictionary(storeItem.itemID).ToLower() == sArgs[1].ToLower())
                    {
                        item = Item.CopyItemFromDictionary(storeItem.itemID);
                        sItem = storeItem;
                        goto itemAppraisal;
                    }
                }
            }
            else if (sArgs.Length == 3)
            {
                int countTo = Convert.ToInt32(sArgs[1]);

                foreach (StoreItem storeItem in storeList)
                {
                    if (Item.GetItemNameFromItemDictionary(storeItem.itemID).ToLower() == sArgs[2].ToLower())
                    {
                        if (--countTo <= 0)
                        {
                            item = Item.CopyItemFromDictionary(storeItem.itemID);
                            sItem = storeItem;
                            goto itemAppraisal;
                        }
                    }
                }
            }
            else
            {
                viewer.WriteToDisplay("Format: <name>, show <item> OR <name>, show # <item>");
                return false;
            }

            if (item == null)
            {
                if (sArgs.Length == 2)
                {
                    viewer.WriteToDisplay(this.Name + ": I am not selling any of those.");
                }
                else if (sArgs.Length == 3)
                {
                    viewer.WriteToDisplay(this.Name + ": I don't have that many " + sArgs[2] + "s for sale.");
                }
                return false;
            }

        itemAppraisal:
            string itmeffect = "";
            string itmSpell = "";
            string itmcharges = "";
            string itmspecial = "";
            string itmattuned = "";
            string itmvalue = "";

            if (item.spell > 0)
            {
                if (item.charges == 0) { itmcharges = ", but there are no charges remaining."; }
                if (item.charges > 1 && item.charges < 100) { itmcharges = " There are " + item.charges + " charges remaining."; }
                if (item.charges == 1) { itmcharges = " There is 1 charge remaining."; }
                if (item.charges == -1) { itmcharges = " The " + item.name + " has unlimited charges."; }

                itmSpell = " It contains the spell of " + Spell.GetSpell(item.spell).Name + "." + itmcharges;
            }
            System.Text.StringBuilder sb = new System.Text.StringBuilder(40);
            if (item.baseType == Globals.eItemBaseType.Figurine)
            {
                sb.AppendFormat(" The {0}'s avatar has " + item.figExp + " experience.", item.name);
            }
            if (item.combatAdds > 0)
            {
                sb.AppendFormat(" The combat adds are {0}.", item.combatAdds);
            }
            if (item.silver)
            {
                sb.AppendFormat(" The {0} is silver.", item.name);
            }
            if (item.blueglow)
            {
                sb.AppendFormat(" The {0} is emitting a faint blue glow.", item.name);
            }
            if (item.alignment != Globals.eAlignment.None)
            {
                sb.AppendFormat(" The {0} is {1}.", item.name, Utils.FormatEnumString(item.alignment.ToString()).ToLower());
            }
            itmspecial = sb.ToString();
            if (item.effectType.Length > 0)
            {
                string[] itmEffectType = item.effectType.Split(" ".ToCharArray());
                string[] itmEffectAmount = item.effectAmount.Split(" ".ToCharArray());

                if (itmEffectType.Length == 1 && Effect.GetEffectName((Effect.EffectType)Convert.ToInt32(itmEffectType[0])) != "")
                {
                    if (item.baseType == Globals.eItemBaseType.Bottle)
                    {
                        Bottle bottle = (Bottle)item;
                        itmeffect = Bottle.GetFluidDesc(bottle);
                        itmeffect += " The " + item.name + " is a potion of " + Effect.GetEffectName((Effect.EffectType)Convert.ToInt32(itmEffectType[0])) + ".";
                    }
                    else
                    {
                        itmeffect = " The " + item.name + " contains the enchantment of " + Effect.GetEffectName((Effect.EffectType)Convert.ToInt32(itmEffectType[0])) + ".";
                    }
                }
                else
                {
                    List<string> itemEffectList = new List<string>();

                    for (int a = 0; a < itmEffectType.Length; a++)
                    {
                        Effect.EffectType effectType = (Effect.EffectType)Convert.ToInt32(itmEffectType[a]);
                        if (Effect.GetEffectName(effectType).ToLower() != "unknown")
                        {
                            itemEffectList.Add(Effect.GetEffectName(effectType));
                        }
                    }

                    if (itemEffectList.Count > 0)
                    {
                        if (itemEffectList.Count > 1)
                        {
                            itmeffect = " The " + item.name + " contains the enchantments of";
                            for (int a = 0; a < itemEffectList.Count; a++)
                            {
                                if (a != itemEffectList.Count - 1)
                                {
                                    itmeffect += " " + (string)itemEffectList[a] + ",";
                                }
                                else
                                {
                                    itmeffect += " and " + (string)itemEffectList[a] + ".";
                                }
                            }
                        }
                        else if (itemEffectList.Count == 1 && Effect.GetEffectName((Effect.EffectType)Convert.ToInt32(itmEffectType[0])).ToLower() != "unknown")
                        {
                            if (item.baseType == Globals.eItemBaseType.Bottle)
                            {
                                itmeffect = " Inside the bottle is a potion of " + Effect.GetEffectName((Effect.EffectType)Convert.ToInt32(itmEffectType[0])) + ".";
                            }
                            else
                            {
                                itmeffect = " The " + item.name + " contains the enchantment of " + Effect.GetEffectName((Effect.EffectType)Convert.ToInt32(itmEffectType[0])) + ".";
                            }
                        }
                    }
                    else
                    {
                        if (item.baseType == Globals.eItemBaseType.Bottle)
                        {
                            Bottle bottle = (Bottle)item;
                            itmeffect = Bottle.GetFluidDesc(bottle);
                        }
                    }
                }
            }
            if (item.attunedID != 0)
            {
                if (item.attunedID == viewer.PlayerID) { itmattuned = " The " + item.name + " is soulbound to you."; }
                else { itmattuned = " The " + item.name + " is soulbound to another individual."; }
            }
            if (sItem.sellPrice == 1)
            {
                itmvalue = " The " + item.name + " is worth 1 coin.";
            }
            else if (sItem.sellPrice > 1)
            {
                itmvalue = " The " + item.name + " is worth " + sItem.sellPrice + " coins.";
            }
            else
            {
                itmvalue = " The " + item.name + " is worthless.";
            }
            viewer.WriteToDisplay(this.Name + ": We are looking at " + item.longDesc + "." + itmeffect + itmSpell + itmspecial + itmattuned + itmvalue);
            return true;
        }

        public override string MerchantSellItem(Character buyer, string args) // merchant sells an item to a character
		{
            if (this.PlayersFlagged.Contains(buyer.PlayerID))
            {
                return this.Name + ": I am quite busy. You will have to come back later.";
            }

            string requestedItemName = "";

            int countTo = 1;

            string[] sArgs = args.Split(" ".ToCharArray());

            if (sArgs.Length == 2)
            {
                requestedItemName = sArgs[1];
            }
            else if (sArgs.Length == 3)
            {
                try
                {
                    countTo = Convert.ToInt32(sArgs[1]);

                    if (countTo < 1)
                    {
                        throw new NullReferenceException();
                    }
                }
                catch
                {
                    return this.Name + ": What do you want me to sell to you?";
                }

                requestedItemName = sArgs[2];
            }

            if (requestedItemName.ToLower() == "spellbook" || requestedItemName.ToLower() == "book")
            {
                if (this.trainerType == Merchant.TrainerType.Spell)
                {
                    Item newbook = Item.CopyItemFromDictionary(Item.ID_SPELLBOOK);
                    newbook.AttuneItem(buyer.PlayerID, "Requested new spellbook from spell trainer.");
                    Map.PutItemOnCounter(this, newbook);
                    Utils.Log("[" + this.NPCIdent + "][" + this.npcID + "] " + this.Name + " sold [" + newbook.itemID + "]" + newbook.name + " to " + this.TargetName + ".", Utils.LogType.MerchantSell);
                    return this.Name + ": Be careful not to lose this one.";
                }
                else
                {
                    return this.Name + ": I don't sell those. Why don't you try the Grand Library of Mu?";
                }
            }

			List<StoreItem> storeInventory = DAL.DBWorld.LoadStoreItems(this.NPCIdent); // inventory of StoreItem objects

            StoreItem requestedStoreItem = null;

			bool dbDelete = false; // boolean to delete the item from the database

			foreach(StoreItem storeItem in storeInventory)
			{
                if (Item.GetItemNameFromItemDictionary(storeItem.itemID).ToLower() == requestedItemName.ToLower())
                {
                    if (--countTo == 0)
                    {
                        requestedStoreItem = storeItem;
                        if (requestedStoreItem.stocked > 0)
                        {
                            requestedStoreItem.stocked -= 1; // reduce the stocked amount of this StoreItem
                        }
                        if (requestedStoreItem.stocked == 0 && !requestedStoreItem.original)
                        {
                            dbDelete = true; // this StoreItem will be deleted from this merchant's store inventory
                        }
                        break; // break the foreach loop
                    }
                }
			}

			if(requestedStoreItem != null)
			{
				Item coin = Map.GetItemFromCounter(this, "coins");
				Item item = Item.CopyItemFromDictionary(requestedStoreItem.itemID);//Item.getItemIDFromCatalogItems(itemName));
				item.charges = requestedStoreItem.charges;
				item.figExp = requestedStoreItem.figExp;
				if(coin != null && coin.coinValue >= requestedStoreItem.sellPrice) // amount of coins on counter greater than or equal to sellPrice
				{
					if(coin.coinValue > requestedStoreItem.sellPrice) // give change
					{
						coin.coinValue -= requestedStoreItem.sellPrice;
						Map.PutItemOnCounter(this, coin);
					}
					Map.PutItemOnCounter(this, item); // place the item on the counter
                    if (item.vRandLow > 0)
                    {
                        item.coinValue = Rules.dice.Next(item.vRandLow, item.vRandHigh);
                    }
					if(dbDelete) // if true delete this record from the Stores table in the database
                    {
                        DAL.DBWorld.deleteStoreItem(requestedStoreItem.stockID);
                    }
					else // otherwise just update the stocked amount
                    {
                        DAL.DBWorld.updateStoreItem(requestedStoreItem.stockID, requestedStoreItem.stocked);
                    }
                    Utils.Log("[" + this.NPCIdent + "][" + this.npcID + "] " + this.Name + " sold [" + item.itemID + "]" + item.name + " to "+this.TargetName+".", Utils.LogType.MerchantSell);
					return this.Name+": Thank you for your business.";
				}
				else
				{
                    if (coin == null)
                    {
                        return this.Name + ": I don't see any coins here.";
                    }
                    else
                    {
                        Map.PutItemOnCounter(this, coin);
                        return this.Name + ": There aren't enough coins here for the " + item.name + ".";
                    }
				}		
			}
			else
			{
				return this.Name +": I don't sell that.";
			}
		}

        public override string MerchantBuyItem(Character chr, string args) // merchant buys an item from the character
		{
            // name, buy <item>
            // name, buy all
            // name, buy # <item>

            if (this.PlayersFlagged.Contains(chr.PlayerID))
            {
                return this.Name + ": I am quite busy. You will have to come back later.";
            }

            string[] sArgs = args.Split(" ".ToCharArray());
            string itemName = sArgs[1];

            bool buyAllSpecific = false;

            Cell counterCell = Map.GetNearestCounterOrAltarCell(this);

            if (sArgs[1].ToLower().EndsWith("s"))
            {
                foreach (Item item in counterCell.Items)
                {
                    if (item.name.ToLower() == sArgs[1].ToLower().Substring(0, sArgs[1].Length - 1))
                    {
                        buyAllSpecific = true;
                        itemName = item.name;
                        break;
                    }
                }
            }

			try
			{
                List<StoreItem> storeInventory = DAL.DBWorld.LoadStoreItems(this.NPCIdent); // get the merchant's current store inventory
                string purchaseinfo = ""; // information sent to the character about purchases
                bool addInventory = true; // will this item be inserted as a record into the Store table
				// asked to buy everything on the counter
                if (sArgs[1] == "all" || sArgs[1] == "items" || sArgs[1] == "everything" || buyAllSpecific)
                {
                    if (counterCell.Items.Count < 1)
                    {
                        return this.Name + ": There is nothing here to buy.";
                    }
                    else
                    {
                        double totalPurchase = 0;
                        //create a temporary array to keep track of purchased items
                        ArrayList purchasedItems = new ArrayList();

                        foreach (Item counterItem in counterCell.Items)
                        {
                            addInventory = true;
                            bool buyItem = true;
                            //make a copy of the item from catalog to compare the two
                            Item cloneItem = Item.CopyItemFromDictionary(counterItem.itemID);
                            //does the item have less charges than it's max
                            if (counterItem.charges != 0 && counterItem.charges < cloneItem.charges)
                            {
                                //if the merchant has lower intelligence than the player let's random to see if we decrease the cValue
                                if (this.Intelligence < chr.Intelligence)
                                {
                                    //second check to see if we lower the value
                                    if (Rules.RollD(1, 20) < this.Intelligence)
                                    {
                                        counterItem.coinValue = counterItem.coinValue - (int)(counterItem.coinValue / (cloneItem.charges - counterItem.charges));
                                        purchaseinfo += " The " + counterItem.name + " has less than it's normal magical charges so I have paid you less for it.";
                                    }
                                }
                            }
                            //do not buy an item that has been depleted of charges
                            if (counterItem.charges == 0 && cloneItem.charges > 0)
                            {
                                buyItem = false;
                                purchaseinfo += " The " + counterItem.name + " is out of charges so it is worthless to me.";
                            }
                            //do not buy an item that is attuned
                            if (counterItem.attunedID != 0)
                            {
                                buyItem = false;
                                purchaseinfo += " The " + counterItem.name + " is soulbound to an individual so it is worthless to me.";
                            }
                            //do not buy an item that has no cValue
                            if (counterItem.coinValue < 1)
                            {
                                buyItem = false;
                                purchaseinfo += " The " + counterItem.name + " is worthless.";
                            }
                            if (buyItem && counterItem.itemType != Globals.eItemType.Coin)
                            {
                                //put this item in our temporary array
                                if (buyAllSpecific)
                                {
                                    if (counterItem.name == itemName)
                                    {
                                        purchasedItems.Add(counterItem);
                                        totalPurchase += counterItem.coinValue;
                                    }
                                }
                                else
                                {
                                    purchasedItems.Add(counterItem);
                                    totalPurchase += counterItem.coinValue;
                                }
                                
                                foreach (StoreItem sItem in storeInventory)
                                {
                                    //if this item is already in our inventory increase the stock amount
                                    if (counterItem.itemID == sItem.itemID)
                                    {
                                        if (DAL.DBWorld.updateStoreItem(sItem.stockID, sItem.stocked + 1) == 1)
                                        {
                                            //update our Inventory array with the increase in stock amount
                                            sItem.stocked = sItem.stocked + 1;
                                            //log this transaction if the menu item is checked
                                            Utils.Log("[" + this.NPCIdent + "][" + this.npcID + "]" + this.Name + " bought and increased stock of item [" + counterItem.itemID + "]" + counterItem.name + " from [" + chr.PlayerID + "]" + chr.Name + "(" + chr.account + ").", Utils.LogType.MerchantBuy);
                                            //don't insert this item as a record into Store
                                            addInventory = false;
                                        }
                                    }
                                }
                                //if this item wasn't updated and our Inventory isn't full
                                if (addInventory && storeInventory.Count < MAX_STORE_INVENTORY)
                                {
                                    //run a check to see if we're the right merchantType to insert this item into our Inventory
                                    if (StoreItem.VerifyStoreInsert(counterItem, this.merchantType))
                                    {
                                        StoreItem storeItem = new StoreItem();
                                        storeItem.itemID = counterItem.itemID;
                                        storeItem.original = false;
                                        storeItem.sellPrice = (int)counterItem.coinValue * this.MerchantMarkup;
                                        storeItem.stocked = 1;
                                        storeItem.seller = chr.Name;
                                        storeItem.restock = 0;

                                        storeItem.charges = counterItem.charges;
                                        storeItem.figExp = counterItem.figExp;
                                        if (DAL.DBWorld.insertStoreItem(this.NPCIdent, storeItem) != -1)
                                        {
                                            storeInventory = DAL.DBWorld.LoadStoreItems(this.npcID); // update Inventory to reflect the addition of this item                                               
                                            Utils.Log("[" + this.NPCIdent + "][" + this.npcID + "]" + this.Name + " bought and added store item [" + counterItem.itemID + "]" + counterItem.name + " from [" + chr.PlayerID + "]" + chr.Name + "(" + chr.account + ").", Utils.LogType.MerchantBuy);
                                        }
                                    }
                                }
                            }
                        }
                        foreach (Item pItem in purchasedItems)
                        {
                            counterCell.Remove(pItem);
                        }
                        if (totalPurchase < 1)
                        {
                            return this.Name + ": There is nothing of value here.";
                        }
                        Item totalSale = Item.CopyItemFromDictionary(Item.ID_COINS);
                        totalSale.coinValue = totalPurchase;
                        Map.PutItemOnCounter(this, totalSale);
                    }
                }
                else
                {
                    Item purchaseItem = null;
                    Item compareItem = null;

                    if (sArgs.Length == 2)
                    {
                        purchaseItem = Map.GetItemFromCounter(this, itemName);
                        if(purchaseItem != null)
                            compareItem = Item.CopyItemFromDictionary(purchaseItem.itemID);
                    }
                    else if(sArgs.Length == 3)
                    {
                        try
                        {
                            itemName = sArgs[2];
                            int countRequest = Convert.ToInt32(sArgs[1]);

                            if (countRequest <= 0)
                            {
                                countRequest = 1;
                            }

                            int itemFound = 0;
                            foreach (Item found in counterCell.Items)
                            {
                                if (found.name.ToLower() == itemName.ToLower())
                                {
                                    if (++itemFound == countRequest)
                                    {
                                        purchaseItem = DragonsSpine.Map.GetItemFromCounter(this, found.worldItemID); ;
                                        compareItem = Item.CopyItemFromDictionary(purchaseItem.itemID);
                                    }
                                }
                            }
                        }
                        catch
                        {
                            purchaseItem = null;
                        }
                    }

                    if (purchaseItem == null)
                    {
                        return this.Name + ": I don't see a " + itemName + " here.";
                    }

                    // make sure we pick up the item - stop the gold dupe.
                    if (purchaseItem == null)
                    {
                        purchaseItem = Map.GetItemFromCounter(this, purchaseItem.name);
                    }

                    if (purchaseItem.itemType == Globals.eItemType.Coin)
                    {
                        //Map.PutItemOnCounter(this, purchaseItem);
                        return this.Name + ": Do you really want me to buy your " + purchaseItem.name + "? How does an exchange of five to one sound?";
                    }
                    if (purchaseItem.charges < 1 && compareItem.charges > 0)
                    {
                        Map.PutItemOnCounter(this, purchaseItem);
                        return this.Name + ": The " + purchaseItem.name + " is out of charges so it is worthless to me. Perhaps a wizard can recharge it for you.";
                    }
                    if (purchaseItem.attunedID != 0 && compareItem.attunedID == 0)
                    {
                        Map.PutItemOnCounter(this, purchaseItem);
                        return this.Name + ": The " + purchaseItem.name + " is soulbound to an individual so it is worthless to me.";
                    }
                    if (purchaseItem.coinValue < 1)
                    {
                        Map.PutItemOnCounter(this, purchaseItem);
                        return this.Name + ": The " + purchaseItem.name + " is worthless.";
                    }
                    if (purchaseItem.charges != 0 && purchaseItem.charges < compareItem.charges)
                    {
                        if (chr.Intelligence > this.Intelligence)
                        {
                            if (Rules.RollD(1, this.Intelligence) < this.Intelligence)
                            {
                                purchaseItem.coinValue = purchaseItem.coinValue - (int)(purchaseItem.coinValue / (compareItem.charges - purchaseItem.charges));
                                purchaseinfo += " The " + purchaseItem.name + " has less than it's normal magical charges so I have paid you less for it.";
                            }
                        }
                    }

                    if (storeInventory.Count < MAX_STORE_INVENTORY)
                    {
                        foreach (StoreItem sItem in storeInventory)
                        {
                            if (purchaseItem.itemID == sItem.itemID) // if the merchant already has this item just the stockedNum is increased
                            {
                                if (DAL.DBWorld.updateStoreItem(sItem.stockID, sItem.stocked + 1) == 1)
                                {
                                    Utils.Log("[" + this.NPCIdent + "][" + this.npcID + "]" + this.Name + " bought and increased stock of item [" + purchaseItem.itemID + "]" + purchaseItem.name + " from [" + chr.PlayerID + "]" + chr.Name + "(" + chr.account + ").", Utils.LogType.MerchantBuy);
                                    addInventory = false;
                                }
                            }
                        }
                    }

                    if (this.RightHand != null) { this.RightHand = null; }

                    if (addInventory && storeInventory.Count < MAX_STORE_INVENTORY)
                    {
                        if (StoreItem.VerifyStoreInsert(purchaseItem, this.merchantType))
                        {
                            StoreItem storeItem = new StoreItem();
                            storeItem.itemID = purchaseItem.itemID;
                            storeItem.original = false;
                            storeItem.sellPrice = (int)purchaseItem.coinValue * this.MerchantMarkup;
                            storeItem.stocked = 1;
                            storeItem.seller = chr.Name;
                            storeItem.restock = 0;

                            storeItem.charges = purchaseItem.charges;
                            storeItem.figExp = purchaseItem.figExp;
                            if (DAL.DBWorld.insertStoreItem(this.NPCIdent, storeItem) != -1)
                            {
                                Utils.Log("[" + this.NPCIdent + "][" + this.npcID + "]" + this.Name + " bought and added store item [" + purchaseItem.itemID + "]" + purchaseItem.name + " from [" + chr.PlayerID + "]" + chr.Name + "(" + chr.account + ").", Utils.LogType.MerchantBuy);
                            }
                        }
                    }

                    Item totalCoins = Item.CopyItemFromDictionary(Item.ID_COINS);
                    totalCoins.coinValue = purchaseItem.coinValue;
                    Map.PutItemOnCounter(this, totalCoins);
                }
				return this.Name+":"+purchaseinfo+" Thank you for your business.";
			}
			catch(Exception e)
			{
                Utils.Log("Failure at merchantBuyItem(" + chr.GetLogString() + ", " + args + ")", Utils.LogType.SystemFailure);
                Utils.LogException(e);
				return this.Name+": I've encountered an error. Please pray to the Ghods with me.";
			}
		}

        public override string MerchantTrainSpell(Character chr, string spellName) // train the character with the new spell
		{
			if(this.trainerType != Merchant.TrainerType.Spell)
			{
				chr.WriteToDisplay(this.Name+": I cannot help you with that.");
			}
			else
			{
				chr.WriteToDisplay(this.Name+": What spell would you like to learn?");
			}
			return "";
		}

        public override void MerchantWithdraw(Character chr, string amount) // withdraw money for character
		{
			if(this.interactiveType != Merchant.InteractiveType.Banker)
			{
				chr.WriteToDisplay(this.Name+": I am not a banker.");
				return;
			}

            if (this.PlayersFlagged.Contains(chr.PlayerID))
            {
                chr.WriteToDisplay(this.Name + ": I am quite busy. You will have to come back later.");
                return;
            }

            if (amount == null || amount == "")
            {
                chr.WriteToDisplay(this.Name + ": Please tell me how much you wish to withdraw.");
                return;
            }

            if (amount.StartsWith("-"))
            {
                chr.WriteToDisplay(this.Name + ": That's interesting. Next!");
                return;
            }

			if(amount == "all")
            {
                amount = chr.bankGold.ToString();
            }

			long gps = Convert.ToInt64(amount, 10);

			if(gps <= 0 )
			{
                chr.WriteToDisplay(this.Name + ": Robbing the bank is not tolerated.", Protocol.TextType.CreatureChat);
				return;
			}

			if(chr.bankGold < gps)
			{
                chr.WriteToDisplay(this.Name + ": You don't have that many coins in your account.", Protocol.TextType.CreatureChat);
			}
			else
			{
				Item gold = Item.CopyItemFromDictionary(Item.ID_COINS);
				gold.coinValue = Convert.ToInt64(amount);
				chr.bankGold -= Convert.ToInt64(amount);
				Map.PutItemOnCounter(this, gold);
                chr.WriteToDisplay(this.Name + ": Thanks for the business.", Protocol.TextType.CreatureChat);
			}
		}

        public override void MerchantDeposit(Character chr, string amount) // deposit money for character
		{
            if (this.PlayersFlagged.Contains(chr.PlayerID))
            {
                chr.WriteToDisplay(this.Name + ": I am quite busy. You will have to come back later.");
                return;
            }

            if (amount.StartsWith("-"))
            {
                chr.WriteToDisplay(this.Name + ": That's interesting. Next!");
                return;
            }

			Item gold = Map.GetItemFromCounter(this, "coins");

			if(amount == "coins" || amount == "gold" || amount == "all" || amount == "")
			{
				chr.bankGold += gold.coinValue;
                chr.WriteToDisplay(this.Name + ": You now have " + chr.bankGold + " coins in your account.", Protocol.TextType.CreatureChat);
				Map.deleteItemFromCounter(this,"coins");
			}
			else
			{
				long gps = Convert.ToInt64(amount, 10);

				// make sure there are enough coins on the counter
				if(gold.coinValue > gps)
				{
					gold.coinValue -= gps;
					chr.bankGold += gps;
                    chr.WriteToDisplay(this.Name + ": You now have " + chr.bankGold + " coins in your account.", Protocol.TextType.CreatureChat);
					Map.PutItemOnCounter(this,gold);
				}
				else if(gold.coinValue == gps)
				{
					chr.bankGold += gps;
				}
				else
				{
                    chr.WriteToDisplay(this.Name + ": I don't see " + amount + " coins here.", Protocol.TextType.CreatureChat);
					Map.PutItemOnCounter(this,gold);
					return;
				}
			}
		}

        public override void MerchantSkin(Character chr, string itemname)
        {
            if (this.interactiveType != Merchant.InteractiveType.Tanner || itemname != "corpse")
            {
                chr.WriteToDisplay(this.Name + ": I don't know how to do that.");
            }
            else
            {
                Item itm = Item.GetItemFromGround(itemname, this.FacetID, this.LandID, this.MapID, this.X, this.Y, this.Z);
                if (itm == null)
                {
                    chr.WriteToDisplay(this.Name + ": There is nothing useful to skin from this.", Protocol.TextType.CreatureChat);
                    return;
                }
                else
                {
                    foreach (Item item in itm.contentList)
                    {
                        this.CurrentCell.Add(item);
                    }
                }
                chr.WriteToDisplay(this.Name + ": There you go.", Protocol.TextType.CreatureChat);
                return;
            }
        }
		
		public override void MerchantTrain(Character chr)
		{
            if (this.PlayersFlagged.Contains(chr.PlayerID))
            {
                chr.WriteToDisplay(this.Name + ": I am quite busy. You will have to come back later.");
                return;
            }

			Item gold = Map.GetItemFromCounter(this, "coins");

            if (gold == null)
            {
                gold = Item.GetItemFromGround("coins", this.CurrentCell);
                if (gold != null)
                {
                    gold.land = this.LandID; // used locally only to determine where to place change
                }
            }
            else gold.land = -1;

            if (gold == null)
            {
                chr.WriteToDisplay(this.Name + ": There is no gold here.");
                return;
            }

            if (this.trainerType == TrainerType.Sage)
            {
                chr.Experience += (int)(gold.coinValue);
                Command.ParseCommand(this, "tell", chr.Name + " " + GetSageAdvice());
            }
            else if (this.trainerType == TrainerType.HP_Doctor)
            {
                #region Hit Point Doctor
                if (chr.Level < 3)
                {
                    chr.WriteToDisplay(this.Name + ": Come back when you are more experienced, " + chr.Name + ".");
                    if (gold.land == -1)
                    {
                        Map.PutItemOnCounter(this, gold);
                    }
                    else this.CurrentCell.Add(gold);
                    return;
                }

                if (PlayersFlagged.Contains(chr.PlayerID))
                {
                    chr.WriteToDisplay(this.Name + ": I am quite busy. You will have to come back later.");
                    if (gold.land == -1)
                    {
                        Map.PutItemOnCounter(this, gold);
                    }
                    else this.CurrentCell.Add(gold);
                    return;
                }

                int doctorLimit = World.DoctoredHPLimits[(int)chr.BaseProfession];

                if (chr.HitsDoctored >= doctorLimit)
                {
                    chr.WriteToDisplay(this.Name + ": You have reached your limit of " + doctorLimit + " doctored hit points.");
                    if (gold.land == -1)
                    {
                        Map.PutItemOnCounter(this, gold);
                    }
                    else this.CurrentCell.Add(gold);
                    return;
                }

                long nextHPCost = 0;
                int totalHPDoctored = 0;
                long totalCost = 0;

                nextHPCost = Rules.Formula_DoctoredHPCost(chr.HitsDoctored + 1);

                while (gold.coinValue >= nextHPCost)
                {
                    chr.HitsDoctored++;
                    totalHPDoctored++;
                    gold.coinValue = gold.coinValue - (double)nextHPCost;
                    totalCost += nextHPCost;
                    nextHPCost = Rules.Formula_DoctoredHPCost(chr.HitsDoctored + 1);
                }

                if (totalHPDoctored == 0)
                {
                    chr.WriteToDisplay(this.Name + ": There is not enough gold here to properly treat you.");
                }
                else
                {
                    string plural = "";
                    if (totalHPDoctored > 1) { plural = "s"; }
                    chr.WriteToDisplay("You have purchased an additional " + totalHPDoctored + " hit point" + plural + " at a cost of " + totalCost + " coins.");
                    chr.WriteToDisplay(this.Name + ": I do thank you for your donation. Good hunting, " + chr.Name + ".");
                }
                if (gold.land == -1)
                {
                    Map.PutItemOnCounter(this, gold);
                }
                else this.CurrentCell.Add(gold); 
                #endregion
            }
            else
            {
                #region Default skill train

                // place gold in cell if same Land ID as merchant
                if (gold.land == this.LandID)
                {
                    if (!Skills.SkillTrain(chr, this, gold))
                    {
                        this.CurrentCell.Add(gold); // drop the gold
                    }
                    return;
                }

                if (!Skills.SkillTrain(chr, this, gold))
                {
                    Map.PutItemOnCounter(chr, gold); // put the entire quantity back on the counter
                }
                #endregion
            }
		}

		public static string GetSageAdvice()
		{
			string advice = "";
			#region Proverbs
			string[] proverbs = {"43% of all statistics are worthless.","A bird does not sing because it has an answer -- it sings because it has a song.",
									"A budget is just a method of worrying before you spend money, as well as afterward.",
									"A bus is a vehicle that runs twice as fast when you are after it as when you are in it.",
									"A camel is a horse designed by a committee.",
									"A celebrity is someone who works hard all their life to become known and then wears dark glasses to avoid being recognized.",
									"A classic is something that everybody wants to have read and Nobody has.",
									"A closed mind is a good thing to lose.",
									"A closed mind is like a closed book; just a block of wood.",
									"A crisis is when you can't say: 'let's forget the whole thing'.",
									"A crumb from a winner's table is better than a feast from a loser's table!",
									"Action may not always be happiness, but there is no happiness without action.",
									"A cynic is someone who knows the price of everything and the value of nothing.",
									"A dancer goes quick on her beautiful legs; a duck goes quack on her beautiful eggs.",
									"A diet is a selection of food that makes other people lose weight.",
									"A diplomat is a man who always remembers a woman's birthday but never remembers her age.",
									"A dog inside a kennel barks at his fleas. A dog hunting does not notice them.",
									"A dog who attends a flea circus most likely will steal the whole show.",
									"A dream is just a dream. A goal is a dream with a plan and a deadline.",
									"A drop of ink may make a million think.",
									"A drunk mans' words are a sober mans' thoughts.",
									"Adult: A person who has stopped growing at both ends and is now growing in the middle.",
									"Adversity doesn't build character, it reveals it.",
									"Advice is what we ask for when we already know the answer but wish we didn't.",
									"A fall will always make a wise man wiser.",
									"Sun Tzu said:  The art of war is of vital importance to the State.",
									"If anything just cannot go wrong, it will anyway.",
									"If you perceive that there are four possible ways in which something can go wrong, and circumvent these, then a fifth way, unprepared for, will promptly develop.",
									"Left to themselves, things tend to go from bad to worse.",
									"If everything seems to be going well, you have obviously overlooked something.",
									"Nature always sides with the hidden flaw.",
									"Mother nature is a bitch.",
									"Things get worse under pressure.",
									"Smile . . . tomorrow will be worse.",
									"Everything goes wrong all at once.",
									"Matter will be damaged in direct proportion to its value.",
									"Enough research will tend to support whatever theory.",
									"Research supports a specific theory depending on the amount of funds dedicated to it.",
									"In nature, nothing is ever right. Therefore, if everything is going right ... something is wrong.",
									"It is impossible to make anything foolproof because fools are so ingenious.",
									"Rule of Accuracy: When working toward the solution of a problem, it always helps if you know the answer.",
									"Nothing is as easy as it looks.",
									"Whenever you set out to do something, something else must be done first.",
									"Every solution breeds new problems.",
									"no matter how perfect things are made to appear, Murphy's law will take effect and screw it up.",
									"The chance of the bread falling with the buttered side down is directly proportional to the cost of the carpet.",
									"A falling object will always land where it can do the most damage.",
									"A shatterproof object will always fall on the only surface hard enough to crack or break it.",
									"You will always find something in the last place you look.",
									"If you're looking for more than one thing, you'll find the most important one last.",
									"After you bought a replacement for something you've lost and searched for everywhere, you'll find the original.",
									"The other line always moves faster.",
									"In order to get a loan, you must first prove you don't need it.",
									"If it jams - force it. If it breaks, it needed replacing anyway.",
									"When a broken appliance is demonstrated for the repairman, it will work perfectly.",
									"Build a system that even a fool can use, and only a fool will use it.",
									"Everyone has a scheme for getting rich that will not work.",
									"In any hierarchy, each individual rises to his own level of incompetence, and then remains there.",
									"There's never time to do it right, but there's always time to do it over.",
									"When in doubt, mumble. When in trouble, delegate.",
									"Anything good in life is either illegal, immoral or fattening.",
									"Murphy's golden rule: whoever has the gold makes the rules.",
									"A Smith & Wesson beats four aces.",
									"In case of doubt, make it sound convincing.",
									"Never argue with a fool, people might not know the difference.",
									"Whatever hits the fan will not be evenly distributed.",
									"No good deed goes unpunished.",
									"Where patience fails, force prevails.",
									"If you want something bad enough, chances are you won't get it.",
									"If you think you are doing the right thing, chances are it will back-fire in your face.",
									"The fish are always biting....yesterday!",
									"The cost of the hair do is directly related to the strength of the wind.",
									"Great ideas are never remembered and dumb statements are never forgotten.",
									"When you see light at the end of the tunnel, the tunnel will cave in.",
									"Being dead right, won't make you any less dead.",
									"Whatever you want, you can't have, what you can have, you don't want.",
									"Whatever you want to do, is Not possible, what ever is possible for you to do, you don't want to do it.",
									"A knowledge of Murphy's Law is no help in any situation.",
									"If you apply Murphy's Law, it will no longer be applicable.",
									"If you say something, and stake your reputation on it, you will lose your reputation.",
									"no matter where I go, there I am.",
									"If authority was mass, stupidity would be gravity.",
									"Ants will always infest the nearest food cupboard.",
									"Those who know the least will always know it the loudest.",
									"You will find an easy way to do it, after you've finished doing it.",
									"It always takes longer than you think, even when you take into account Hofstadter's Law.",
									"Laundry Math: 1 Washer + 1 Dryer + 2 Socks = 1 Sock",
									"Anyone who isn't paranoid simply isn't paying attention.",
									"A valuable falling in a hard to reach place will be exactly at the distance of the tip of your fingers.",
									"The probability of rain is inversely proportional to the size of the umbrella you carry around with you all day.",
									"Whenever you cut your finger nails, you find a need for them an hour later.",
									"In order for something to get clean, something else must get dirty.",
									"Nothing is impossible for the man who doesn't have to do it himself.",
									"The likelihood of something happening is in inverse proportion to the desirability of it happening.",
									"Common Sense Is Not So Common.",
									"Two wrongs don't make a right. It usually takes three or four.",
									"If the truth is in your favor no one will believe you.",
									"Laws are like a spider web, in that they ensnare the poor and weak while the rich and powerful break them.",
									"The key to happiness is to be O.K. with not being O.K.",
									"Every rule has an exception except the Rule of Exceptions.",
									"If your action has a 50% possibility of being correct, you will be wrong 75% of the time.",
									"The difference between Stupidity and Genius is that Genius has its limits.",
									"The universe is great enough for all possibilities to exist.",
									"Those who don't take decisions never make mistakes.",
									"Anything that seems right, is putting you into a false sense of security.",
									"The only time you're right, is when its about being wrong.",
									"The road to success is always under construction.",
									"Any given program, when running, is obsolete.",
									"Any given program costs more and takes longer each time it is run.",
									"If a program is useful, it will have to be changed.",
									"If a program is useless, it will have to be documented.",
									"Any given program will expand to fill all the available memory.",
									"Program complexity grows until it exceeds the capability of the programmer who must maintain it.",
									"Software bugs are impossible to detect by anybody except the end user.",
									"A sucking chest wound is Nature's way of telling you to slow down.",
									"If it's stupid but it works, it isn't stupid.",
									"Try to look unimportant; the enemy may be low on ammo and not want to waste a bullet on you.",
									"If at first you don't succeed, call in an air strike.",
									"If you are forward of your position, your artillery will fall short.",
									"Never share a foxhole with anyone braver than yourself.",
									"Never go to bed with anyone crazier than yourself.",
									"Never forget that your weapon was made by the lowest bidder.",
									"If your attack is going really well, it's an ambush.",
									"The enemy diversion you're ignoring is their main attack.",
									"There is no such thing as a perfect plan.",
									"Five second fuses always burn three seconds.",
									"There is no such thing as an atheist in a foxhole.",
									"The easy way is always mined.",
									"Teamwork is essential, it gives the enemy other people to shoot at.",
									"Never draw fire, it irritates everyone around you.",
									"If you are short of everything but the enemy, you are in the combat zone.",
									"When you have secured the area, make sure the enemy knows it too.",
									"Incoming fire has the right of way.",
									"If you can't remember, the Claymore is pointed toward you.",
									"Bullet Proof vests aren't.",
									"The bigger they are, the harder they fall. They punch, kick and choke harder too.",
									"Tear gas works on cops too, and regardless of wind direction, will always blow back in your face.",
									"Any suspect with a rifle is a better shot than any cop with a pistol.",
									"When in doubt, empty your shotgun.",
									"Success occurs when no one is looking, failure occurs when the Client is watching."};
			#endregion
			advice = proverbs[Rules.dice.Next(proverbs.Length)];
			return advice;
		}

        //public static bool SkillTrain(Character chr, Character trainer, Item gold)
        //{
        //    long trainerSkill = 0; // trainer exp in the skill
        //    long skillExp = 0; // player exp in the skill
        //    long trainedSkillExp = 0; // current player trained amount
        //    string skillName = "";
        //    SkillType skillType = SkillType.None;

        //    if(chr.RightHand == null) 
        //    {
        //        skillType = SkillType.Unarmed;
        //        skillName = "in the martial arts";
        //    }
        //    else
        //    {
        //        skillType = chr.RightHand.skillType;
        //        if (skillType == SkillType.Magic)
        //        {
        //            switch (chr.BaseProfession)
        //            {
        //                case ClassType.Thaumaturge:
        //                    skillName = "in the art of thaumaturgy";
        //                    break;
        //                case ClassType.Thief:
        //                    skillName = "in the art of shadow magic";
        //                    break;
        //                case ClassType.Wizard:
        //                    skillName = "in the art of wizardry";
        //                    break;
        //                default:
        //                    skillName = "in magic";
        //                    break;
        //            }
        //        }
        //        else if (skillType == SkillType.Thievery)
        //        {
        //            skillName = "in the ways of thievery";
        //        }
        //        else
        //        {
        //            skillName = "with " + chr.RightHand.shortDesc;
        //        }
        //    }

        //    trainerSkill = trainer.GetSkillExperience(skillType);
        //    skillExp = chr.GetSkillExperience(skillType);
        //    trainedSkillExp = chr.GetTrainedSkillExperience(skillType);

        //    if (trainerSkill <= 0) // if the trainer is not skilled
        //    {
        //        chr.WriteToDisplay(trainer.Name+": I am not skilled "+skillName+".");
        //        return false;
        //    }
        //    if (skillExp > trainerSkill) // if the character is more skilled than the trainer
        //    {
        //        chr.WriteToDisplay(trainer.Name+": You are more skilled "+skillName+" than I am.");
        //        return false;
        //    }

        //    long maxTrainAmount = 0;
        //    long skillMaxXp = Skills.GetSkillToMax(Skills.GetSkillLevel(skillExp));
        //    long skillIntoLevel = skillMaxXp - skillExp;
        //    long skillRemain = Skills.GetSkillToNext(Skills.GetSkillLevel(skillExp)) - skillIntoLevel;
        //    int skillRank = (int)(((float)skillRemain /(float) Skills.GetSkillToNext(Skills.GetSkillLevel(skillExp)))*10);
        //    if(skillRank <= 0){skillRank = 1;}
        //    if(skillRank > 10){skillRank = 10;}
        //    long skillRankCost = Rules.GetTrainingCostPerRank(Skills.GetSkillLevel(skillExp));
        //    long nextLevelRankCost = Rules.GetTrainingCostPerRank(Skills.GetSkillLevel(skillExp) + 1);
        //    long trainAmount = 0;

        //    if(skillRank <= 5)
        //    {
        //        maxTrainAmount = 5 * skillRankCost;
        //    }
        //    else
        //    {
        //        maxTrainAmount = ((10 - skillRank) * skillRankCost) + nextLevelRankCost;
        //    }
        //    if (trainedSkillExp >= maxTrainAmount) // if the player's already trained to the maximum then send them off to practice
        //    {
        //        chr.WriteToDisplay(trainer.Name+": You must practice more before I can train you again.");
        //        return false;
        //    }

        //    // subtract the amount this character is already trained from the maximum to set new maximum
        //    if(trainedSkillExp > 0){maxTrainAmount = maxTrainAmount - trainedSkillExp;}

        //    // figure the amount of gold we'll take and set the training amount
        //    if(gold.coinValue > maxTrainAmount)
        //    {
        //        trainAmount = maxTrainAmount;
        //        gold.coinValue = gold.coinValue - trainAmount;
        //        if (gold.land == -1)
        //        {
        //            Map.PutItemOnCounter(chr, gold);
        //        }
        //        else
        //        {
        //            trainer.CurrentCell.Add(gold);
        //        }
        //    }
        //    else
        //    {
        //        trainAmount = Convert.ToInt64(gold.coinValue);
        //    }

        //    //adjust the character's training amount
        //    switch(skillType)
        //    {
        //        case SkillType.Bow:
        //            chr.trainedBow += trainAmount;
        //            break;
        //        case SkillType.Sword:
        //            chr.trainedSword += trainAmount;
        //            break;
        //        case SkillType.Two_Handed:
        //            chr.trainedTwoHanded += trainAmount;
        //            break;
        //        case SkillType.Unarmed:
        //            chr.trainedUnarmed += trainAmount;
        //            break;
        //        case SkillType.Staff:
        //            chr.trainedStaff += trainAmount;
        //            break;
        //        case SkillType.Dagger:
        //            chr.trainedDagger += trainAmount;
        //            break;
        //        case SkillType.Halberd:
        //            chr.trainedHalberd += trainAmount;
        //            break;
        //        case SkillType.Rapier:
        //            chr.trainedRapier += trainAmount;
        //            break;
        //        case SkillType.Shuriken:
        //            chr.trainedShuriken += trainAmount;
        //            break;
        //        case SkillType.Magic:
        //            chr.trainedMagic += trainAmount;
        //            skillName = "in magic";
        //            break;
        //        case SkillType.Mace:
        //            chr.trainedMace += trainAmount;
        //            break;
        //        case SkillType.Flail:
        //            chr.trainedFlail += trainAmount;
        //            break;
        //        case SkillType.Threestaff:
        //            chr.trainedThreestaff += trainAmount;
        //            break;
        //        default:
        //            chr.WriteToDisplay(trainer.Name + ": There has been an error with your training. Please report this.");
        //            return false;
        //    }

        //    Utils.Log(chr.GetLogString()+" trains "+skillType+". trainAmount: "+trainAmount+" maxTrainAmount: "+maxTrainAmount+" skillMaxXp: "+skillMaxXp+
        //        " skillRemain: "+skillRemain+" skillRank: "+skillRank+" skillRankCost: "+skillRankCost+" nextLevelRankCost: "+nextLevelRankCost+" trainedSkillExp: "+trainedSkillExp, Utils.LogType.SkillTraining);
            
        //    chr.WriteToDisplay(trainer.Name + ": You have been trained " + skillName + ", go now and practice.");
        //    //int currentLevel = chr.Level; // get character level before training experience
        //    chr.Experience += (int)(trainAmount / 5); // give training experience
        //    //if(Rules.levelCheck(chr.Experience) > currentLevel) // compare level before and after training
        //    //{
        //    //    if (chr.Hits < chr.HitsMax || chr.Stamina < chr.StaminaMax)
        //    //    {
        //    //        chr.WriteToDisplay("You have earned enough experience for your next level! Type REST when you are at full health and stamina to advance.");
        //    //    }
        //    //    else
        //    //    {
        //    //        chr.WriteToDisplay("You have earned enough experience for your next level! Type REST to advance.");
        //    //    }
        //    //}
        //    return true;
        //}

        public override void MerchantShowBalance(Character chr)
        {
            if (this.interactiveType != Merchant.InteractiveType.Banker)
            {
                chr.WriteToDisplay(this.Name + ": I am not a banker.");
                return;
            }

            if (this.PlayersFlagged.Contains(chr.PlayerID))
            {
                chr.WriteToDisplay(this.Name + ": I am quite busy. You will have to come back later.");
                return;
            }

            chr.WriteToDisplay(this.Name + ": You have " + chr.bankGold + " coins in your account.");
        }
		
		public override string GetMerchantSpellList(Character chr) 
		{
            if (this.PlayersFlagged.Contains(chr.PlayerID))
            {
                return this.Name + ": I am quite busy. You will have to come back later.";                
            }

            int a;

			string spells = "";

			bool addSpell = false;

            List<string> teachList = new List<string>();

            foreach(Spell spell in Spell.spellDictionary.Values)
            {
                addSpell = false;
                if (spell.IsClassSpell(chr.BaseProfession))
                {
                    addSpell = true;
                }
                if (addSpell)
                {
                    if (Skills.GetSkillLevel(chr.magic) < spell.RequiredLevel)
                    {
                        addSpell = false;
                    }
                }
                if (chr.spellList.SpellIDExists(spell.SpellID))
                {
                    addSpell = false;
                }
                if (addSpell)
                {
                    teachList.Add(spell.Name + " (" + spell.SpellCommand + ", " + spell.TrainingPrice + "g)");
                }
            }
            for (a = 0; a < teachList.Count; a++)
            {
                if (a == teachList.Count - 1 && teachList.Count > 1)
                {
                    spells += "and " + teachList[a] + ".";
                }
                else if (teachList.Count > 1)
                {
                    spells += teachList[a] + ", ";
                }
                else
                {
                    spells += teachList[a] + ".";
                }
            }
			if(teachList.Count < 1)
			{
                return this.Name + ": I cannot teach you anything more at this time.";
			}
			else
			{
                return this.Name + ": I am willing to teach you " + spells;
			}
		}

		public override void MerchantAppraise(Character chr, string args)
		{
            if (this.PlayersFlagged.Contains(chr.PlayerID))
            {
                chr.WriteToDisplay(this.Name + ": I am quite busy. You will have to come back later.");
                return;
            }

            string[] sArgs = args.Split(" ".ToCharArray());

            if (sArgs[1].Equals("all"))
            {
                chr.WriteToDisplay(this.Name + ": Appraising is a fine art. I will appraise one item at a time for you.");
                return;
            }

            Item item;
            int countRequest = 1;
            string itemName = "";

            if (sArgs.Length == 2)
            {
                goto appraiseFirstItem;
            }

            try
            {
                countRequest = Convert.ToInt32(sArgs[1]);
                if(countRequest <= 0)
                {
                    countRequest = 1;
                }

                if (sArgs.Length >= 3)
                {
                    itemName = sArgs[2];
                }
                else
                {
                    chr.WriteToDisplay(this.Name + ": I cannot appraise something that I cannot see.");
                    return;
                }

                item = Map.getItemCopyFromCounter(this, countRequest, itemName);

                if (item == null)
                {
                    chr.WriteToDisplay(this.Name + ": I don't see a " + countRequest + GetNumberEnd(countRequest) + " " + itemName + " here.");
                    return;
                }
                else
                {
                    goto itemAppraisal;
                }
            }
            catch
            {
                goto appraiseFirstItem;
            }

        appraiseFirstItem:

			item = Map.getItemCopyFromCounter(chr, 1, sArgs[1]);

            if (item == null)
            {
                chr.WriteToDisplay(this.Name + ": I don't see a " + sArgs[1] + " here.");
                return;
            }

        itemAppraisal:
            string itmeffect = "";
            string itmSpell = "";
            string itmcharges = "";
            string itmspecial = "";
            string itmattuned = "";
            string itmvalue = "";

            if (item.spell > 0)
            {
                if (item.charges == 0) { itmcharges = ", but there are no charges remaining."; }
                if (item.charges > 1 && item.charges < 100) { itmcharges = " There are " + item.charges + " charges remaining."; }
                if (item.charges == 1) { itmcharges = " There is 1 charge remaining."; }
                if (item.charges == -1) { itmcharges = " The " + item.name + " has unlimited charges."; }

                itmSpell = " It contains the spell of " + Spell.GetSpell(item.spell).Name + "." + itmcharges;
            }

            System.Text.StringBuilder sb = new System.Text.StringBuilder(40);
            if (item.baseType == Globals.eItemBaseType.Figurine)
            {
                sb.AppendFormat(" The {0}'s avatar has " + item.figExp + " experience.", item.name);
            }
            if (item.combatAdds > 0)
            {
                sb.AppendFormat(" The combat adds are {0}.", item.combatAdds);
            }
            if (item.silver)
            {
                sb.AppendFormat(" The {0} is silver.", item.name);
            }
            if (item.blueglow)
            {
                sb.AppendFormat(" The {0} is emitting a faint blue glow.", item.name);
            }
            if (item.alignment != Globals.eAlignment.None)
            {
                sb.AppendFormat(" The {0} is {1}.", item.name, Utils.FormatEnumString(item.alignment.ToString()).ToLower());
            }
            itmspecial = sb.ToString();

            if (item.effectType.Length > 0)
            {
                string[] itmEffectType = item.effectType.Split(" ".ToCharArray());
                string[] itmEffectAmount = item.effectAmount.Split(" ".ToCharArray());

                if (itmEffectType.Length == 1 && Effect.GetEffectName((Effect.EffectType)Convert.ToInt32(itmEffectType[0])) != "")
                {
                    if (item.baseType == Globals.eItemBaseType.Bottle)
                    {
                        Bottle bottle = (Bottle)item;
                        itmeffect = Bottle.GetFluidDesc(bottle);
                        itmeffect += " The " + item.name + " is a potion of " + Effect.GetEffectName((Effect.EffectType)Convert.ToInt32(itmEffectType[0])) + ".";
                    }
                    else
                    {
                        itmeffect = " The " + item.name + " contains the enchantment of " + Effect.GetEffectName((Effect.EffectType)Convert.ToInt32(itmEffectType[0])) + ".";
                    }
                }
                else
                {
                    ArrayList itemEffectList = new ArrayList();

                    for (int a = 0; a < itmEffectType.Length; a++)
                    {
                        Effect.EffectType effectType = (Effect.EffectType)Convert.ToInt32(itmEffectType[a]);
                        if (Effect.GetEffectName(effectType).ToLower() != "unknown")
                        {
                            itemEffectList.Add(Effect.GetEffectName(effectType));
                        }
                    }

                    if (itemEffectList.Count > 0)
                    {
                        if (itemEffectList.Count > 1)
                        {
                            itmeffect = " The " + item.name + " contains the enchantments of";
                            for (int a = 0; a < itemEffectList.Count; a++)
                            {
                                if (a != itemEffectList.Count - 1)
                                {
                                    itmeffect += " " + (string)itemEffectList[a] + ",";
                                }
                                else
                                {
                                    itmeffect += " and " + (string)itemEffectList[a] + ".";
                                }
                            }
                        }
                        else if (itemEffectList.Count == 1)
                        {
                            if (item.baseType == Globals.eItemBaseType.Bottle)
                            {
                                itmeffect = " Inside the bottle is a potion of " + Effect.GetEffectName((Effect.EffectType)Convert.ToInt32(itmEffectType[0])) + ".";
                            }
                            else
                            {
                                itmeffect = " The " + item.name + " contains the enchantment of " + Effect.GetEffectName((Effect.EffectType)Convert.ToInt32(itmEffectType[0])) + ".";
                            }
                        }
                    }
                    else
                    {
                        if (item.baseType == Globals.eItemBaseType.Bottle)
                        {
                            Bottle bottle = (Bottle)item;
                            itmeffect = Bottle.GetFluidDesc(bottle);
                        }
                    }
                }

            }
            if (item.attunedID != 0)
            {
                if (item.attunedID == chr.PlayerID)
                {
                    itmattuned = " The " + item.name + " is soulbound to you.";
                }
                else
                {
                    itmattuned = " The " + item.name + " is soulbound to another individual.";
                }
            }
            if (item.coinValue == 1)
            {
                itmvalue = " The " + item.name + " is worth 1 coin.";
            }
            else if (item.coinValue > 1)
            {
                itmvalue = " The " + item.name + " is worth " + item.coinValue + " coins.";
            }
            else
            {
                itmvalue = " The " + item.name + " is worthless.";
            }
			chr.WriteToDisplay(this.Name+": We are looking at "+item.longDesc+"."+itmeffect+itmSpell+itmspecial+itmattuned+itmvalue);
			return;	
		}

        public override void MerchantCritique(Character chr, string skillName)
        {
            if (this.PlayersFlagged.Contains(chr.PlayerID))
            {
                chr.WriteToDisplay(this.Name + ": I am quite busy. You will have to come back later.");
                return;
            }

            Globals.eSkillType skillType = Globals.eSkillType.None;

            for (int a = 0; a < Enum.GetValues(skillType.GetType()).Length; a++)
            {
                if (Utils.FormatEnumString(Enum.GetValues(skillType.GetType()).GetValue(a).ToString().ToLower()) == skillName.ToLower())
                {
                    skillType = (Globals.eSkillType)Enum.GetValues(skillType.GetType()).GetValue(a);
                }
            }

            #region Found no skillType match
            if (skillType == Globals.eSkillType.None)
            {
                chr.WriteToDisplay(this.Name + ": I do not know how to critique " + Utils.FormatEnumString(skillName).ToLower() + ".");
                return;
            }
            #endregion

            #region Weapon skill critique requested, but this trainer is not a weapon trainer
            if ((skillType != Globals.eSkillType.Magic && skillType != Globals.eSkillType.Unarmed && skillType != Globals.eSkillType.Thievery) && this.trainerType != TrainerType.Weapon)
            {
                chr.WriteToDisplay(this.Name + ": I do not know how to critique " + Utils.FormatEnumString(skillType.ToString()).ToLower() + " skill.");
                return;
            }
            #endregion

            #region Thievery skill critique requested, but this trainer is not a thief
            if (skillType == Globals.eSkillType.Thievery && this.BaseProfession != ClassType.Thief)
            {
                chr.WriteToDisplay(this.Name + ": I do not know how to critique " + Utils.FormatEnumString(skillType.ToString()).ToLower() + " skill.");
                return;
            } 
            #endregion

            #region Unarmed skill critique requested, but this trainer is not a martial arts trainer
            if (skillType == Globals.eSkillType.Unarmed && this.trainerType != TrainerType.Martial_Arts)
            {
                chr.WriteToDisplay(this.Name + ": I do not know how to critique the martial arts.");
                return;
            }
            #endregion

            #region Magic skill critique requested, but this trainer type is not a spell trainer OR is not the same class
            if (skillType == Globals.eSkillType.Magic && (this.BaseProfession != chr.BaseProfession || this.trainerType != TrainerType.Spell))
            {
                string magictype = "";
                switch (chr.BaseProfession)
                {
                    case ClassType.Wizard:
                        magictype = "wizardry";
                        break;
                    case ClassType.Thaumaturge:
                        magictype = "thaumaturgy";
                        break;
                    case ClassType.Thief:
                        magictype = "shadow magic";
                        break;
                    default:
                        magictype = "magic";
                        break;
                }
                chr.WriteToDisplay(this.Name + ": I am not skilled in the art of " + magictype + ".");
                return;
            }
            #endregion

            long skillExp = chr.GetSkillExperience(skillType);
            long highSkillExp = chr.GetHighSkillExperience(skillType);

            if (this.GetSkillExperience(skillType) < skillExp)
            {
                chr.WriteToDisplay(this.Name + ": You are more skilled with " + skillType.ToString() + " skill than I.");
                return;
            }
            if (skillExp == -1 || skillType == Globals.eSkillType.None)
            {
                chr.WriteToDisplay(this.Name + ": I cannot critique that skill.");
                return;
            }

            long skillMaxExp = Skills.GetSkillToMax(Skills.GetSkillLevel(skillExp)); // get skill to max
            long skillIntoLevel = skillMaxExp - skillExp; // get skill into current skill level
            long skillRemain = Skills.GetSkillToNext(Skills.GetSkillLevel(skillExp)) - skillIntoLevel; // get skill remaining in this skill level
            int skillPercent = (int)(((float)skillRemain / (float)Skills.GetSkillToNext(Skills.GetSkillLevel(skillExp))) * 10);
            string skillTitle = Skills.GetSkillTitle(skillType, chr.BaseProfession, skillExp, chr.gender);
            string skillRank = SkillRankToString(skillPercent);

            string skillMessage = this.Name + ": You have achieved the " + skillRank + " of " + skillTitle + " in your " + Utils.FormatEnumString(skillType.ToString()).ToLower() + " skill";

            if (skillTitle.ToLower() == "untrained")
            {
                skillMessage = this.Name + ": You have not been trained with your " + Utils.FormatEnumString(skillType.ToString()).ToLower() + " skill.";
                chr.WriteToDisplay(skillMessage);
                return;
            }

            if (skillExp < highSkillExp)
            {
                skillMaxExp = Skills.GetSkillToMax(Skills.GetSkillLevel(highSkillExp));
                skillIntoLevel = skillMaxExp - highSkillExp;
                skillRemain = Skills.GetSkillToNext(Skills.GetSkillLevel(highSkillExp)) - skillIntoLevel;
                skillPercent = (int)(((float)skillRemain / (float)Skills.GetSkillToNext(Skills.GetSkillLevel(highSkillExp))) * 10);
                skillRank = SkillRankToString(skillPercent);
                skillTitle = Skills.GetSkillTitle(skillType, chr.BaseProfession, highSkillExp, chr.gender);
                skillMessage += ", however you are below your peak of " + skillRank + " of " + skillTitle + ".";

            }
            else
            {
                skillMessage += ".";
            }

            chr.WriteToDisplay(skillMessage);
        }

		public static string SkillRankToString(int rank)
		{
			string skillrank = "rank";
			switch(rank)
			{
				case 1:
					skillrank = "first rank";
					break;
				case 2:
					skillrank = "second rank";
					break;
				case 3:
					skillrank = "third rank";
					break;
				case 4:
					skillrank = "fourth rank";
					break;
				case 5:
					skillrank = "fifth rank";
					break;
				case 6:
					skillrank = "sixth rank";
					break;
				case 7:
					skillrank = "seventh rank";
					break;
				case 8:
					skillrank = "eighth rank";
					break;
				case 9:
					skillrank = "ninth rank";
					break;
				case 10:
					skillrank = "ninth rank";
					break;
				default:
					skillrank = "rank";
					break;
			}
			return skillrank;
		}
	}
}

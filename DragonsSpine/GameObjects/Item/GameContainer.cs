using System;
using System.Collections.Generic;
using System.Text;

namespace DragonsSpine.GameObjects
{
    public class GameContainer : GameItem
    {
        /// <summary>
        /// Holds the items that are in the container.
        /// </summary>
        protected List<GameItem> m_items;

        /// <summary>
        /// Gets the list of items that are in the container.
        /// </summary>
        public List<GameItem> Items
        {
            get { return m_items; }
        }

        public GameContainer()
            : base()
        {
            m_items = new List<GameItem>();
        }

        internal static void LoadGameContainer(ref GameContainer container, System.Data.DataRow dr)
        {
            GameItem item = container as GameItem;
            GameItem.LoadGameItem(ref item, dr);
            container = item as GameContainer;
        }
    }
}

using System;
using System.Collections.Generic;

namespace DragonsSpine.GameObjects
{
    public class GameBook : GameItem
    {
        /// <summary>
        /// Holds the type of book the object is.
        /// </summary>
        protected Globals.eBookType m_bookType;

        /// <summary>
        /// Holds the current page number.
        /// </summary>
        protected short m_currentPage;

        /// <summary>
        /// Holds the maximum amount of pages in the book.
        /// </summary>
        protected short m_maxPages;

        /// <summary>
        /// Holds all the pages in the book.
        /// </summary>
        protected List<string> m_pages;
        public short CurrentPage
        {
            get { return m_currentPage; }
            set { m_currentPage = value; }
        }
        public short MaxPages
        {
            get { return m_maxPages; }
            set { m_maxPages = value; }
        }
        public Globals.eBookType BookType
        {
            get { return m_bookType; }
        }
        public List<string> Pages
        {
            get { return m_pages; }
            set { m_pages = value; }
        }
        public GameBook()
            : base()
        {
            m_bookType = Globals.eBookType.None;
            m_currentPage = 0;
            m_maxPages = 0;
            m_pages = new List<string>();
        }

        public static void LoadGameBook(ref GameBook gitem, System.Data.DataRow dr)
        {
            GameItem item = gitem as GameItem;
            GameItem.LoadGameItem(ref item, dr);
            gitem = item as GameBook;
            gitem.m_bookType = (Globals.eBookType)Enum.Parse(typeof(Globals.eBookType), dr["bookType"].ToString());
            gitem.m_maxPages = Convert.ToInt16(dr["maxPages"]);
            gitem.m_pages = new List<string>();
            string[] pageArray = dr["pages"].ToString().Split(Protocol.ISPLIT.ToCharArray());
            foreach (string page in pageArray)
                gitem.m_pages.Add(page);

        }

        public string ReadPage()
        {
            // Will read the same page over and over again

            int mp = m_pages.Count / 2; // do this to filter out the blank pages

            if (m_currentPage + 1 > mp)
            {
                m_currentPage = 0;
            }

            if (m_currentPage < 0)
            {
                m_currentPage = 0;
            }

            return m_pages[m_currentPage];
        }

        public string NextPage()
        {
            if (m_currentPage != m_maxPages)
            {
                m_currentPage++;
                return m_pages[m_currentPage];
            }
            else
            {
                return "You are at the end of the book.";
            }

        }

        public string PreviousPage()
        {
            if (m_currentPage > 1)
            {
                m_currentPage--;
                return m_pages[m_currentPage];
            }
            else
            {
                return "You are at the beginning of the book.";
            }
        }
    }
}

DROP TABLE [PlayerSack]

CREATE TABLE [PlayerSack] 
(
	[SackID]	[int]	IDENTITY NOT NULL PRIMARY KEY NONCLUSTERED ,
	[PlayerID]	[int]   NOT NULL REFERENCES Player(PlayerID) ,
	[SackSlot]	[int]	NULL ,
	[SackItem]	[int]	NULL ,
	[Attuned]	[int]	NULL ,
	[SackGold]	[float]	NULL ,
	[Special]	[nvarchar] (255) NULL ,
	[CoinValue] [float] NULL ,
	[Charges]	[int] NULL ,
	[Venom]		[int] NULL ,
	[WillAttune][bit] NULL ,		
	[FigExp]	[bigint] NULL
) 
ON [PRIMARY]
GO






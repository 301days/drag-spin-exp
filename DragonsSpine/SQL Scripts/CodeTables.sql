DROP TABLE [BaseTypeCode]

CREATE TABLE [AlignCode] 
(
	[AlignCode] 		[int] NULL ,
	[Name] 			[nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
	[Description] 	[nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL

) 
ON [PRIMARY]
GO




ItemTypeCode
WearLocationCode
BlockRankCode
EffectTypeCode
LootTypeCode
NpcTypeCode
BaseTypeCode
CharacterClassCode
AlignCode

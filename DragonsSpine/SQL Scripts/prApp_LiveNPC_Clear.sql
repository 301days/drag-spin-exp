
/****** Object:  StoredProcedure [dbo].[prApp_LiveNPC_Clear]    Script Date: 08/31/2015 22:40:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


----------------------------------------------------------------------------
-- Clear all entries from the LiveNPC table
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_LiveNPC_Clear]

AS

DELETE	LiveNPC

GO


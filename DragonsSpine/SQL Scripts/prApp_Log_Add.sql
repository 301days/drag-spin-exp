USE [DragSpinDB]

/****** Object:  StoredProcedure [dbo].[prApp_Log_Add] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prApp_Log_Add]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[prApp_Log_Add]
GO

/****** Object:  StoredProcedure [dbo].[prApp_Log_Add] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[prApp_Log_Add] 
	@logtype nvarchar(50) = '', 
	@message nvarchar(MAX) = ''
AS

	INSERT Log(logtype, message)
	VALUES (@logtype, @message);
	
GO



/****** Object:  StoredProcedure [dbo].[prApp_LivePC_Clear]    Script Date: 08/31/2015 22:40:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


----------------------------------------------------------------------------
-- Clear all entries from the LivePC table
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_LivePC_Clear]

AS

DELETE	LivePC

GO


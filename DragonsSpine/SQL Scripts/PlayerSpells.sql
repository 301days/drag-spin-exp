DROP TABLE [PlayerSpells]

CREATE TABLE [PlayerSpells] 
(
	[SpellListID]	[int]	IDENTITY NOT NULL PRIMARY KEY NONCLUSTERED,
	[PlayerID]	[int]   NOT NULL REFERENCES Player(PlayerID),
	[SpellSlot]	[int]	NULL,
	[SpellID]	[int]	NULL,
	[ChantString]	[nvarchar] (255)	NULL,
) 
ON [PRIMARY]
GO






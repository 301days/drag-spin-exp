
/****** Object:  StoredProcedure [dbo].[prApp_LiveCell_Clear] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


----------------------------------------------------------------------------
-- Clear all entries from the LiveCell table
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_LiveCell_Clear]

AS

DELETE	LiveCell

GO


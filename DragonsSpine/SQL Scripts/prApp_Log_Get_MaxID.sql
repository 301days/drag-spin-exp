USE [DragSpinDB]

/****** Object:  StoredProcedure [dbo].[prApp_Log_Get_MaxID] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prApp_Log_Get_MaxID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[prApp_Log_Get_MaxID]
GO

/****** Object:  StoredProcedure [dbo].[prApp_Log_Get_MaxID] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------
-- Select log entry with the max ID
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Log_Get_MaxID]

AS

SELECT TOP 1 *
  FROM Log
  ORDER BY id DESC

GO


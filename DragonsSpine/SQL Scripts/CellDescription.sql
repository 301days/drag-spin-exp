DROP TABLE [CellInfo$]

CREATE TABLE [CellInfo$] 
(
	[CellInfoID] 	[int] NULL ,
	[LandID] 		[int] NULL ,
	[MapID] 		[int] NULL ,
	[Xcord] 		[int] NULL ,
	[Ycord] 		[int] NULL ,
	[CellDesc] 		[nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
	
) 
ON [PRIMARY]
GO



<cellDesc>You are standing on a dock.</cellDesc>
<ycord>34</ycord>
<xcord>27</xcord>
DROP TABLE [NPC]

CREATE TABLE [NPC] 
(
	[NpcID] 		[int] NULL ,
	[Name] 			[nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
	[MovementString] 	[nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
	[Special] 		[nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
	[NpcTypeCode] 		[int] NULL ,
	[BaseTypeCode] 		[int] NULL ,
	[Gold] 			[int] NULL ,
	[CharacterClassCode] 	[int] NULL ,
	[Experience] 		[int] NULL ,
	[HitsMax] 		[int] NULL ,
	[AlignCode] 		[int] NULL ,
	[Stamina] 		[int] NULL ,
	[Speed] 		[int] NULL ,
	[Strength] 		[int] NULL ,
	[Dexterity] 		[int] NULL ,
	[Intelligence] 		[int] NULL ,
	[Wisdom] 		[int] NULL ,
	[Constitution] 		[int] NULL ,
	[Charisma] 		[int] NULL ,
	[Unarmed] 		[int] NULL ,
	[LootVeryCommon] 	[int] NULL ,
	[SpawnArmorItemID]	[int] NULL ,
	[SpawnLeftHandItemID] 	[int] NULL ,
	[spawnRightHandItemID] 	[int] NULL ,
	[MaceSkill] 		[bigint] NULL ,
	[BowSkill] 		[bigint] NULL ,
	[DaggerSkill] 		[bigint] NULL ,
	[FlailSkill]		[bigint] NULL ,
	[RapierSkill] 		[bigint] NULL ,
	[TwohandedSkill] 	[bigint] NULL ,
	[StaffSkill] 		[bigint] NULL ,
	[ShurikenSkill] 	[bigint] NULL ,
	[SwordSkill] 		[bigint] NULL ,
	[ThreestaffSkill]	[bigint] NULL ,
	[HalberdSkill] 		[bigint] NULL ,
	[ThieverySkill]		[bigint] NULL ,
	[MagicSkill] 		[bigint] NULL ,
	[Difficulty] 			[int] NULL ,
	[IsAnimal] 		[bit] NOT NULL  DEFAULT (0),
	[TanningResult]	[varchar] (255) NULL ,
	[IsUndead] 		[bit] NOT NULL  DEFAULT (0),
	[IsHidden] 		[bit] NOT NULL  DEFAULT (0),
	[CanFly] 		[bit] NOT NULL  DEFAULT (0),
	[CanBreatheWater]	[bit] NOT NULL  DEFAULT (0),
	[IsLairCritter]		[bit] NOT NULL  DEFAULT (0),
	[IsMovable] 		[bit] NOT NULL  DEFAULT (0)





) 
ON [PRIMARY]
GO





		<NPC>
			<!-- Mama -->
			<npcID>2001</npcID>
			<npcType>0</npcType>
			<baseType>0</baseType>
			<Name>dragon</Name>
			<isAnimal>true</isAnimal>
			<isundead>false</isundead>
			<ishidden>false</ishidden>
			<canFly>true</canFly>
			<breatheWater>true</breatheWater>
			<moveString>the beating of giant wings...</moveString>
			<gold>0</gold>
			<lair>1</lair>
			<charClass>fi</charClass>
			<exp>55000</exp>
			<hits>1900</hits>
			<hitsmax>1900</hitsmax>
			<align>2</align>
			<stamina>150</stamina>
			<speed>3</speed>
			<strength>32</strength>
			<dexterity>16</dexterity>
			<intelligence>19</intelligence>
			<wisdom>19</wisdom>
			<constitution>25</constitution>
			<charisma>17</charisma>
			<Movable>1</Movable>
			<unarmed>1638400</unarmed>
			<lootVeryCommon>0</lootVeryCommon>
			<spawnArmor>8110</spawnArmor>
			<spawnLeftHand>0</spawnLeftHand>
			<spawnRightHand>0</spawnRightHand>
			<special>silver</special>
			<mace>3</mace>
			<bow>3</bow>
			<dagger>3</dagger>
			<rapier>3</rapier>
			<twoHanded>3</twoHanded>
			<staff>3</staff>
			<shuriken>3</shuriken>
			<sword>3</sword>
			<threestaff>3</threestaff>
			<halberd>3</halberd>
			<thievery>3</thievery>
			<magic>500000</magic>
			<diff>7</diff>
		</NPC>
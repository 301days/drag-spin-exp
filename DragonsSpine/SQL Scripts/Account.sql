DROP TABLE [Account]

CREATE TABLE [Account] 
(
	[AccountID]	[int]	IDENTITY NOT NULL PRIMARY KEY NONCLUSTERED,
	[AccountName]	[nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Password]	[nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IPAddress]	[nvarchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
) 
ON [PRIMARY]
GO






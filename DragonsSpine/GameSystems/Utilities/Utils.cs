namespace DragonsSpine
{
    using System;
	using System.Reflection;
	using System.Collections;
    using System.Collections.Generic;
	using System.IO;
	using System.Text;

    public class Utils
    {
        public enum LogType { Unknown, SystemGo, SystemFailure, SystemFatalError, SystemWarning, Exception, Connection, Login, Logout,
            Disconnect, Timeout, SkillGainCombat, SkillGainNonCombat, SkillGainRisk, SkillTraining, ExperienceMeleeKill, ExperienceLevelGain,
            ExperienceSpellKill, ExperienceSpellCasting, ExperienceSpellAEKill, ExperienceTraining, DeathCreature, DeathLair, DeathPlayer,
            CommandFailure, CommandImmortal, CommandAllPlayer, LootRare, LootVeryRare, LootBelt, LootAlways,
            SpellDamageToCreature, SpellDamageToPlayer, QuestCompletion, 
            SpellDamageFromMapEffect, ItemFigurineUse, ItemAttuned, CombatDamageToCreature, CombatDamageToPlayer,
            CriticalCombatDamageToCreature, CriticalCombatDamageToPlayer, MerchantBuy, MerchantSell, MerchantTanning,
            SpellBeneficialFromCreature, SpellHarmfulFromCreature, SpellBeneficialFromPlayer, SpellHarmfulFromPlayer,
            SpellWarmingFromCreature, SpellWarmingFromPlayer, DeletePlayer, DeleteAccount, DisplayCreature, DisplayPlayer, Karma, Mark,
            CoinLogging, Info,PlayerChat }
        public static object lockObject = new object();
        public static int[] ConvertStringToIntArray(string text)
        {
            if (text == "")
            {
                return null;
            }

            try
            {
                string[] list = text.Split(Protocol.ASPLIT.ToCharArray());

                int[] intarray = new int[list.Length];

                for (int a = 0; a < intarray.Length; a++)
                {
                    intarray[a] = Convert.ToInt32(list[a]);
                }

                return intarray;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return null;
            }
        }

        public static string ConvertIntArrayToString(int[] array)
        {
            if (array == null)
            {
                return "";
            }

            try
            {
                string list = "";

                for (int a = 0; a < array.Length; a++)
                {
                    list += Convert.ToString(array[a]) + Protocol.ASPLIT;
                }

                if (list.Length > 0)
                {
                    list = list.Substring(0, list.Length - Protocol.ASPLIT.Length);
                }

                return list;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return "";
            }
        }

        public static string ConvertListToString(List<string> generic)
        {
            try
            {
                string list = "";
                for (int a = 0; a < generic.Count; a++)
                {
                    list += Convert.ToString(generic[a]) + Protocol.ASPLIT;
                }

                if (list.Length > 0)
                {
                    list = list.Substring(0, list.Length - Protocol.ASPLIT.Length);
                }
                return list;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return "";
            }
        }

        public static string ConvertListToString(List<short> generic)
        {
            try
            {
                string list = "";
                for (int a = 0; a < generic.Count; a++)
                {
                    list += Convert.ToString(generic[a]) + Protocol.ASPLIT;
                }

                if (list.Length > 0)
                {
                    list = list.Substring(0, list.Length - Protocol.ASPLIT.Length);
                }
                return list;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return "";
            }
        }

        public static string ConvertListToString(List<int> generic)
        {
            try
            {
                string list = "";
                for (int a = 0; a < generic.Count; a++)
                {
                    list += Convert.ToString(generic[a]) + Protocol.ASPLIT;
                }

                if (list.Length > 0)
                {
                    list = list.Substring(0, list.Length - Protocol.ASPLIT.Length);
                }
                return list;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return "";
            }
        }

        public static string FormatEnumString(string enumString)
        {
            enumString = enumString.Replace("__", "'");
            enumString = enumString.Replace("_", " ");
            return enumString;
        }

        public static string ParseEmote(string emoteString)
        {
            if (emoteString.Contains("{") && emoteString.Contains("}"))
            {
                return emoteString.Substring(emoteString.IndexOf("{") + 1, (emoteString.IndexOf("}") - emoteString.IndexOf("{")) - 1);
            }
            else return "";
        }

        public static void Log(string message, LogType logType)
        {

            //System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
            lock (lockObject)
            {
                string date = DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Year.ToString().Substring(2);
                string directory = Utils.GetStartupPath() + Path.DirectorySeparatorChar + "Logs" + Path.DirectorySeparatorChar + date;

                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);

                directory += Path.DirectorySeparatorChar;

                try
                {
                    FileStream file = new FileStream(directory + "DS_" + date + "_" + logType.ToString() + ".log", FileMode.Append, FileAccess.Write);
                    StreamWriter rw = new StreamWriter(file);
                    rw.WriteLine(DateTime.Now.ToString() + ": {" + logType.ToString() + "} " + message);
                    rw.Close();
                    file.Close();
                }
                catch (System.IO.IOException)
                {
                    //Console.WriteLine(DateTime.Now.ToString() + ": Threw an IOException at Utils.Log(" + message + ", " + logType.ToString() + ").");
                    //Console.Write("> ");
                }
                catch (Exception)
                {
                    //Console.WriteLine(DateTime.Now.ToString() + ": Threw an Exception at Utils.Log(" + message + ", " + logType.ToString() + ").");
                    //Console.Write("> ");
                }

                //
                try
                {
                    int result = DAL.DBLog.addLogEntry(message, logType.ToString());
                    if (result < 1)
                        Console.WriteLine(DateTime.Now.ToString() + ": Utils.Log(" + message + ", " + logType.ToString() + ") database entry result is " + 
                                          result + ".");
                }
                catch (System.IO.IOException)
                {
                    Console.WriteLine(DateTime.Now.ToString() + ": Threw an IOException at Utils.Log(" + message + ", " + logType.ToString() + ").");
                    Console.Write("> ");
                }
                catch (Exception)
                {
                    Console.WriteLine(DateTime.Now.ToString() + ": Threw an Exception at Utils.Log(" + message + ", " + logType.ToString() + ").");
                    Console.Write("> ");
                }

                switch (logType)
                {
                    case LogType.Connection:
                    case LogType.Login:
                    case LogType.Logout:
                    case LogType.SystemFailure:
                    case LogType.SystemFatalError:
                    case LogType.SystemGo:
                    case LogType.SystemWarning:
                    case LogType.Exception:
                    case LogType.Info:
                        Console.WriteLine(DateTime.Now.ToString() + ": {" + logType.ToString() + "} " + message);
                        Console.Write("> ");
                        break;
                    default:
                        break;
                }
            }
        }
        public static string GetStartupPath()
        {
            Assembly executingAssembly = Assembly.GetExecutingAssembly();
            string exeName = Path.GetFileNameWithoutExtension(executingAssembly.Location);
            return (Path.GetDirectoryName(executingAssembly.Location) + Path.DirectorySeparatorChar);
        }
        public static void LogException(Exception e)
        {
            //System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
            lock (lockObject)
            {
                string date = DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Year.ToString().Substring(2);
                string directory = string.Concat(new object[] { GetStartupPath(), Path.DirectorySeparatorChar, "Logs", Path.DirectorySeparatorChar, date });
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                FileStream file = new FileStream((directory + Path.DirectorySeparatorChar) + "DS_" + date + "_EXCEPTIONS.log", FileMode.Append, FileAccess.Write);
                StreamWriter rw = new StreamWriter(file);
                rw.WriteLine(DateTime.Now.ToString() + ": EXCEPTION");
                rw.WriteLine("TargetSite: " + e.TargetSite.ToString());
                rw.WriteLine("Message: " + e.Message.ToString());
                rw.WriteLine("StackTrace: " + e.StackTrace.ToString());
                rw.WriteLine("**** COMMENTS ****");
                rw.WriteLine("");
                rw.Close();
                file.Close();
            }
        }

		public static bool SetFieldValue(object obj, String fieldName, object val) {
            Type type = obj.GetType();
            FieldInfo field = type.GetField(fieldName);
            if (field == null) { return false; }
            field.SetValue(obj, Convert.ChangeType(val, field.FieldType));
			return true;
		}
	}

	public class IntStringMap
	{
        public ArrayList strings = new ArrayList(), ints = new ArrayList();

        public int Count
        {
            get { return ints.Count; }
        }

        public int IndexOf(string str)
        {
            return strings.IndexOf(str);
        }

        public int IndexOf(int integer)
        {
            return ints.IndexOf(integer);
        }

		public void Add(int num, string str)
		{
			strings.Add(str);
			ints.Add(num);
		}

		public string GetString(int num)
		{
			for(int a = 0; a <= ints.Count - 1; a++)
			{
				if(a == num)
				{
					return (string)strings[a];
				}
			}
			return null;
		}

		public bool SpellIDExists(int spellID)
		{
            return ints.Contains(spellID);
            //foreach(int num in ints)
            //{
            //    if(num == spellID){return true;}
            //}
            //return false;
		}

		public bool Exists(string str)
		{
            return strings.Contains(str);
            //foreach(string sz in strings)
            //{
            //    if(sz.ToLower() == str)
            //        return true;
            //}
            //return false;
		}

		public int GetInt(string str)
		{
			int count = 0;
			foreach(string tmpStr in strings)
			{
				if(tmpStr.ToLower() == str.ToLower())
				{
					return (int)ints[count];
				}
				count++;
			}
			return 0;
		}

		public int GetSpellID(int num) 
		{
			try
			{
                for (int a = 0; a < ints.Count; a++)
                {
                    if (a == num)
                    {
                        return (int)ints[a];
                    }
                }
				return -1;
			}
			catch(Exception e)
			{
                Utils.LogException(e);
				return -1;
			}
		}									 
	}
}

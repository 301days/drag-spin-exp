namespace DragonsSpine
{
	using System;
	using System.Collections;
    using System.Collections.Generic;
	using System.Reflection;
	using System.IO;
	using System.Net.Sockets;
	using System.Collections.Specialized;

	public class Rules
	{
		public static RandMT dice = new RandMT();

        #region Random Number Generator (New)
        public sealed class RandMT
        {
            #region Constants -------------------------------------------------------

            // Period parameters.
            private const int N = 624;
            private const int M = 397;
            private const uint MATRIX_A = 0x9908b0dfU;   // constant vector a
            private const uint UPPER_MASK = 0x80000000U; // most significant w-r bits
            private const uint LOWER_MASK = 0x7fffffffU; // least significant r bits
            private const int MAX_RAND_INT = 0x7fffffff;

            #endregion Constants

            #region Instance Variables ----------------------------------------------

            // mag01[x] = x * MATRIX_A  for x=0,1
            private uint[] mag01 = { 0x0U, MATRIX_A };

            // the array for the state vector
            private uint[] mt = new uint[N];

            // mti==N+1 means mt[N] is not initialized
            private int mti = N + 1;

            #endregion Instance Variables

            #region Constructors ----------------------------------------------------

            /// <summary>
            /// Creates a random number generator using the time of day in milliseconds as
            /// the seed.
            /// </summary>
            public RandMT()
            {
                init_genrand((uint)DateTime.Now.Millisecond);
            }

            /// <summary>
            /// Creates a random number generator initialized with the given seed. 
            /// </summary>
            /// <param name="seed">The seed.</param>
            public RandMT(int seed)
            {
                init_genrand((uint)seed);
            }

            /// <summary>
            /// Creates a random number generator initialized with the given array.
            /// </summary>
            /// <param name="init">The array for initializing keys.</param>
            public RandMT(int[] init)
            {
                uint[] initArray = new uint[init.Length];
                for (int i = 0; i < init.Length; ++i)
                    initArray[i] = (uint)init[i];

                init_by_array(initArray, (uint)initArray.Length);
            }

            #endregion Constructors

            #region Properties ------------------------------------------------------

            /// <summary>
            /// Gets the maximum random integer value. All random integers generated
            /// by instances of this class are less than or equal to this value. This
            /// value is <c>0x7fffffff</c> (<c>2,147,483,647</c>).
            /// </summary>
            public static int MaxRandomInt
            {
                get
                {
                    return 0x7fffffff;
                }
            }

            #endregion Properties

            #region Member Functions ------------------------------------------------

            /// <summary>
            /// Returns a random integer greater than or equal to zero and
            /// less than or equal to <c>MaxRandomInt</c>. 
            /// </summary>
            /// <returns>The next random integer.</returns>
            public int Next()
            {
                return genrand_int31();
            }

            /// <summary>
            /// Returns a positive random integer less than the specified maximum.
            /// </summary>
            /// <param name="maxValue">The maximum value. Must be greater than zero.</param>
            /// <returns>A positive random integer less than or equal to <c>maxValue</c>.</returns>
            public int Next(int maxValue)
            {
                return Next(0, maxValue);
            }

            /// <summary>
            /// Returns a random integer within the specified range.
            /// </summary>
            /// <param name="minValue">The lower bound.</param>
            /// <param name="maxValue">The upper bound.</param>
            /// <returns>A random integer greater than or equal to <c>minValue</c>, and less than
            /// or equal to <c>maxValue</c>.</returns>
            public int Next(int minValue, int maxValue)
            {
                if (minValue == maxValue)
                    return minValue;
                if (minValue > maxValue)
                {
                    int tmp = maxValue;
                    maxValue = minValue;
                    minValue = tmp;
                }
                int test = (int)(Math.Floor((maxValue - minValue) * genrand_real2() + minValue));
                return test;
            }

            /// <summary>
            /// Returns a random number between 0.0 and 1.0.
            /// </summary>
            /// <returns>A single-precision floating point number greater than or equal to 0.0, 
            /// and less than 1.0.</returns>
            public float NextFloat()
            {
                return (float)genrand_real2();
            }

            /// <summary>
            /// Returns a random number greater than or equal to zero, and either strictly
            /// less than one, or less than or equal to one, depending on the value of the
            /// given boolean parameter.
            /// </summary>
            /// <param name="includeOne">
            /// If <c>true</c>, the random number returned will be 
            /// less than or equal to one; otherwise, the random number returned will
            /// be strictly less than one.
            /// </param>
            /// <returns>
            /// If <c>includeOne</c> is <c>true</c>, this method returns a
            /// single-precision random number greater than or equal to zero, and less
            /// than or equal to one. If <c>includeOne</c> is <c>false</c>, this method
            /// returns a single-precision random number greater than or equal to zero and
            /// strictly less than one.
            /// </returns>
            public float NextFloat(bool includeOne)
            {
                if (includeOne)
                {
                    return (float)genrand_real1();
                }
                return (float)genrand_real2();
            }

            /// <summary>
            /// Returns a random number greater than 0.0 and less than 1.0.
            /// </summary>
            /// <returns>A random number greater than 0.0 and less than 1.0.</returns>
            public float NextFloatPositive()
            {
                return (float)genrand_real3();
            }

            /// <summary>
            /// Returns a random number between 0.0 and 1.0.
            /// </summary>
            /// <returns>A double-precision floating point number greater than or equal to 0.0, 
            /// and less than 1.0.</returns>
            public double NextDouble()
            {
                return genrand_real2();
            }

            /// <summary>
            /// Returns a random number greater than or equal to zero, and either strictly
            /// less than one, or less than or equal to one, depending on the value of the
            /// given boolean parameter.
            /// </summary>
            /// <param name="includeOne">
            /// If <c>true</c>, the random number returned will be 
            /// less than or equal to one; otherwise, the random number returned will
            /// be strictly less than one.
            /// </param>
            /// <returns>
            /// If <c>includeOne</c> is <c>true</c>, this method returns a
            /// single-precision random number greater than or equal to zero, and less
            /// than or equal to one. If <c>includeOne</c> is <c>false</c>, this method
            /// returns a single-precision random number greater than or equal to zero and
            /// strictly less than one.
            /// </returns>
            public double NextDouble(bool includeOne)
            {
                if (includeOne)
                {
                    return genrand_real1();
                }
                return genrand_real2();
            }

            /// <summary>
            /// Returns a random number greater than 0.0 and less than 1.0.
            /// </summary>
            /// <returns>A random number greater than 0.0 and less than 1.0.</returns>
            public double NextDoublePositive()
            {
                return genrand_real3();
            }

            /// <summary>
            /// Generates a random number on <c>[0,1)</c> with 53-bit resolution.
            /// </summary>
            /// <returns>A random number on <c>[0,1)</c> with 53-bit resolution</returns>
            public double Next53BitRes()
            {
                return genrand_res53();
            }

            /// <summary>
            /// Reinitializes the random number generator using the time of day in
            /// milliseconds as the seed.
            /// </summary>
            public void Initialize()
            {
                init_genrand((uint)DateTime.Now.Millisecond);
            }


            /// <summary>
            /// Reinitializes the random number generator with the given seed.
            /// </summary>
            /// <param name="seed">The seed.</param>
            public void Initialize(int seed)
            {
                init_genrand((uint)seed);
            }

            /// <summary>
            /// Reinitializes the random number generator with the given array.
            /// </summary>
            /// <param name="init">The array for initializing keys.</param>
            public void Initialize(int[] init)
            {
                uint[] initArray = new uint[init.Length];
                for (int i = 0; i < init.Length; ++i)
                    initArray[i] = (uint)init[i];

                init_by_array(initArray, (uint)initArray.Length);
            }


            #region Methods ported from C -------------------------------------------

            // initializes mt[N] with a seed
            private void init_genrand(uint s)
            {
                mt[0] = s & 0xffffffffU;
                for (mti = 1; mti < N; mti++)
                {
                    mt[mti] =
                      (uint)(1812433253U * (mt[mti - 1] ^ (mt[mti - 1] >> 30)) + mti);
                    // See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. 
                    // In the previous versions, MSBs of the seed affect   
                    // only MSBs of the array mt[].                        
                    // 2002/01/09 modified by Makoto Matsumoto             
                    mt[mti] &= 0xffffffffU;
                    // for >32 bit machines
                }
            }

            // initialize by an array with array-length
            // init_key is the array for initializing keys 
            // key_length is its length
            private void init_by_array(uint[] init_key, uint key_length)
            {
                int i, j, k;
                init_genrand(19650218U);
                i = 1; j = 0;
                k = (int)(N > key_length ? N : key_length);
                for (; k > 0; k--)
                {
                    mt[i] = (uint)((uint)(mt[i] ^ ((mt[i - 1] ^ (mt[i - 1] >> 30)) * 1664525U)) + init_key[j] + j); /* non linear */
                    mt[i] &= 0xffffffffU; // for WORDSIZE > 32 machines
                    i++; j++;
                    if (i >= N) { mt[0] = mt[N - 1]; i = 1; }
                    if (j >= key_length) j = 0;
                }
                for (k = N - 1; k > 0; k--)
                {
                    mt[i] = (uint)((uint)(mt[i] ^ ((mt[i - 1] ^ (mt[i - 1] >> 30)) * 1566083941U)) - i); /* non linear */
                    mt[i] &= 0xffffffffU; // for WORDSIZE > 32 machines
                    i++;
                    if (i >= N) { mt[0] = mt[N - 1]; i = 1; }
                }

                mt[0] = 0x80000000U; // MSB is 1; assuring non-zero initial array
            }

            // generates a random number on [0,0xffffffff]-interval
            uint genrand_int32()
            {
                uint y;
                if (mti >= N)
                { /* generate N words at one time */
                    int kk;

                    if (mti == N + 1)   /* if init_genrand() has not been called, */
                        init_genrand(5489U); /* a default initial seed is used */

                    for (kk = 0; kk < N - M; kk++)
                    {
                        y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
                        mt[kk] = mt[kk + M] ^ (y >> 1) ^ mag01[y & 0x1U];
                    }
                    for (; kk < N - 1; kk++)
                    {
                        y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
                        mt[kk] = mt[kk + (M - N)] ^ (y >> 1) ^ mag01[y & 0x1U];
                    }
                    y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
                    mt[N - 1] = mt[M - 1] ^ (y >> 1) ^ mag01[y & 0x1U];

                    mti = 0;
                }

                y = mt[mti++];

                // Tempering
                y ^= (y >> 11);
                y ^= (y << 7) & 0x9d2c5680U;
                y ^= (y << 15) & 0xefc60000U;
                y ^= (y >> 18);

                return y;
            }

            // generates a random number on [0,0x7fffffff]-interval
            private int genrand_int31()
            {
                return (int)(genrand_int32() >> 1);
            }

            // generates a random number on [0,1]-real-interval
            double genrand_real1()
            {
                return genrand_int32() * (1.0 / 4294967295.0);
                // divided by 2^32-1
            }

            // generates a random number on [0,1)-real-interval
            double genrand_real2()
            {
                return genrand_int32() * (1.0 / 4294967296.0);
                // divided by 2^32
            }

            // generates a random number on (0,1)-real-interval
            double genrand_real3()
            {
                return (((double)genrand_int32()) + 0.5) * (1.0 / 4294967296.0);
                // divided by 2^32
            }

            // generates a random number on [0,1) with 53-bit resolution
            double genrand_res53()
            {
                uint a = genrand_int32() >> 5, b = genrand_int32() >> 6;
                return (a * 67108864.0 + b) * (1.0 / 9007199254740992.0);
            }
            // These real versions are due to Isaku Wada, 2002/01/09 added

            #endregion Methods ported from C

            #endregion Member Functions
        }
        #endregion
        

		#region Detect Thief
		public static Character.ClassType DetectThief(Character thief, Character detector)
		{
			if(detector.BaseProfession == Character.ClassType.Knight)
			{
				if(detector.Level > Skills.GetSkillLevel(thief.magic))
				{
					return Character.ClassType.Thief;
				}
			}
			else if(detector.BaseProfession == Character.ClassType.Thief)
			{
				if(Skills.GetSkillLevel(detector.magic) >= Skills.GetSkillLevel(thief.magic))
				{
					return Character.ClassType.Thief;
				}
			}
			return Character.ClassType.Fighter;
		}
		#endregion

		#region Detect Alignment
		public static bool DetectAlignment(Character target, Character detector)
		{
            /*
             * Since alignment is the primary determining factor if a target is an enemy,
             * it only makes sense to return true here if the target is indeed an enemy.
             */

            try
            {
                if (!detector.IsPC) // detector is not a player
                {
                    if (target.IsImmortal) { return false; }
                    if (target.trainerType != Merchant.TrainerType.None) { return false; }
                    if (target.merchantType != Merchant.MerchantType.None) { return false; }
                    if (target.interactiveType != Merchant.InteractiveType.None) { return false; }
                    if (!target.IsPC && target.questList.Count > 0) { return false; }
                    if (target.IsPC && detector.PlayersFlagged.Contains(target.PlayerID)) { return true; }
                    switch (detector.Alignment)
                    {
                        case Globals.eAlignment.Amoral: // amorals don't care about alignment
                            return false;
                        case Globals.eAlignment.Chaotic:
                            if (target.Alignment == Globals.eAlignment.Chaotic) { return false; } // chaotics do not attack chaotic alignment
                            else if (target.Alignment == Globals.eAlignment.Amoral) { return false; } // chaotics do not attack amoral alignment
                            return true;
                        case Globals.eAlignment.Evil: // evils attack everything except amoral
                            if (target.Alignment == Globals.eAlignment.Amoral) { return false; }
                            if (target.IsPC) { return true; } // evil npc attacks evil PC 
                            if (target.Alignment == Globals.eAlignment.Evil) { return false; }
                            return true;
                        case Globals.eAlignment.Lawful: // lawfuls will usually attack any non lawful and non amoral
                            if (target.Alignment != Globals.eAlignment.Lawful)
                            {
                                if (target.Alignment == Globals.eAlignment.Amoral) { return false; } // lawfuls will not attack amoral alignment
                                if (target.BaseProfession == Character.ClassType.Thief) // a thief can disguise their alignment
                                {
                                    if (Rules.DetectThief(target, detector) != target.BaseProfession) { return false; }
                                }
                                return true; // otherwise return true, lawfuls will attack any non lawful
                            }
                            return false;
                        case Globals.eAlignment.Neutral:
                            if (target.Map.Name != "Praetoseba")
                            {
                                if (target.Alignment == Globals.eAlignment.Chaotic || target.Alignment == Globals.eAlignment.Evil)
                                    return true;
                            }
                            return false;
                        default:
                            return false;
                    }
                }
                else
                {
                    if (target.BaseProfession == Character.ClassType.Thief)
                    {
                        if (target.Alignment == Globals.eAlignment.Neutral)
                        {
                            if (Rules.DetectThief(target, detector) != target.BaseProfession)
                            {
                                return false;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Utils.Log("Rules.DetectAlignment(" + target.Alignment + ", " + detector.Alignment + ") " + e.Message + "  Stack: " + e.StackTrace, Utils.LogType.Exception);
                return false;
            }
		}
		#endregion

		#region Detect Hidden

        public static bool DetectInvisible(Character invis, Character detector)
        {
            if (invis == detector)
            {
                return true;
            }

            if (!invis.IsInvisible)
            {
                return true;
            }

            if (detector.ImpLevel >= invis.ImpLevel && detector.IsImmortal)
            {
                return true;
            }

            return false;
        }

		public static bool DetectHidden(Character hider, Character detector) // return true if a hider is det
		{
            // The hider will always see itself.
            if (hider == detector)
            {
                return true;
            }

            // If the hider does not have a Hide_In_Shadows effect it is not hidden.
            if (!hider.IsHidden)
            {
                return true;
            }

            // If the immortal flag is set the hider (player only) is always hidden.
            if (detector.IsImmortal)
            {
                return true;
            }

            // AI Enforcer will see any hidden NPC, it's their job.
            if (!detector.IsPC && !hider.IsPC)
            {
                NPC npc = (NPC)detector;
                if (npc.aiType == NPC.AIType.Enforcer)
                {
                    return true;
                }
            }

            if (detector.seenList.Count > 0)
            {
                foreach (Character chr in new List<Character>(detector.seenList))
                {
                    if (chr == hider)
                        return true;
                }
            }

            int distance = Cell.GetCellDistance(detector.X, detector.Y, hider.X, hider.Y);

            // unless the NPC is a thief it has a natural ability to hide and is thus hidden at a distance greater than 1
            if (!hider.IsPC && distance > 1 && hider.BaseProfession != Character.ClassType.Thief)
            {
                return false;
            }
            

            int difference = Skills.GetSkillLevel(hider.magic) - detector.Level;

            // intelligence effects a hider and detector's ability
            difference += (int)((hider.Intelligence + hider.TempIntelligence) - (detector.Intelligence + detector.TempIntelligence) / 2);

            // small bonus for those with magic skill >= 10
            if (Skills.GetSkillLevel(hider.magic) >= 10)
            {
                difference += (int)(Skills.GetSkillLevel(hider.magic) - 10);
            }

            if (distance <= 0) // always see hidden characters when on the same cell
            {
                // give a thief with skill 10+ a chance to remain hidden when on the same cell
                if (hider.BaseProfession == Character.ClassType.Thief && Skills.GetSkillLevel(hider.magic) >= 10)
                {
                    if (Rules.RollD(1, 60) + (Skills.GetSkillLevel(hider.magic) - 10) >= 50)
                    {
                        return false;
                    }
                }
                return true;
            }

            switch (distance)
            {
                case 1:
                    if (65 + hider.Dexterity + hider.TempDexterity + difference > Rules.RollD(1, 100))
                    {
                        return false;
                    }
                    return true;
                case 2:
                    if (75 + hider.Dexterity + hider.TempDexterity + difference > Rules.RollD(1, 100))
                    {
                        return false;
                    }
                    return true;
                case 3:
                    if (85 + hider.Dexterity + hider.TempDexterity + difference > Rules.RollD(1, 100))
                    {
                        return false;
                    }
                    return true;
            }
            return false;
		}
		#endregion

		#region Aging Effects
        public static void DoAgingEffect(Character ch)
        {
            if (ch.Age == World.AgeCycles[0]) // young
            { ch.WriteToDisplay("You are now young."); }
            else if (ch.Age == World.AgeCycles[1]) // middle-aged
            {
                ch.WriteToDisplay("You are now middle-aged.");
                if (ch.Wisdom < 18) { ch.Wisdom++; }
            }
            else if (ch.Age == World.AgeCycles[2]) // old
            {
                ch.WriteToDisplay("You are now old.");
                if (ch.Constitution > 9) { ch.Constitution--; }
                if (ch.Strength > 9) { ch.Strength--; }
                if (ch.Dexterity > 9) { ch.Dexterity--; }
                if (ch.Wisdom < 18) { ch.Wisdom++; }
                if (ch.Charisma < 18) { ch.Charisma++; }
            }
            else if (ch.Age == World.AgeCycles[3]) // very old
            {
                ch.WriteToDisplay("You are now very old.");
                if (ch.Constitution > 9) { ch.Constitution--; }
                if (ch.Strength > 9) { ch.Strength--; }
                if (ch.Dexterity > 9) { ch.Dexterity--; }
                if (ch.Wisdom < 21) { ch.Wisdom++; }
                if (ch.Charisma < 21) { ch.Charisma++; }
            }
            else if (ch.Age == World.AgeCycles[4]) // ancient
            {
                ch.WriteToDisplay("You are now ancient.");
                if (ch.Constitution > 5) { ch.Constitution = 9; }
                if (ch.Strength > 5) { ch.Strength = 9; }
                if (ch.Dexterity > 5) { ch.Dexterity--; }
                if (ch.Wisdom < 22) { ch.Wisdom++; }
                if (ch.Charisma < 22) { ch.Charisma++; }
            }
            if (ch.Age >= World.AgeCycles[5] && ch.ImpLevel == Globals.eImpLevel.USER)
            {
                int grim;
                grim = Rules.RollD(1, 100);
                // if character is lucky add to grim roll...
                if (grim < 5)
                {
                    Rules.EnterUnderworld(ch);
                }
            }
        }
		#endregion

        public static bool BreakHideSpell(Character ch)
        {
            if (GetEncumbrance(ch) == "severely")
                return true;

            if (ch.RightHand != null)
            {
                if (ch.RightHand.size == Globals.eItemSize.Belt_Large_Slot_Only || ch.RightHand.size == Globals.eItemSize.No_Container)
                {
                    return true;
                }
            }

            if (ch.LeftHand != null)
            {
                if (ch.LeftHand.size == Globals.eItemSize.Belt_Large_Slot_Only || ch.LeftHand.size == Globals.eItemSize.No_Container)
                {
                    return true;
                }
            }

            return false;
        }

        public static int GetHitsGain(Character ch, int loopTimes)
        {
            int hitsGain = 0;
            int minimum = 1;
            if (ch.IsLucky) { minimum = (int)ch.Land.HitDice[(int)ch.BaseProfession] / 2; }
            int statScore = ch.Constitution + ch.TempConstitution;
            for (int a = 0; a < loopTimes; a++)
            {
                if (ch.BaseProfession == Character.ClassType.Fighter && ch.Level > 8) // level 9+ fighters gain 2 more hp max
                {
                    hitsGain += dice.Next(minimum, ch.Land.HitDice[(int)ch.BaseProfession] + 3);
                }
                else hitsGain += dice.Next(minimum, ch.Land.HitDice[(int)ch.BaseProfession] + 1);

                if (statScore == 16)
                    hitsGain = hitsGain + 1;
                else if (statScore == 17)
                    hitsGain = hitsGain + 2;
                else if (statScore >= 18)
                    hitsGain = hitsGain + 3;
                else if (statScore == 9)
                    hitsGain = hitsGain - 1;
                else if (statScore == 8)
                    hitsGain = hitsGain - 2;
                else if (statScore <= 7)
                    hitsGain = hitsGain - 3;
            }
            if (hitsGain <= 0) { hitsGain = 1; } // confirm at least 1 hit is gained
            return hitsGain;
        }

        public static int GetManaGain(Character ch, int loopTimes)
        {
            if (ch.Land.ManaDice[(int)ch.BaseProfession] == 0) { return 0; }
            int manaGain = 0;
            int minimum = 1;
            if (ch.IsLucky) { minimum = (int)ch.Land.ManaDice[(int)ch.BaseProfession] / 2; }
            int statScore = 0;
            if (ch.IsIntelligenceCaster)
                statScore = ch.Intelligence + ch.TempIntelligence;
            if (ch.IsWisdomCaster)
                statScore = ch.Wisdom + ch.TempWisdom;
            for (int a = 0; a < loopTimes; a++)
            {
                manaGain += dice.Next(minimum, ch.Land.ManaDice[(int)ch.BaseProfession] + 1);

                if (statScore == 16)
                    manaGain = manaGain + 1;
                else if (statScore == 17)
                    manaGain = manaGain + 2;
                else if (statScore >= 18)
                    manaGain = manaGain + 3;
                else if (statScore == 9)
                    manaGain = manaGain - 1;
                else if (statScore == 8)
                    manaGain = manaGain - 2;
                else if (statScore <= 7)
                    manaGain = manaGain - 3;
            }
            if (manaGain <= 0) { manaGain = 1; } // confirm at least 1 mana is gained
            return manaGain;
        }

        public static int GetStaminaGain(Character ch, int loopTimes)
        {
            int staminaGain = 0;
            int minimum = 1;
            if (ch.IsLucky) { minimum = (int)ch.Land.StaminaDice[(int)ch.BaseProfession] / 2; }
            int statScore = ch.Constitution + ch.TempConstitution;
            for (int a = 0; a < loopTimes; a++)
            {
                staminaGain += dice.Next(minimum, ch.Land.StaminaDice[(int)ch.BaseProfession] + 1);

                if (statScore == 16)
                    staminaGain = staminaGain + 1;
                else if (statScore == 17)
                    staminaGain = staminaGain + 2;
                else if (statScore >= 18)
                    staminaGain = staminaGain + 3;
                else if (statScore == 9)
                    staminaGain = staminaGain - 1;
                else if (statScore == 8)
                    staminaGain = staminaGain - 2;
                else if (statScore <= 7)
                    staminaGain = staminaGain - 3;
            }
            if (staminaGain <= 0) { staminaGain = 1; } // confirm at least 1 stamina is gained
            return staminaGain;
        }

        public static int GetMaximumHits(Character ch)
        {
            return ch.Land.HitDice[(int)ch.BaseProfession] * ch.Level + ch.Land.StatCapOperand;
        }

        public static int GetMaximumMana(Character ch)
        {
            return ch.Land.ManaDice[(int)ch.BaseProfession] * ch.Level;
        }

        public static int GetMaximumStamina(Character ch)
        {
            return ch.Land.StaminaDice[(int)ch.BaseProfession] * ch.Level;
        }

		public static string GetEncumbrance(Character ch)
		{
			int maxEncumb = Rules.Formula_MaxEncumbrance(ch);

            if (ch.encumbrance >= maxEncumb * 1.5)
                return "severely";
			else if(ch.encumbrance >= maxEncumb)
				return "heavily";
			else if(ch.encumbrance >= maxEncumb / 1.5)
				return "moderately";
			else
                return "lightly";
		}

        public static int Formula_MaxEncumbrance(Character ch)
        {
            return Convert.ToInt32(((ch.Strength + ch.TempStrength) * 9) + ((ch.Dexterity + ch.TempDexterity) * 2) +
                ((ch.Constitution + ch.TempConstitution) * 2));
        }

        public static long Formula_TrainingCostForLevel(int skillLevel)
        {
            return Convert.ToInt64((skillLevel + 1) * Math.Pow(2, skillLevel) * 25);
        } 

        public static long Formula_DoctoredHPCost(int hitPoint)
        {
            return Convert.ToInt64(Math.Pow(hitPoint, 2));
        }

        public static long Formula_SkillsLossAtDeath(int skillLevel)
        {
            return Convert.ToInt64(Skills.GetSkillToMax(skillLevel) / 4);
        }

		#region D&D Combat Code - 2nd Ed.

        public static double AC_GetDexterityArmorClassBonus(Character target)
        {
            double bonus = 0;

            // feared targets actually receive an AC bonus
            if (target.IsFeared)
                bonus -= target.effectList[Effect.EffectType.Fear].effectAmount / 2;

            // blind or stunned targets suffer a penalty
            if (target.IsBlind || target.Stunned > 0)
                bonus -= 2.3;

            if (Rules.GetEncumbrance(target) == "moderately")
                bonus -= .7;
            else if (Rules.GetEncumbrance(target) == "heavily")
                bonus -= 1.4;
            else if (Rules.GetEncumbrance(target) == "severely")
               bonus -= 2.8;

            if (!target.IsBlind && !target.IsFeared && target.Stunned <= 0 && Rules.GetEncumbrance(target) == "lightly")
                bonus += target.dexterityAdd / 2;

            return bonus;
        }

        public static double AC_GetShieldingArmorClass(int shielding, bool rangedAttack)
        {
            if (shielding <= 0) { return 0; }

            if (rangedAttack)
                return (double)shielding;
            else return shielding / 2;
        }

        public static double AC_GetArmorClassRating(Character target)
        {
            double rating = 0;

            foreach (Item armor in target.wearing)
                if (armor.itemType == Globals.eItemType.Wearable)
                    rating += armor.armorClass;

            return rating;
        }

        public static double AC_GetUnarmedArmorClassBonus(Character target)
        {
            if (!target.IsBlind && !target.IsFeared && target.Stunned <= 0)
            {
                // right hand is empty or right hand is weapon that uses unarmed skill
                if ((target.RightHand == null || target.RightHand != null && target.RightHand.skillType == Globals.eSkillType.Unarmed) &&
                            Skills.GetSkillLevel(target.unarmed) > 0 && // greater than Untrained in skill level
                            Rules.GetEncumbrance(target) == "lightly")
                {
                    if (target.BaseProfession == Character.ClassType.Martial_Artist)
                    {
                        if (target.LeftHand == null || (target.LeftHand != null && target.LeftHand.skillType == Globals.eSkillType.Unarmed))
                        {
                            if (target.GetEncumbrance() <= Character.MAX_UNARMED_WEIGHT)
                                return (Skills.GetSkillLevel(target.unarmed) * 1.3);
                            else
                            {
                                double overEncumb = target.GetEncumbrance() - Character.MAX_UNARMED_WEIGHT;

                                if (overEncumb <= Character.MAX_UNARMED_WEIGHT)
                                    return (Skills.GetSkillLevel(target.unarmed) / 1.3);
                                else if (overEncumb <= Character.MAX_UNARMED_WEIGHT * 1.5)
                                    return (Skills.GetSkillLevel(target.unarmed) / 2);
                                else return (Skills.GetSkillLevel(target.unarmed) / 4);
                            }
                        }
                        else return (Skills.GetSkillLevel(target.unarmed) / 1.5);
                    }
                    else
                    {
                        if (target.GetEncumbrance() <= Character.MAX_UNARMED_WEIGHT)
                            return (Skills.GetSkillLevel(target.unarmed) / 2);
                        else
                        {
                            double overEncumb = target.GetEncumbrance() - Character.MAX_UNARMED_WEIGHT;

                            if (overEncumb <= Character.MAX_UNARMED_WEIGHT)
                                return (Skills.GetSkillLevel(target.unarmed) / 4);
                            else if (overEncumb <= Character.MAX_UNARMED_WEIGHT * 1.5)
                                return (Skills.GetSkillLevel(target.unarmed) / 6);
                            else return (Skills.GetSkillLevel(target.unarmed) / 8);
                        }
                    }
                }
            }
            return 0;
        }

        public static double AC_GetRightHandArmorClass(Character target)
        {
            if (target.RightHand != null)
            {
                // weapon or miscellaneous items can lower AC
                if (target.RightHand.itemType == Globals.eItemType.Weapon || target.RightHand.itemType == Globals.eItemType.Miscellaneous)
                {
                    // if two handed weapon in right hand then left hand must be empty
                    if (target.RightHand.baseType == Globals.eItemBaseType.TwoHanded && target.LeftHand == null || target.RightHand.baseType != Globals.eItemBaseType.TwoHanded)
                    {
                        // no weapon AC bonus if heavily encumbered
                        if (Rules.GetEncumbrance(target) == "lightly")
                            return target.RightHand.armorClass;
                    }
                }

                // negative armor class
                if (target.RightHand.armorClass < 0)
                    return target.RightHand.armorClass;
            }
            return 0;
        }

        public static double AC_GetLeftHandArmorClass(Character target)
        {
            if (target.LeftHand != null)
            {
                // weapon or miscellaneous items can lower AC
                if (target.LeftHand.itemType == Globals.eItemType.Weapon || target.LeftHand.itemType == Globals.eItemType.Miscellaneous)
                {
                    // if two handed weapon in left hand then right hand must be empty
                    if (target.LeftHand.baseType == Globals.eItemBaseType.TwoHanded && target.RightHand == null || target.LeftHand.baseType != Globals.eItemBaseType.TwoHanded)
                    {
                        // no weapon AC bonus if heavily encumbered
                        if (Rules.GetEncumbrance(target) == "lightly")
                            return target.LeftHand.armorClass;
                    }
                }

                // negative armor class
                if (target.LeftHand.armorClass < 0)
                    return target.LeftHand.armorClass;
            }
            return 0;
        }

        public static double DND_GetHitLocation(Character attacker, Character target)
        {
            double multiplier = 0.0;

            int location = Rules.dice.Next(1, Enum.GetValues(typeof(Globals.eWearLocation)).Length);

            switch ((Globals.eWearLocation)location)
            {
                case Globals.eWearLocation.Head:
                    multiplier = 2.2;
                    break;
                case Globals.eWearLocation.Face:
                    multiplier = 2.0;
                    break;
                case Globals.eWearLocation.Ear:
                case Globals.eWearLocation.Nose:
                    multiplier = 1.8;
                    break;
                case Globals.eWearLocation.Neck:
                case Globals.eWearLocation.Back:
                case Globals.eWearLocation.Torso:
                    multiplier = 1.5;
                    break;
                default:
                    multiplier = 1.3;
                    break;
            }

            attacker.WriteToDisplay("You have scored a critical hit to your target's " + ((Globals.eWearLocation)location).ToString().ToLower() + "!");
            target.WriteToDisplay("You have suffered a critical hit to your " + ((Globals.eWearLocation)location).ToString().ToLower() + "!");
            
            return multiplier;
        }

        public static void DoFlag(Character attacker, Character target)
        {
            if (target != null && attacker != null && (attacker != target))
            {
                if (attacker.IsPC)
                {
                    if (!target.PlayersFlagged.Contains(attacker.PlayerID))
                        target.PlayersFlagged.Add(attacker.PlayerID);
                }
            }
        }       

		public static int DND_RollToHit(Character attacker, Character target, Item attackWeapon)
		{
            if (attacker.IsPC || target.IsPC)
                Rules.DoFlag(attacker, target);

			int roll = Rules.RollD(1, 20);

            #region Add 1 to a specialized fighter's roll
            if (attackWeapon != null && attacker.fighterSpecialization == attackWeapon.skillType)
                roll++;
            #endregion

            if (roll <= 1) { return -1; } // return critical miss

            if (roll >= 20) { return 2; } // return critical hit

            // record attacker's roll
            attacker.AttackRoll = roll;

            // base armor class
            double targetAC = target.baseArmorClass;

            // armor class rating
            targetAC -= Rules.AC_GetArmorClassRating(target);

            // unarmed armor class bonus
            targetAC -= Rules.AC_GetUnarmedArmorClassBonus(target);

            #region Shield spell armor class adjustments
            /*
             * If the attacker shot, threw or jumpkicked the target then we adjust the targetAC by the full shield value.
             * Otherwise we divide the shield value in half. Keep in mind a negative shield value will affect the targetAC...
             * (eg: future debuff spells)
             */
            if (target.Shielding > 0)
            {
                switch (attacker.CommandType)
                {
                    case Command.CommandType.Shoot:
                    case Command.CommandType.Throw:
                    case Command.CommandType.Jumpkick:
                        targetAC -= Rules.AC_GetShieldingArmorClass(target.Shielding, true);
                        break;
                    default:
                        targetAC -= Rules.AC_GetShieldingArmorClass(target.Shielding, false);
                        break;
                }
            }
            #endregion

            if (target.RightHand != null && !target.RightHand.IsAttunedToOther(target) &&
                target.RightHand.AlignmentCheck(target))
            {
                // right hand AC adjustment
                targetAC -= Rules.AC_GetRightHandArmorClass(target);
            }

            if (target.LeftHand != null && !target.LeftHand.IsAttunedToOther(target) &&
                target.LeftHand.AlignmentCheck(target))
            {
                // left hand AC adjustment
                targetAC -= Rules.AC_GetLeftHandArmorClass(target);
            }

            // skill AC adjustment
            if (target.GetWeaponSkillLevel(target.RightHand) > attacker.GetWeaponSkillLevel(attacker.RightHand))
                targetAC -= target.GetWeaponSkillLevel(target.RightHand) - attacker.GetWeaponSkillLevel(attacker.RightHand);

            targetAC -= Rules.AC_GetDexterityArmorClassBonus(target);

            int rollNeeded = Rules.DND_GetModifiedTHAC0(attacker, attackWeapon) + Rules.CalculateArmorClass(targetAC);

			if(roll >= rollNeeded)
				return 1; // hit

			return 0; // miss
		}

		public static void DND_Attack(Character ch, Character target, Item attackWeapon, bool critical)
		{
			Globals.eSkillType skillType = Globals.eSkillType.Unarmed;
			int damage = 0;
			int totalDamage = 0;
            int damageBonus = 0;
            string dmgReduction = "DISABLED";
            int criticalDamage = 0;
            int hardHitterDamage = 0;
			string dmgAdjective = "fatal";
            string dmgDisplay = ""; // display combat damage to player

            // attacker is wielding a weapon, or using a worn weapon (ie: gauntlets, boots)
            if (attackWeapon != null)
            {
                skillType = attackWeapon.skillType;
                damageBonus += ch.GetWeaponSkillLevel(attackWeapon);
                damage = CalculateWeaponDamage(ch, attackWeapon);

                // "unarmed" attack but gauntlets or boots were sent here as attackWeapon
                if (attackWeapon == ch.GetInventoryItem(Globals.eWearLocation.Hands) || attackWeapon == ch.GetInventoryItem(Globals.eWearLocation.Feet))
                    damage += Rules.CalculateUnarmedDamage(ch);
            }
            else // using pure unarmed combat
            {
                damageBonus += ch.GetWeaponSkillLevel(null);
                damage += Rules.CalculateUnarmedDamage(ch);             
            }

            // critical damage
			if(critical)
			{
                double multiplier = Rules.DND_GetHitLocation(ch, target);

                criticalDamage = (int)(damage * multiplier) - damage;

                // TODO increase stun chance for various reasons...

                // below 1.8 will not cause a stun
                if (multiplier < 1.8) critical = false;
			}

            // tally damage
			totalDamage += damage; // add damage to total damage
            totalDamage += damageBonus; // add damage bonus to total damage
            totalDamage += ch.TempStrength; // add temporary strength amount to total damage
            totalDamage += ch.strengthAdd; // add strength add to total damage
            totalDamage += criticalDamage; // add critical damage
            if (ch.IsHardHitter) //if hard hitter then multipy the damage by 2 to 6 times
            {
                int hardHitterMultiplier = Rules.RollD(2, 3);
                hardHitterDamage = (int)(totalDamage * hardHitterMultiplier);
                totalDamage += hardHitterDamage;
            }

            // confirm total damage is atleast 1
            if (totalDamage <= 0) totalDamage = Rules.RollD(1, 4);

            // determine damage adjective
            int percentage = (int)(((float)totalDamage / (float)target.Hits) * 100); // get the damage adjective

			if (percentage >= 0 && percentage < 15) dmgAdjective = "light"; 
			else if (percentage >=15 && percentage < 31) dmgAdjective = "moderate"; 
			else if (percentage >=31 && percentage < 71) dmgAdjective = "heavy"; 
			else if (percentage >=71 && percentage < 100) dmgAdjective = "severe";
			else if (percentage >= 100) dmgAdjective = "fatal";            

            // display combat damage if applicable
            if (ch.displayCombatDamage && (System.Configuration.ConfigurationManager.AppSettings["DisplayCombatDamage"].ToLower() == "true" || ch.ImpLevel >= Globals.eImpLevel.DEV))
               dmgDisplay = " (" + totalDamage + ")";

            // emit sound
            target.EmitSound(Sound.GetSoundForDamage(dmgAdjective));

			if(ch.IsPC)
			{
                if (!target.IsPC) // player vs. environment
                {
                    Skills.GiveSkillExp(ch, target, skillType); // give skill experience for player vs. environment
                }
                else // pvp and sparring
                {
                    Skills.GiveSkillExp(ch, skillType, ch.Level + target.Level); // give skill experience for player vs. player
                }

                #region PC Return messages
                if (attackWeapon == null || (attackWeapon != null && (attackWeapon.wearLocation == Globals.eWearLocation.Hands || attackWeapon.wearLocation == Globals.eWearLocation.Feet))) // handle unarmed attack messages
                {
                    if (ch.CommandType == Command.CommandType.Kick)
                    {
                        ch.WriteToDisplay("Kick hits for " + dmgAdjective + " damage!" + dmgDisplay);
                        if (ch.race != "")
                            target.WriteToDisplay(ch.Name + " kicks you!");
                        else
                            target.WriteToDisplay("The " + ch.Name + " kicks you!");
                    }
                    else if (ch.CommandType == Command.CommandType.Jumpkick)
                    {
                        ch.WriteToDisplay("Jumpkick hits for " + dmgAdjective + " damage!" + dmgDisplay);
                        if (ch.race != "")
                        {
                            target.WriteToDisplay(ch.Name + " jumpkicks you!");
                        }
                        else
                        {
                            target.WriteToDisplay("The " + ch.Name + " jumpkicks you!");
                        }
                    }
                    else
                    {
                        ch.WriteToDisplay("Swing hits with " + dmgAdjective + " damage!" + dmgDisplay);
                        if (ch.race != "")
                        {
                            target.WriteToDisplay(ch.Name + " punches you!");
                        }
                        else
                        {
                            target.WriteToDisplay("The " + ch.Name + " punches you!");
                        }
                    }
                }
                else if (ch.CommandType == Command.CommandType.Shoot || ch.CommandType == Command.CommandType.Throw)
                {
                    ch.WriteToDisplay("Shot hits with " + dmgAdjective + " damage!" + dmgDisplay);
                    if (ch.race != "")
                    {
                        target.WriteToDisplay(ch.Name + " hits with " + attackWeapon.shortDesc + "!");
                    }
                    else
                    {
                        target.WriteToDisplay("The " + ch.Name + " hits with " + attackWeapon.shortDesc + "!");
                    }
                }
                else if (ch.CommandType == Command.CommandType.Bash)
                {
                    ch.WriteToDisplay("Bash hits for " + dmgAdjective + " damage!" + dmgDisplay);
                    if (ch.race != "")
                    {
                        target.WriteToDisplay(ch.Name + " bashes you with " + attackWeapon.shortDesc + "!");
                    }
                    else
                    {
                        target.WriteToDisplay("The " + ch.Name + " bashes you with " + attackWeapon.shortDesc + "!");
                    }
                }
                else if (ch.CommandType == Command.CommandType.Poke)
                {
                    ch.WriteToDisplay("Poke hits with " + dmgAdjective + " damage!" + dmgDisplay);
                    if (ch.race != "")
                    {
                        target.WriteToDisplay(ch.Name + " pokes you with " + attackWeapon.shortDesc + "!");
                    }
                    else
                    {
                        target.WriteToDisplay("The " + ch.Name + " pokes you with " + attackWeapon.shortDesc + "!");
                    }
                }
                else
                {
                    ch.WriteToDisplay("Swing hits with " + dmgAdjective + " damage!" + dmgDisplay);

                    string weaponHitDesc = " hits with " + attackWeapon.shortDesc + "!";

                    if (attackWeapon == ch.GetInventoryItem(Globals.eWearLocation.Hands))
                        weaponHitDesc = " punches you!";
                    else if (attackWeapon == ch.GetInventoryItem(Globals.eWearLocation.Feet))
                    {
                        if (ch.CommandType == Command.CommandType.Jumpkick)
                            weaponHitDesc = " jumpkicks you!";
                        if (ch.CommandType == Command.CommandType.Kick)
                            weaponHitDesc = "kicks you!";
                    }

                    if (ch.race != "")
                        target.WriteToDisplay(ch.Name + weaponHitDesc);
                    else
                        target.WriteToDisplay("The " + ch.Name + weaponHitDesc);
                }
                #endregion

			}
			else NPC.CombatAI((NPC)ch, target);

            #region Combat Logging
            if (target.IsPC)
            {
                if (critical)
                {
                    if (attackWeapon == null)
                    {
                        Utils.Log(target.GetLogString() + " took (Damage: " + damage + " + Damage Bonus: " + damageBonus + " + Temp Strength: " + ch.TempStrength + " + Strength Add: " + ch.strengthAdd + " + Critical Damage: " + criticalDamage + " - Armor Damage Reduction: " + dmgReduction + " + Hard Hitter Damage: " + hardHitterDamage + ") = " + totalDamage + " from " + ch.GetLogString() + " Unarmed (" + ch.LastCommand + ") Skill: " +
                            Skills.GetSkillTitle(Globals.eSkillType.Unarmed, ch.BaseProfession, ch.GetSkillExperience(Globals.eSkillType.Unarmed), ch.gender), Utils.LogType.CriticalCombatDamageToPlayer);
                    }
                    else
                    {
                        Utils.Log(target.GetLogString() + " took (Damage: " + damage + " + Damage Bonus: " + damageBonus + " + Temp Strength: " + ch.TempStrength + " + Strength Add: " + ch.strengthAdd + " + Critical Damage: " + criticalDamage + " - Armor Damage Reduction: " + dmgReduction + " + Hard Hitter Damage: " + hardHitterDamage + ") = " + totalDamage + " from " + ch.GetLogString() + " " + attackWeapon.GetLogString() + " (" + ch.LastCommand + ") Skill: " +
                            Skills.GetSkillTitle(attackWeapon.skillType, ch.BaseProfession, ch.GetSkillExperience(attackWeapon.skillType), ch.gender), Utils.LogType.CriticalCombatDamageToPlayer);
                    }
                }
                else
                {
                    if (attackWeapon == null)
                    {
                        Utils.Log(target.GetLogString() + " took (Damage: " + damage + " + Damage Bonus: " + damageBonus + " + Temp Strength: " + ch.TempStrength + " + Strength Add: " + ch.strengthAdd + " - Armor Damage Reduction: " + dmgReduction + " + Hard Hitter Damage: " + hardHitterDamage + ") = " + totalDamage + " from " + ch.GetLogString() + " Unarmed (" + ch.LastCommand + ") Skill: " +
                            Skills.GetSkillTitle(Globals.eSkillType.Unarmed, ch.BaseProfession, ch.GetSkillExperience(Globals.eSkillType.Unarmed), ch.gender), Utils.LogType.CombatDamageToPlayer);
                    }
                    else
                    {
                        Utils.Log(target.GetLogString() + " took (Damage: " + damage + " + Damage Bonus: " + damageBonus + " + Temp Strength: " + ch.TempStrength + " + Strength Add: " + ch.strengthAdd + " - Armor Damage Reduction: " + dmgReduction + " + Hard Hitter Damage: " + hardHitterDamage + ") = " + totalDamage + " from " + ch.GetLogString() + " " + attackWeapon.GetLogString() + " (" + ch.LastCommand + ") Skill: " +
                            Skills.GetSkillTitle(attackWeapon.skillType, ch.BaseProfession, ch.GetSkillExperience(attackWeapon.skillType), ch.gender), Utils.LogType.CombatDamageToPlayer);
                    }
                }
            }
            else
            {
                if (critical)
                {
                    if (attackWeapon == null)
                    {
                        Utils.Log(target.GetLogString() + " took (Damage: " + damage + " + Damage Bonus: " + damageBonus + " + Temp Strength: " + ch.TempStrength + " + Strength Add: " + ch.strengthAdd + " + Critical Damage: " + criticalDamage + " - Armor Damage Reduction: " + dmgReduction + " + Hard Hitter Damage: " + hardHitterDamage + ") = " + totalDamage + " from " + ch.GetLogString() + " Unarmed (" + ch.LastCommand + ") Skill: " +
                            Skills.GetSkillTitle(Globals.eSkillType.Unarmed, ch.BaseProfession, ch.GetSkillExperience(Globals.eSkillType.Unarmed), ch.gender), Utils.LogType.CriticalCombatDamageToCreature);
                    }
                    else
                    {
                        Utils.Log(target.GetLogString() + " took (Damage: " + damage + " + Damage Bonus: " + damageBonus + " + Temp Strength: " + ch.TempStrength + " + Strength Add: " + ch.strengthAdd + " + Critical Damage: " + criticalDamage + " - Armor Damage Reduction: " + dmgReduction + " + Hard Hitter Damage: " + hardHitterDamage + ") = " + totalDamage + " from " + ch.GetLogString() + " " + attackWeapon.GetLogString() + " (" + ch.LastCommand + ") Skill: " +
                            Skills.GetSkillTitle(attackWeapon.skillType, ch.BaseProfession, ch.GetSkillExperience(attackWeapon.skillType), ch.gender), Utils.LogType.CriticalCombatDamageToCreature);
                    }
                }
                else
                {
                    if (attackWeapon == null)
                    {
                        Utils.Log(target.GetLogString() + " took (Damage: " + damage + " + Damage Bonus: " + damageBonus + " + Temp Strength: " + ch.TempStrength + " + Strength Add: " + ch.strengthAdd + " - Armor Damage Reduction: " + dmgReduction + " + Hard Hitter Damage: " + hardHitterDamage + ") = " + totalDamage + " from " + ch.GetLogString() + " Unarmed (" + ch.LastCommand + ") Skill: " +
                            Skills.GetSkillTitle(Globals.eSkillType.Unarmed, ch.BaseProfession, ch.GetSkillExperience(Globals.eSkillType.Unarmed), ch.gender), Utils.LogType.CombatDamageToCreature);
                    }
                    else
                    {
                        Utils.Log(target.GetLogString() + " took (Damage: " + damage + " + Damage Bonus: " + damageBonus + " + Temp Strength: " + ch.TempStrength + " + Strength Add: " + ch.strengthAdd + " - Armor Damage Reduction: " + dmgReduction + " + Hard Hitter Damage: " + hardHitterDamage + ") = " + totalDamage + " from " + ch.GetLogString() + " " + attackWeapon.GetLogString() + " (" + ch.LastCommand + ") Skill: " +
                            Skills.GetSkillTitle(attackWeapon.skillType, ch.BaseProfession, ch.GetSkillExperience(attackWeapon.skillType), ch.gender), Utils.LogType.CombatDamageToCreature);
                    }
                }
            } 
            #endregion

            if (ch.CommandType == Command.CommandType.Bash)
                critical = true;

			Rules.DoDamage(target, ch, totalDamage, critical); // deal the damage
		}

		public static int DND_GetModifiedTHAC0(Character ch, Item attackWeapon)
		{
			int modifier = 0;

			if(attackWeapon == null) // unarmed
			{
                #region Pure Unarmed Combat
                // bonus after red belt
                if (Skills.GetSkillLevel(ch.GetSkillExperience(Globals.eSkillType.Unarmed)) > 5)
                    modifier += Skills.GetSkillLevel(ch.GetSkillExperience(Globals.eSkillType.Unarmed)) - 5;

                if (ch.CommandType == Command.CommandType.Kick)
                    modifier -= 3;
                else if (ch.CommandType == Command.CommandType.Jumpkick)
                    modifier -= 5;
                #endregion
			}
			else
			{
                // using gauntlets or boots and character is a pure martial artist, give bonus after red belt
                if (attackWeapon.skillType == Globals.eSkillType.Unarmed && ch.BaseProfession == Character.ClassType.Martial_Artist)
                {
                    if (Skills.GetSkillLevel(ch.GetSkillExperience(Globals.eSkillType.Unarmed)) > 5)
                        modifier += Skills.GetSkillLevel(ch.GetSkillExperience(Globals.eSkillType.Unarmed)) - 5;
                }

                if (ch.CommandType == Command.CommandType.Kick)
                    modifier -= 3;
                else if (ch.CommandType == Command.CommandType.Jumpkick)
                    modifier -= 5;

                // weapon bonus base is attack weapon combat adds
				modifier = attackWeapon.combatAdds;
                    
                // bonus for fighter specialization
                if (ch.fighterSpecialization == attackWeapon.skillType)
                    modifier += ch.Level - 8;

                // bonus for missile weapons when hidden
                if ((ch.CommandType == Command.CommandType.Shoot || ch.CommandType == Command.CommandType.Throw) && ch.IsHidden)
                    modifier += (int)(Skills.GetSkillLevel(ch.GetSkillExperience(attackWeapon.skillType)) / 3);
			}

            // CHEAT
            if (ch.IsPC) modifier += 2;

            return DND_GetBaseTHAC0(ch.BaseProfession, ch.GetWeaponSkillLevel(attackWeapon)) - modifier + ch.THAC0Adjustment;
		}

        public static int DND_GetBaseTHAC0(Character.ClassType classType, int skillLevel) // returns THAC0 based on class and skill level
        {
            int thac0 = 20;
            int[] Fighter = new int[] { 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12, -13, -14, -15, -16 };
            int[] Thaumaturge = new int[] { 20, 19, 18, 18, 17, 16, 16, 15, 14, 14, 13, 12, 12, 11, 10, 10, 9, 8, 8, 7, 6, 6, 5, 4, 4, 3, 2, 2, 1, 0, 0, -1, -2, -2, -3, -4 };
            int[] MartialArtist = new int[] { 19, 19, 18, 17, 16, 15, 14, 13, 12, 11, 11, 10, 10, 9, 9, 8, 8, 7, 6, 5, 5, 4, 3, 2, 2 };
            int[] Wizard = new int[] { 20, 20, 19, 19, 19, 18, 18, 18, 17, 17, 17, 16, 16, 16, 15, 15, 15, 14, 14, 14, 13, 13, 13, 12, 12, 12, 11, 11, 11, 10, 10, 10, 9, 9, 9, 8 };
            int[] Thief = new int[] { 20, 19, 19, 18, 18, 17, 17, 16, 16, 15, 15, 14, 14, 13, 13, 12, 12, 11, 11, 10, 10, 9, 9, 8, 8, 7, 7, 6, 6, 5, 5, 4, 4, 3, 3, 2 };
            int[] Knight = new int[] { 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12, -13, -14, -15 };
            switch (classType)
            {
                case Character.ClassType.Fighter:
                    thac0 = Fighter[skillLevel];
                    break;
                case Character.ClassType.Knight:
                    thac0 = Knight[skillLevel];
                    break;
                case Character.ClassType.Martial_Artist:
                    thac0 = MartialArtist[skillLevel];
                    break;
                case Character.ClassType.Thaumaturge:
                    thac0 = Thaumaturge[skillLevel];
                    break;
                case Character.ClassType.Thief:
                    thac0 = Thief[skillLevel];
                    break;
                case Character.ClassType.Wizard:
                    thac0 = Wizard[skillLevel];
                    break;
                default:
                    thac0 = Fighter[skillLevel];
                    break;
            }
            
            return thac0;
        }

        public enum SavingThrow { ParalyzationPoisonDeath, PetrificationPolymorph, RodStaffWand, BreathWeapon, Spell }

		public static bool DND_GetSavingThrow(Character ch, SavingThrow savingThrow, int modifier)
		{
			// Types:
			// PPD (Poison, Death Magic)
			// PP (Paralyzation, Petrification, or Polymorph)
			// RSW (Rod, Staff, Wand)
			// S (Spells) (Concussion)
			// BW (Breath Weapon)

            if (ch == null) // TODO: item saving throws
            {
                switch (savingThrow)
                {
                    case SavingThrow.BreathWeapon:
                        modifier -= 2;
                        break;
                    case SavingThrow.ParalyzationPoisonDeath:
                        break;
                    case SavingThrow.PetrificationPolymorph:
                        modifier += 2;
                        break;
                    case SavingThrow.RodStaffWand:
                        modifier += 1;
                        break;
                    case SavingThrow.Spell:
                        modifier -= 1;
                        break;
                }
                if (Rules.RollD(1, 20) + modifier >= 10)
                {
                    return true;
                }
                return false;
            }

            int PPD = modifier;
            int PP = modifier;
            int RSW = modifier;
            int S = modifier;
            int BW = modifier;
			int rollNeeded = 20;

			#region Class Saving Throw Mods
			switch(ch.BaseProfession)
			{
				case Character.ClassType.Fighter:
					PPD += -2;
					PP += -2;
					RSW += -1;
					S += 0;
					BW += 0;
					break;
				case Character.ClassType.Knight:
					PPD += -4;
					PP += -4;
					RSW += -3;
					S += -2;
					BW += -2;
					break;
				case Character.ClassType.Thaumaturge:
					PPD += -2;
					PP += -2;
					RSW += 0;
					S += -1;
					BW += 0;
					break;
				case Character.ClassType.Thief:
                    PPD += -1;
                    PP += -2;
                    RSW += -1;
                    S += -1;
                    BW += 0;
					break;
				case Character.ClassType.Martial_Artist:
                    PPD += -2;
                    PP += -2;
                    RSW += -1;
                    S += 0;
                    BW += 0;
					break;
				case Character.ClassType.Wizard:
                    PPD += 0;
                    PP += 0;
                    RSW += -2;
                    S += -2;
                    BW += -1;
					break;
				default:
					break;
			}
			#endregion

			#region Race Saving Throw Mods
			switch(ch.race)
			{
				case "Illyria":
                    PPD += -2;
                    PP += -2;
                    RSW += -1;
                    S += 0;
                    BW += 0;
					break;
				case "Mu":
                    PPD += 0;
                    PP += 0;
                    RSW += -1;
                    S += -1;
                    BW += -2;
					break;
				case "Lemuria":
                    PPD += 0;
                    PP += 0;
                    RSW += -2;
                    S += -2;
                    BW += 0;
					break;
				case "Leng":
                    PPD += 0;
                    PP += -1;
                    RSW += -1;
                    S += -1;
                    BW += -1;
					break;
				case "Draznia":
                    PPD += -2;
                    PP += -2;
                    RSW += -1;
                    S += 0;
                    BW += 0;
					break;
				case "Hovath":
                    PPD += -1;
                    PP += 0;
                    RSW += -1;
                    S += -2;
                    BW += 0;
					break;
				case "Mnar":
                    PPD += -1;
                    PP += -1;
                    RSW += 0;
                    S += 0;
                    BW += -2;
					break;
				case "the plains":
                    PPD += -1;
                    PP += -1;
                    RSW += -2;
                    S += 0;
                    BW += 0;
					break;
				default:
                    //PPD += 0;
                    //PP += -1;
                    //RSW += -1;
                    //S += 0;
                    //BW += -2;
					break;
			}
			#endregion

			#region PPD Chart by Level

			if(savingThrow == SavingThrow.ParalyzationPoisonDeath)
			{
				switch(ch.Level)
				{
					case 0:
						rollNeeded = 20 + PPD;
						break;
					case 1:
						rollNeeded = 19 + PPD;
						break;
					case 2:
						rollNeeded = 18 + PPD;
						break;
					case 3:
						rollNeeded = 18 + PPD;
						break;
					case 4:
						rollNeeded = 17 + PPD;
						break;
					case 5:
						rollNeeded = 16 + PPD;
						break;
					case 6:
						rollNeeded = 16 + PPD;
						break;
					case 7:
						rollNeeded = 15 + PPD;
						break;
					case 8:
						rollNeeded = 14 + PPD;
						break;
					case 9:
						rollNeeded = 14 + PPD;
						break;
					case 10:
						rollNeeded = 13 + PPD;
						break;
					case 11:
						rollNeeded = 12 + PPD;
						break;
					case 12:
						rollNeeded = 12 + PPD;
						break;
					case 13:
						rollNeeded = 12 + PPD;
						break;
					case 14:
						rollNeeded = 12 + PPD;
						break;
					case 15:
						rollNeeded = 12 + PPD;
						break;
					case 16:
						rollNeeded = 11 + PPD;
						break;
					case 17:
						rollNeeded = 11 + PPD;
						break;
					case 18:
						rollNeeded = 10 + PPD;
						break;
					case 19:
						rollNeeded = 10 + PPD;
						break;
					case 20:
						rollNeeded = 10 + PPD;
						break;
					case 21:
						rollNeeded = 10 + PPD;
						break;
					case 22:
						rollNeeded = 9 + PPD;
						break;
					case 23:
						rollNeeded = 9 + PPD;
						break;
					case 24:
						rollNeeded = 8 + PPD;
						break;
					case 25:
						rollNeeded = 8 + PPD;
						break;
					default:
						rollNeeded = 7 + PPD;
						break;
				}
			}
			#endregion

			#region PP Chart by Level
			else if(savingThrow == SavingThrow.PetrificationPolymorph)
			{
				switch(ch.Level)
				{
					case 0:
						rollNeeded = 20 + PP;
						break;
					case 1:
						rollNeeded = 19 + PP;
						break;
					case 2:
						rollNeeded = 19 + PP;
						break;
					case 3:
						rollNeeded = 18 + PP;
						break;
					case 4:
						rollNeeded = 17 + PP;
						break;
					case 5:
						rollNeeded = 17 + PP;
						break;
					case 6:
						rollNeeded = 16 + PP;
						break;
					case 7:
						rollNeeded = 16 + PP;
						break;
					case 8:
						rollNeeded = 15 + PP;
						break;
					case 9:
						rollNeeded = 14 + PP;
						break;
					case 10:
						rollNeeded = 14 + PP;
						break;
					case 11:
						rollNeeded = 14 + PP;
						break;
					case 12:
						rollNeeded = 13 + PP;
						break;
					case 13:
						rollNeeded = 13 + PP;
						break;
					case 14:
						rollNeeded = 13 + PP;
						break;
					case 15:
						rollNeeded = 13 + PP;
						break;
					case 16:
						rollNeeded = 12 + PP;
						break;
					case 17:
						rollNeeded = 12 + PP;
						break;
					case 18:
						rollNeeded = 11 + PP;
						break;
					case 19:
						rollNeeded = 11 + PP;
						break;
					case 20:
						rollNeeded = 11 + PP;
						break;
					case 21:
						rollNeeded = 11 + PP;
						break;
					case 22:
						rollNeeded = 10 + PP;
						break;
					case 23:
						rollNeeded = 10 + PP;
						break;
					case 24:
						rollNeeded = 10 + PP;
						break;
					case 25:
						rollNeeded = 10 + PP;
						break;
					default:
						rollNeeded = 9 + PP;
						break;
				}
			}
			#endregion

			#region RSW Chart by Level
			else if(savingThrow == SavingThrow.RodStaffWand)
			{
				switch(ch.Level)
				{
					case 0:
						rollNeeded = 20 + RSW;
						break;
					case 1:
						rollNeeded = 19 + RSW;
						break;
					case 2:
						rollNeeded = 19 + RSW;
						break;
					case 3:
						rollNeeded = 18 + RSW;
						break;
					case 4:
						rollNeeded = 18 + RSW;
						break;
					case 5:
						rollNeeded = 17 + RSW;
						break;
					case 6:
						rollNeeded = 16 + RSW;
						break;
					case 7:
						rollNeeded = 16 + RSW;
						break;
					case 8:
						rollNeeded = 15 + RSW;
						break;
					case 9:
						rollNeeded = 15 + RSW;
						break;
					case 10:
						rollNeeded = 14 + RSW;
						break;
					case 11:
						rollNeeded = 14 + RSW;
						break;
					case 12:
						rollNeeded = 14 + RSW;
						break;
					case 13:
						rollNeeded = 14 + RSW;
						break;
					case 14:
						rollNeeded = 13 + RSW;
						break;
					case 15:
						rollNeeded = 13 + RSW;
						break;
					case 16:
						rollNeeded = 12 + RSW;
						break;
					case 17:
						rollNeeded = 12 + RSW;
						break;
					case 18:
						rollNeeded = 12 + RSW;
						break;
					case 19:
						rollNeeded = 12 + RSW;
						break;
					case 20:
						rollNeeded = 11 + RSW;
						break;
					case 21:
						rollNeeded = 11 + RSW;
						break;
					case 22:
						rollNeeded = 11 + RSW;
						break;
					case 23:
						rollNeeded = 11 + RSW;
						break;
					case 24:
						rollNeeded = 10 + RSW;
						break;
					case 25:
						rollNeeded = 10 + RSW;
						break;
					default:
						rollNeeded = 10 + RSW;
						break;
				}
			}
			#endregion

			#region S Chart by Level
			else if(savingThrow == SavingThrow.Spell)
			{
				switch(ch.Level)
				{
					case 0:
						rollNeeded = 20 + S;
						break;
					case 1:
					case 2:
						rollNeeded = 19 + S;
						break;
					case 3:
					case 4:
						rollNeeded = 18 + S;
						break;
					case 5:
					case 6:
						rollNeeded = 17 + S;
						break;
					case 7:
						rollNeeded = 16 + S;
						break;
					case 8:
					case 9:
						rollNeeded = 15 + S;
						break;
					case 10:
					case 11:
					case 12:
					case 13:
						rollNeeded = 14 + S;
						break;
					case 14:
					case 15:
					case 16:
					case 17:
						rollNeeded = 13 + S;
						break;
					case 18:
					case 19:
					case 20:
					case 21:
						rollNeeded = 12 + S;
						break;
					case 22:
					case 23:
						rollNeeded = 11 + S;
						break;
					case 24:
					case 25:
						rollNeeded = 10 + S;
						break;
					default:
						rollNeeded = 10 + S;
						break;
				}
			}
			#endregion

			#region BW Chart by Level
			else if(savingThrow == SavingThrow.BreathWeapon)
			{
				switch(ch.Level)
				{
					case 0:
						rollNeeded = 20 + BW;
						break;
					case 1:
					case 2:
						rollNeeded = 19 + BW;
						break;
					case 3:
					case 4:
						rollNeeded = 18 + BW;
						break;
					case 5:
					case 6:
						rollNeeded = 17 + BW;
						break;
					case 7:
					case 8:
						rollNeeded = 16 + BW;
						break;
					case 9:
					case 10:
					case 11:
						rollNeeded = 15 + BW;
						break;
					case 12:
					case 13:
					case 14:
					case 15:
						rollNeeded = 14 + BW;
						break;
					case 16:
					case 17:
					case 18:
					case 19:
						rollNeeded = 13 + BW;
						break;
					case 20:
					case 21:
					case 22:
					case 23:
						rollNeeded = 12 + BW;
						break;
					case 24:
					case 25:
						rollNeeded = 11 + BW;
						break;
					default:
						rollNeeded = 11 + BW;
						break;
				}
			}
			#endregion

			int roll = Rules.RollD(1, 20);
			if(roll >= rollNeeded)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		#endregion

        /// <summary>
        /// Roll and check vs. full stat.
        /// </summary>
        /// <param name="ch">The character whos stat will be checked.</param>
        /// <param name="stat">The string representation of the stat.</param>
        /// <returns>True if pass, false otherwise.</returns>
        public static bool FullStatCheck(Character ch, string stat)
        {
            int statScore = 0;

            switch (stat.ToLower())
            {
                case "strength":
                    statScore = ch.Strength + ch.TempStrength;
                    break;
                case "dexterity":
                    statScore = ch.Dexterity + ch.TempDexterity;
                    break;
                case "intelligence":
                    statScore = ch.Intelligence + ch.TempIntelligence;
                    break;
                case "wisdom":
                    statScore = ch.Wisdom + ch.TempWisdom;
                    break;
                case "constitution":
                    statScore = ch.Constitution + ch.TempConstitution;
                    break;
                case "charisma":
                    statScore = ch.Charisma + ch.TempCharisma;
                    break;
            }

            if (RollD(1, World.GetFacetByIndex(0).GetLandByID(ch.LandID).MaxAbilityScore + (int)(World.GetFacetByIndex(0).GetLandByID(ch.LandID).MaxTempAbilityScore / 4)) < statScore)
            {
                return true;
            }
            return false;
        }

        public static int RollD(int number, int sides)
        {
            int roll = 0;

            for (int a = 0; a < number; a++)
            {
                roll += Rules.dice.Next(1, sides);
            }
            return roll;
        }

		public static int GetExpLevel(long exp)
		{
            long low = 1600;

            long high = 3200;

            for (int a = 3; a <= Globals.MAX_EXP_LEVEL; a++)
            {
                if (exp >= low && exp < high)
                    return a;

                low = high;

                high = high * 2;
            }
            return 3;
		}

		public static void DoJumpKick(Character ch, Character target) 
		{
			string EncumbDesc = GetEncumbrance(ch);
			int fallChance = 0;
			int fallDmg = 1;
			int roll = 0;

            if (ch.GetWeaponSkillLevel(null) < 6) // if character is below black belt there is a chance to fall when jumpkicking
			{
                fallChance += 20 - ch.GetWeaponSkillLevel(null); // add more of a chance to fall, and damage according to encumbrance
				fallDmg += Rules.RollD(1, 2);

				if(EncumbDesc == "moderately")
                {
                    fallChance += 5;
                    fallDmg += Rules.RollD(1, 2);
                }
				else if(EncumbDesc == "heavily")
                {
                    fallChance += 10;
                    fallDmg += Rules.RollD(1, 4);
                }
                else if (EncumbDesc == "severely")
                {
                    fallChance += 15;
                    fallDmg += Rules.RollD(1, 6);
                }

				roll = Rules.RollD(1, 100);

				if(roll < (60 + fallChance) - ch.GetWeaponSkillLevel(null))
				{
					ch.WriteToDisplay("You have slipped and fallen.");
					if(roll < (50 + fallChance) - ch.GetWeaponSkillLevel(null))
					{ch.Stunned = (short)Rules.RollD(1, 2);}
                    Rules.DoDamage(ch, ch, fallDmg, false);
					return;
				}
				else
                {
                    Rules.Combat(ch, target, ch.GetInventoryItem(Globals.eWearLocation.Feet));
                }
			}
            else // at black belt level there is no chance to fall when jumpkicking, unless encumbered
			{
				if(EncumbDesc != "lightly")
				{
					if(EncumbDesc == "moderately"){fallChance += 2; fallDmg += Rules.RollD(1, 2);}
					else if(EncumbDesc == "heavily"){fallChance += 5; fallDmg += Rules.RollD(1, 3);}
                    else if (EncumbDesc == "severely") { fallChance += 8; fallDmg += Rules.RollD(1, 6); }

					roll = Rules.RollD(1, 100);

					if(roll < (20 + fallChance) - ch.GetWeaponSkillLevel(null))
					{
						ch.WriteToDisplay("You have slipped and fallen.");
						if(roll < (10 + fallChance) - ch.GetWeaponSkillLevel(null))
						{
                            ch.Stunned = (short)Rules.RollD(1, fallChance);
                        }
                        Rules.DoDamage(ch, ch, fallDmg, false);
					}
					else
                    {
                        Rules.Combat(ch, target, ch.GetInventoryItem(Globals.eWearLocation.Feet));
                    }
				}
				else
                {
                    Rules.Combat(ch, target, ch.GetInventoryItem(Globals.eWearLocation.Feet));
                }
			}
		}		

		public static void GiveAEKillExp(Character ch, Character target)
		{
            try
            {
                if (ch == null) { return; }
                if (target != null && target.IsSummoned) { return; }
                if (ch != null && (ch.IsDead || !ch.IsPC)) { return; }

                if (ch.MapID != target.MapID) { return; }

                int randomExpGain = Rules.dice.Next((int)-(target.Experience * .05), (int)(target.Experience * .05)); // random xp adjustment

                long expGain = Convert.ToInt64(target.Experience + randomExpGain); // total xp gain value

                expGain = (long)(expGain * .4);

                if (ch.IsPC && !target.IsPC)
                {
                    if (ch.Group != null)
                    {
                        ch.Group.GiveGroupExperience(ch, expGain, ch.Name + " has slain a foe.");
                    }
                    else
                    {
                        ch.Experience += expGain;
                    }
                    ch.Kills++;
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
		}

		public static void GiveKillExp(Character ch, Character target)
		{
            try
            {
                if (ch.IsDead) { return; }

                if (ch.MapID != target.MapID) { return; }
                if (target != null && target.IsSummoned) { return; }
                int randomExpGain = Rules.dice.Next((int)-(target.Experience * .05), (int)(target.Experience * .05)); // random xp adjustment

                long expGain = Convert.ToInt64(target.Experience + randomExpGain); // total xp gain value

                if (ch.IsPC && !target.IsPC) // give experience for killing non player characters only
                {
                    if (ch.Group != null)
                    {
                        ch.Group.GiveGroupExperience(ch, expGain, ch.Name + " has slain a foe.");
                        if (target.questFlags.Count > 0)
                        {
                            foreach (string flag in target.questFlags)
                            {
                                //TODO
                            }
                        }
                    }
                    else
                    {
                        ch.Experience += expGain;
                        if ((target as NPC).questFlags.Count > 0)
                        {
                            foreach (string flag in (target as NPC).questFlags)
                            {
                                string[] s = flag.Split(Protocol.VSPLIT.ToCharArray());
                                // verify that the player has the quest in order to get the flag
                                // if the questID parse results in 0 then the quest does not need to be started to get the flag
                                int questID = Convert.ToInt32(s[0]);
                                if (questID <= 0 || ch.GetQuest(questID) != null)
                                {
                                    if (ch.GetQuest(Convert.ToInt32(s[0])) != null)
                                    {
                                        if (!ch.questFlags.Contains(flag))
                                        {
                                            ch.questFlags.Add(flag);
                                            ch.WriteToDisplay("You have received a quest flag!");
                                        }
                                    }
                                }
                            }
                        }
                        Utils.Log(ch.GetLogString() + " earned " + expGain + " for killing " + target.GetLogString() + ".", Utils.LogType.ExperienceMeleeKill);
                        //if (Rules.levelCheck(ch.Experience) > currentLevel)
                        //{
                        //    if (ch.Hits < ch.HitsMax || ch.Stamina < ch.StaminaMax)
                        //    {
                        //        ch.WriteToDisplay("You have earned enough experience for your next level! Type REST when you are at full health and stamina to advance.");
                        //    }
                        //    else
                        //    {
                        //        ch.WriteToDisplay("You have earned enough experience for your next level! Type REST to advance.");
                        //    }
                        //}
                    }
                    ch.Kills++;
                }
                else if (!target.IsPC && !ch.IsPC)
                {
                    ch.Experience += expGain;
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
		}

        public static void Combat(Character attacker, Character target, Item weapon)
        {
            try
            {
                #region Special Block
                if (Rules.CheckSpecialBlock(attacker, target, weapon))
                {
                    // give some skill experience for learning this target has a special block
                    // also gives some skill to those sparring amoral demons and merchants (temporary)

                    if (target.merchantType > Merchant.MerchantType.None ||
                        target.trainerType > Merchant.TrainerType.None ||
                        target.Alignment == Globals.eAlignment.Amoral || target.IsImmortal)
                    {
                            attacker.WriteToDisplay("You miss!");
                            if (attacker.race != "")
                            {
                                target.WriteToDisplay(attacker.Name + " misses you.");
                            }
                            else
                            {
                                target.WriteToDisplay("The " + attacker.Name + " misses you.");
                            }
                    }
                    else
                    {
                        switch (attacker.CommandType)
                        {
                            case Command.CommandType.Shoot:
                            case Command.CommandType.Throw:
                                attacker.WriteToDisplay("Shot hits for little effect.");
                                break;
                            case Command.CommandType.Bash:
                            case Command.CommandType.Jumpkick:
                            case Command.CommandType.Kick:
                            case Command.CommandType.Poke:
                                attacker.WriteToDisplay(attacker.CommandType.ToString() + " hits for little effect.");
                                break;
                            default:
                                attacker.WriteToDisplay("Swing hits for little effect.");
                                break;
                        }
                        if (attacker.race != "")
                        {
                            target.WriteToDisplay(attacker.Name + " hits for little effect.");
                        }
                        else
                        {
                            target.WriteToDisplay("The " + attacker.Name + " hits for little effect.");
                        }
                    }
                    return; // end Combat due to special block
                }
                #endregion
            }
            catch (Exception e)
            {
                Utils.Log("Failed to check special block portion of Rules.doCombat.", Utils.LogType.SystemFailure);
                Utils.LogException(e);
            }

            try
            {
                int hitcheck = 0;

                try
                {
                    hitcheck = Rules.DND_RollToHit(attacker, target, weapon);
                }
                catch (Exception e)
                {
                    Utils.Log("Failed Rules.DNDrollToHit portion of Rules.doCombat.", Utils.LogType.SystemFailure);
                    Utils.LogException(e);
                }

                switch (hitcheck)
                {
                    case -1: // critical miss, possible fumble
                        #region critical miss
                        if (weapon == null || weapon != null &&
                                            (weapon == attacker.GetInventoryItem(Globals.eWearLocation.Hands) ||
                                            weapon == attacker.GetInventoryItem(Globals.eWearLocation.Feet)))
                        {
                            if (!attacker.animal)
                            {
                                attacker.EmitSound(Sound.GetCommonSound(Sound.CommonSound.UnarmedMiss));
                            }
                            else
                            {
                                attacker.EmitSound(Sound.GetCommonSound(Sound.CommonSound.MeleeMiss));
                            }

                            // critical miss jumpkick
                            if (attacker.CommandType == Command.CommandType.Jumpkick)
                            {
                                if (Skills.GetSkillLevel(attacker.GetSkillExperience(Globals.eSkillType.Unarmed)) < 7)
                                {
                                    attacker.WriteToDisplay("You have slipped and fallen.");
                                    if (Rules.RollD(1, 20) < 10)
                                    {
                                        attacker.Stunned = (short)Rules.RollD(1, 2); // stun the character
                                        attacker.WriteToDisplay("You are stunned!");
                                    }
                                    if (attacker.Hits > 4) // falling damage - don't kill the character on a critically failed jumpkick. tempting though.
                                        Rules.DoDamage(attacker, attacker, Rules.RollD(1, 4), false);
                                    else
                                        Rules.DoDamage(attacker, attacker, Rules.RollD(1, attacker.Hits - 1), false);
                                }
                                else attacker.WriteToDisplay("You miss!");
                            }
                            // critical miss kick
                            else if (attacker.CommandType == Command.CommandType.Kick)
                            {
                                attacker.WriteToDisplay("You have pulled a muscle.");
                                if (attacker.Hits > 2) // again, don't kill the character on a critically failed kick
                                    Rules.DoDamage(attacker, attacker, Rules.RollD(1, 2), false);
                                else
                                    Rules.DoDamage(attacker, attacker, Rules.RollD(1, attacker.Hits - 1), false);
                            }
                            else
                            {
                                attacker.WriteToDisplay("You miss!");
                                if (attacker.race != "")
                                {
                                    target.WriteToDisplay(attacker.Name + " misses you.");
                                }
                                else
                                {
                                    target.WriteToDisplay("The " + attacker.Name + " misses you.");
                                }
                            }
                        }
                        else
                        {
                            if (Rules.CheckFumble(attacker, weapon))
                            {
                                Rules.Fumble(attacker, weapon);
                            }
                            else
                            {
                                attacker.WriteToDisplay("You miss!");
                                if (attacker.race != "")
                                {
                                    target.WriteToDisplay(attacker.Name + " misses you.");
                                }
                                else
                                {
                                    target.WriteToDisplay("The " + attacker.Name + " misses you.");
                                }
                            }
                        }
                        break; 
                        #endregion
                    case 0: // miss
                        #region miss
                        if (Rules.CheckMiss(attacker, target, weapon))
                        {
                            if (weapon == null || weapon != null && (weapon.wearLocation == Globals.eWearLocation.Hands || weapon.wearLocation == Globals.eWearLocation.Feet))
                            {
                                if (!attacker.animal)
                                {
                                    attacker.EmitSound(Sound.GetCommonSound(Sound.CommonSound.UnarmedMiss));
                                }
                                else
                                {
                                    attacker.EmitSound(Sound.GetCommonSound(Sound.CommonSound.MeleeMiss));
                                }
                            }
                            else //TODO: range attack miss
                            {
                                attacker.EmitSound(Sound.GetCommonSound(Sound.CommonSound.MeleeMiss));
                            }

                            attacker.WriteToDisplay("You miss!");
                            if (attacker.race != "")
                            {
                                target.WriteToDisplay(attacker.Name + " misses you.");
                            }
                            else
                            {
                                target.WriteToDisplay("The " + attacker.Name + " misses you.");
                            }
                        }
                        break; 
                        #endregion
                    case 1: // normal hit
                        Rules.DND_Attack(attacker, target, weapon, false);
                        break;
                    case 2: // critical hit
                        if (attacker.GetWeaponSkillLevel(weapon) < 5) // critical hits (including stuns) not possible below skill level 5
                            Rules.DND_Attack(attacker, target, weapon, false);
                        else Rules.DND_Attack(attacker, target, weapon, true);
                        break;
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return;
            }
        }

        public static void Fumble(Character ch, Item weapon)
        {
            if (!ch.IsPC && ch.Group != null)
            {
                ch.Group.Remove((NPC)ch);
            }

            ch.WriteToDisplay("You fumble!");

            // fumble sound
            if (ch.IsPC)
            {
                if (ch.gender == Globals.eGender.Male)
                    ch.EmitSound(Sound.GetCommonSound(Sound.CommonSound.MaleFumble));
                else if (ch.gender == Globals.eGender.Female)
                    ch.EmitSound(Sound.GetCommonSound(Sound.CommonSound.FemaleFumble));
            }

            if (ch.race != "")
            {
                ch.SendToAllInSight(ch.Name + " fumbles.");
            }
            else
            {
                ch.SendToAllInSight("The " + ch.Name + " fumbles.");
            }

            if (weapon.baseType == Globals.eItemBaseType.Bow)
            {
                weapon.nocked = false;
            }
            else
            {
                if (weapon == ch.RightHand) // weapon from character's right hand
                {
                    ch.CurrentCell.Add(weapon);
                    ch.UnEquipRightHand(weapon);
                    
                }
                else if (weapon == ch.LeftHand) // weapon from character's left hand
                {
                    ch.CurrentCell.Add(weapon);
                    ch.UnEquipLeftHand(weapon);
                }
                else // weapon from character's belt
                {
                    for (int a = 0; a < ch.beltList.Count; a++)
                    {
                        if (weapon == ch.beltList[a])
                        {
                            ch.beltList.Remove(weapon);
                            ch.CurrentCell.Add(weapon);
                            break;
                        }
                    }
                }
            }
           
            if (weapon.lightning)  // bad things can happen when certain items are fumbled
            {
                if (Rules.RollD(1, 100) >= 50) // 50 percent chance a fumbled lightning weapon will cast
                {
                    Spell oldPrepped = ch.preppedSpell;
                    long oldMagic = ch.magic;

                    ch.preppedSpell = Spell.GetSpell("lightning");
                    ch.magic = Rules.dice.Next((int)((weapon.combatAdds + 1) * 100000 / 2), weapon.combatAdds + 1 * 100000);

                    ch.preppedSpell.CastSpell(ch, "");

                    ch.preppedSpell = oldPrepped;
                    ch.magic = oldMagic;

                    if (ch.IsDead) { return; }
                    //for (int a = 0; a < ch.CurrentCell.cellCharList.Count; a++)
                    //{
                    //    Rules.DoSpellDamage(ch, (Character)ch.CurrentCell.cellCharList[a], weapon, Rules.rollD((int)weapon.spellPower / 2, 12), "lightning");
                    //}
                }
            }

            if (weapon.baseType == Globals.eItemBaseType.Bottle || weapon.fragile) // certain items break when fumbled
            {
                if (Rules.RollD(1, 100) >= 50)
                {
                    ch.SendToAllInSight("You hear something shatter at " + ch.Name + "'s feet.");
                    ch.WriteToDisplay("The " + weapon.name + " shatters at your feet.");
                    ch.CurrentCell.Remove(weapon);
                }
            }
        }

		public static bool CheckFumble(Character ch, Item weapon)
		{
            if (weapon == null) { return false; } // no fumble if there is no weapon
            if (weapon == ch.GetInventoryItem(Globals.eWearLocation.Hands) || weapon == ch.GetInventoryItem(Globals.eWearLocation.Feet))
                return false;
            if (!ch.IsPC && weapon.attuneType != Globals.eAttuneType.None) { return false; } // prevent a weapon wielded by an npc that will be attuned from fumbling
            if (ch.fighterSpecialization == weapon.skillType) { return false; }  // no fumbles for fighters using their specialized skill
            if (13 - ch.GetWeaponSkillLevel(weapon) < RollD(1, 12)) // chance to fumble decreases the higher the skill is, no chance after skill 12
                return false;
            return true;
		}

		public static bool CheckSpecialBlock(Character ch, Character target, Item weapon)
		{
            // all merchants, trainers, and amoral creatures are always blocked
            if (target.merchantType > Merchant.MerchantType.None ||
                target.trainerType > Merchant.TrainerType.None ||
                target.Alignment == Globals.eAlignment.Amoral)
                return true;

            // always block if target is immortal
            if (target.IsImmortal)
                return true;

            // special weapon requirement field
            if (!target.IsPC)
            {
                NPC npc = (NPC)target;
                if (npc.WeaponRequirement != "")
                {
                    if (weapon == null || !weapon.special.ToLower().Contains(npc.WeaponRequirement.ToLower()))
                    {
                        return true;
                    }
                }
            }

            bool blocked = false;

			#region Blueglow Requirement
		    if(target.special.Contains("blueglow")) // blueglow weapon needed to hit this target
			{
				if(weapon == null)
				{
                    Globals.eWearLocation wearLoc = Globals.eWearLocation.Hands;
                    if (ch.CommandType == Command.CommandType.Kick || ch.CommandType == Command.CommandType.Jumpkick)
                        wearLoc = Globals.eWearLocation.Feet;

					foreach(Item wItem in ch.wearing)
					{
                        if (wItem.wearLocation == wearLoc && wItem.blueglow) { blocked = false; break; }
					}
					goto piercers;
				}
                else if (weapon.blueglow) { blocked = false; }
			} 
	        #endregion

            #region Silver Required
            else if (target.special.Contains("silver")) // silver weapon needed to hit this target
            {
                if (weapon == null)
                {
                    Globals.eWearLocation wearLoc = Globals.eWearLocation.Hands;
                    if (ch.CommandType == Command.CommandType.Kick || ch.CommandType == Command.CommandType.Jumpkick)
                        wearLoc = Globals.eWearLocation.Feet;
                    foreach (Item wItem in ch.wearing)
                    {
                        if (wItem.wearLocation == wearLoc && !wItem.silver) { blocked = true; break; }
                    }
                    goto piercers;
                }
                else if (!weapon.silver) { blocked = true; }
            } 
            #endregion

            #region Blueglow & Silver Required
            else if (target.special.Contains("blueglow") && target.special.Contains("silver")) // blueglow and silver weapon needed to hit this target
            {
                if (weapon == null)
                {
                    Globals.eWearLocation wearLoc = Globals.eWearLocation.Hands;
                    if (ch.CommandType == Command.CommandType.Kick || ch.CommandType == Command.CommandType.Jumpkick)
                        wearLoc = Globals.eWearLocation.Feet;
                    foreach (Item wItem in ch.wearing)
                    {
                        if (wItem.wearLocation == wearLoc)
                        {
                            if (!wItem.blueglow || !wItem.silver) { blocked = true; break; }
                        }
                    }
                    goto piercers;
                }
                if (!weapon.blueglow || !weapon.silver) { blocked = true; }
            } 
            #endregion

            piercers:
            #region Non Piercing Weapon Required (eg: skeletons)
		    if (target.special.Contains("nopierce")) // pierce weapons have no effect
			{
                if(weapon != null && weapon.special.Contains("pierce")){blocked = true;}
			} 
	        #endregion
            #region Blunt Required
            if (target.special.Contains("onlyblunt"))
            {
                if (weapon == null) { blocked = false; }
                else if (!weapon.special.Contains("blunt")) { blocked = true; }
            }
            #endregion
            #region Slash Required
            if (target.special.Contains("onlyslash"))
            {
                if (weapon == null) { blocked = true; }
                else if (!weapon.special.Contains("slash")) { blocked = true; }
            }
            #endregion
            #region Pierce Required
            if (target.special.Contains("onlypierce"))
            {                
                if (weapon == null) { blocked = true; }
                else if (!weapon.special.Contains("pierce")) { blocked = true; }
            }
            #endregion
            #region Only Normal weapons
            if (-1 != target.special.IndexOf("onlynormal"))
            {
                if (weapon == null) { blocked = true; }
                if (weapon.blueglow) { blocked = true; }
                if (weapon.silver) { blocked = true; }

            }
            #endregion
			return blocked;
		}

        public static bool CheckMiss(Character attacker, Character target, Item attackWeapon)
        {
            int attackRoll = attacker.AttackRoll;

            int thac0 = DND_GetModifiedTHAC0(attacker, attackWeapon);

            double targetAC = target.baseArmorClass;

            #region True Miss
            if (attackRoll <= thac0 + Rules.CalculateArmorClass(targetAC))
                return true;
            #endregion

            #region Dexterity Miss
            targetAC -= Rules.AC_GetDexterityArmorClassBonus(target);
            if(attackRoll <= thac0 + CalculateArmorClass(targetAC))
            {
                //Utils.Log("Attacker: " + attacker.getLogString() + " Target: " + target.getLogString() + " DEXTERITY_MISS: (attackRoll = " + attackRoll + " thac0 = " + thac0 + " targetDexAdd: " + target.dexterityAdd + ")", Utils.LogType.MissCheckRolls);
                return true;
            }
            #endregion

            #region Base Armor Class Block

            targetAC -= Rules.AC_GetArmorClassRating(target);

            if (attackRoll <= thac0 + CalculateArmorClass(targetAC))
            {
                if (target.baseArmorClass >= 10)
                {
                    if (target.RightHand == null)
                    {
                        if (!target.IsPC)
                        {
                            NPC npc = (NPC)target;
                            if (npc.animal || npc.blockString1 != null)
                            {
                                string blocktype = "";
                                int roll = Rules.RollD(1, 3);
                                switch (roll)
                                {
                                    case 1:
                                        blocktype = npc.blockString1;
                                        break;
                                    case 2:
                                        blocktype = npc.blockString2;
                                        break;
                                    case 3:
                                        blocktype = npc.blockString3;
                                        break;
                                }
                                if (blocktype == "")
                                {
                                    if (npc.animal)
                                    {
                                        blocktype = "You are blocked by a paw.";
                                    }
                                    else
                                    {
                                        blocktype = "You are blocked by a hand.";
                                    }
                                }
                                attacker.WriteToDisplay(blocktype);
                                if (attacker.race != "")
                                {
                                    target.WriteToDisplay(attacker.Name + " is blocked by your " + blocktype + ".");
                                }
                                else
                                {
                                    target.WriteToDisplay("The " + attacker.Name + " is blocked by your " + blocktype + ".");
                                }
                            }
                            else
                            {
                                attacker.WriteToDisplay("You are blocked by a hand.");
                                if (attacker.race != "")
                                {
                                    target.WriteToDisplay(attacker.Name + " is blocked by your hand.");
                                }
                                else
                                {
                                    target.WriteToDisplay("The " + attacker.Name + " is blocked by your hand.");
                                }
                            }
                        }
                        else
                        {
                            attacker.WriteToDisplay("You are blocked by a hand.");
                            if (attacker.race != "")
                            {
                                target.WriteToDisplay(attacker.Name + " is blocked by your hand.");
                            }
                            else
                            {
                                target.WriteToDisplay("The " + attacker.Name + " is blocked by your hand.");
                            }
                        }
                        return false;
                    }
                    return true; // return a true miss
                }
                else
                {
                    attacker.WriteToDisplay("You are blocked by the armor.");
                    if (attacker.race != "")
                    {
                        target.WriteToDisplay(attacker.Name + " is blocked by your armor.");
                    }
                    else
                    {
                        target.WriteToDisplay("The " + attacker.Name + " is blocked by your armor.");
                    }
                }
                return false;
            }
            #endregion

            #region Shield Spell Block
            if (target.Shielding > 0)
            {
                switch (attacker.CommandType)
                {
                    case Command.CommandType.Shoot:
                    case Command.CommandType.Throw:
                    case Command.CommandType.Jumpkick:
                        targetAC -= Rules.AC_GetShieldingArmorClass(target.Shielding, true);
                        break;
                    default:
                        targetAC -= Rules.AC_GetShieldingArmorClass(target.Shielding, false);
                        break;
                }

                if (attackRoll <= thac0 + CalculateArmorClass(targetAC))
                {
                    //Utils.Log("Attacker: " + attacker.getLogString() + " Target: " + target.getLogString() + " SHIELD_SPELL: (attackRoll = " + attackRoll + " thac0 = " + thac0 + " targetAC = " + targetAC, Utils.LogType.MissCheckRolls);

                    attacker.WriteToDisplay("You are blocked by a shield spell.");
                    if (attacker.race != "")
                    {
                        target.WriteToDisplay(attacker.Name + " is blocked by your shield spell.");
                    }
                    else
                    {
                        target.WriteToDisplay("The " + attacker.Name + " is blocked by your shield spell.");
                    }
                    return false;
                }
            }
            #endregion

            #region Unarmed Block
            targetAC -= Rules.AC_GetUnarmedArmorClassBonus(target);

                if (attackRoll <= thac0 + CalculateArmorClass(targetAC))
                {
                    if (target.RightHand == null)
                    {
                        if (target.animal)
                        {
                            NPC npc = (NPC)target;
                            string blocktype = "";
                            int roll = Rules.RollD(1, 3);
                            switch (roll)
                            {
                                case 1:
                                    blocktype = npc.blockString1;
                                    break;
                                case 2:
                                    blocktype = npc.blockString2;
                                    break;
                                case 3:
                                    blocktype = npc.blockString3;
                                    break;
                            }
                            if (blocktype == "") { blocktype = "You are blocked by a paw."; }
                            attacker.WriteToDisplay(blocktype);
                            //if (attacker.race != "")
                            //{
                            //    target.WriteToDisplay(attacker.Name + " is blocked by your " + blocktype + ".");
                            //}
                            //else
                            //{
                            //    target.WriteToDisplay("The " + attacker.Name + " is blocked by your " + blocktype + ".");
                            //}
                        }
                        else
                        {
                            target.EmitSound(Sound.GetCommonSound(Sound.CommonSound.HandBlock));
                            attacker.WriteToDisplay("You are blocked by a hand.");
                            if (attacker.race != "")
                            {
                                target.WriteToDisplay(attacker.Name + " is blocked by your hand.");
                            }
                            else
                            {
                                target.WriteToDisplay("The " + attacker.Name + " is blocked by your hand.");
                            }
                        }
                    }
                    else
                    {
                        attacker.WriteToDisplay("You are blocked by a " + target.RightHand.name + ".");
                        if (attacker.race != "")
                        {
                            target.WriteToDisplay(attacker.Name + " is blocked by your " + target.RightHand.name + ".");
                        }
                        else
                        {
                            target.WriteToDisplay("The " + attacker.Name + " is blocked by your " + target.RightHand.name + ".");
                        }
                    }
                    return false;
                }
                #endregion

            #region Held Item (WEAPON, MISCELLANEOUS) Armor Class Block (non skill related)
            if (target.RightHand != null && !target.RightHand.IsAttunedToOther(target) &&
                target.RightHand.AlignmentCheck(target))
            {
                targetAC -= Rules.AC_GetRightHandArmorClass(target);

                if (attackRoll <= thac0 + CalculateArmorClass(targetAC))
                {
                    attacker.WriteToDisplay("You are blocked by a " + target.RightHand.name + ".");
                    if (attacker.race != "")
                    {
                        target.WriteToDisplay(attacker.Name + " is blocked by your " + target.RightHand.name + ".");
                    }
                    else
                    {
                        target.WriteToDisplay("The " + attacker.Name + " is blocked by your " + target.RightHand.name + ".");
                    }
                    return false;
                }
            }

            if (target.LeftHand != null && !target.LeftHand.IsAttunedToOther(target) &&
                target.LeftHand.AlignmentCheck(target))
            {
                targetAC -= Rules.AC_GetLeftHandArmorClass(target);

                if (attackRoll <= thac0 + CalculateArmorClass(targetAC))
                {
                    attacker.WriteToDisplay("You are blocked by a " + target.LeftHand.name + ".");
                    if (attacker.race != "")
                    {
                        target.WriteToDisplay(attacker.Name + " is blocked by your " + target.LeftHand.name + ".");
                    }
                    else
                    {
                        target.WriteToDisplay("The " + attacker.Name + " is blocked by your " + target.LeftHand.name + ".");
                    }
                    return false;
                }
            }
            #endregion

            #region Armor Block

            targetAC -= Rules.AC_GetArmorClassRating(target);

            //Utils.Log("Attacker: " + attacker.getLogString() + " Target: " + target.getLogString() + " ARMOR_BLOCK: (attackRoll = " + attackRoll + " thac0 = " + thac0 + " targetAC = " + targetAC, Utils.LogType.MissCheckRolls);
            if (attackRoll <= thac0 + CalculateArmorClass(targetAC))
            {
                attacker.WriteToDisplay("You are blocked by the armor.");
                if (attacker.race != "")
                {
                    target.WriteToDisplay(attacker.Name + " is blocked by your armor.");
                }
                else
                {
                    target.WriteToDisplay("The " + attacker.Name + " is blocked by your armor.");
                }
                return false;
            }
            #endregion

            #region If all other misses do not return, then do a hand block in most instances...
            else if (!target.IsBlind && !target.IsFeared && target.Stunned <= 0)
            {
                if (target.RightHand == null)
                {
                    if (target.animal)
                    {
                        NPC npc = (NPC)target;
                        string blocktype = "";
                        int roll = Rules.RollD(1, 3);
                        switch (roll)
                        {
                            case 1:
                                blocktype = npc.blockString1;
                                break;
                            case 2:
                                blocktype = npc.blockString2;
                                break;
                            case 3:
                                blocktype = npc.blockString3;
                                break;
                        }
                        if (blocktype == "") { blocktype = "You are blocked by a paw."; }
                        attacker.WriteToDisplay(blocktype);
                        if (attacker.race != "")
                        {
                            target.WriteToDisplay(attacker.Name + " is blocked by your " + blocktype + ".");
                        }
                        else
                        {
                            target.WriteToDisplay("The " + attacker.Name + " is blocked by your " + blocktype + ".");
                        }
                    }
                    else
                    {
                        attacker.WriteToDisplay("You are blocked by a hand.");
                        if (attacker.race != "")
                        {
                            target.WriteToDisplay(attacker.Name + " is blocked by your hand.");
                        }
                        else
                        {
                            target.WriteToDisplay("The " + attacker.Name + " is blocked by your hand.");
                        }
                    }
                }
                else
                {
                    if (target.RightHand.name != "greataxe")
                    {
                        attacker.WriteToDisplay("You are blocked by a " + target.RightHand.name + ".");
                        if (attacker.race != "")
                        {
                            target.WriteToDisplay(attacker.Name + " is blocked by your " + target.RightHand.name + ".");
                        }
                        else
                        {
                            target.WriteToDisplay("The " + attacker.Name + " is blocked by your " + target.RightHand.name + ".");
                        }
                    }
                    else
                    {
                        attacker.WriteToDisplay("You are blocked by the armor.");
                        if (attacker.race != "")
                        {
                            target.WriteToDisplay(attacker.Name + " is blocked by your armor.");
                        }
                        else
                        {
                            target.WriteToDisplay("The " + attacker.Name + " is blocked by your armor.");
                        }
                    }
                }
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }

        public static bool CheckSpellFailure(int casterLevel, int spellLevel)
        {
            if (casterLevel < spellLevel && RollD(1, 20) < spellLevel)
                return true;

            return false;
        }

        public static int CalculateArmorClass(double ac)
        {
            if (ac > 0)
                return (int)(-(ac));
            else return (int)Math.Abs(ac);
        }

        public static int CalculateUnarmedDamage(Character ch)
        {
            if (ch.BaseProfession == Character.ClassType.Martial_Artist) // attacker is a martial artist
            {
                if (ch.CommandType == Command.CommandType.Jumpkick)
                    return Rules.RollD(ch.GetWeaponSkillLevel(null), 5);
                else if (ch.CommandType == Command.CommandType.Kick)
                    return Rules.RollD(ch.GetWeaponSkillLevel(null), 4);
                else
                    return Rules.RollD(ch.GetWeaponSkillLevel(null), 3);
            }
            else if (!ch.IsPC) // attacker is not a player
            {
                NPC npc = (NPC)ch;

                if (npc.lairCritter)
                {
                    if (ch.CommandType == Command.CommandType.Jumpkick)
                        return Rules.RollD(ch.GetWeaponSkillLevel(null), 7);
                    else if (ch.CommandType == Command.CommandType.Kick)
                        return Rules.RollD(ch.GetWeaponSkillLevel(null), 6);
                    else
                    {
                        return Rules.RollD(ch.GetWeaponSkillLevel(null), 5);
                    }
                }
                else
                {
                    if (ch.CommandType == Command.CommandType.Jumpkick)
                        return Rules.RollD(ch.GetWeaponSkillLevel(null), 3);
                    else if (ch.CommandType == Command.CommandType.Kick)
                        return Rules.RollD(ch.GetWeaponSkillLevel(null), 2);
                    else
                        return (int)(Rules.RollD(ch.GetWeaponSkillLevel(null), 2) / 1.5);
                }
            }
            else // attacker is a non martial artist player using unarmed skill
            {
                if (ch.CommandType == Command.CommandType.Jumpkick)
                    return Rules.RollD(ch.GetWeaponSkillLevel(null), 3);
                else if (ch.CommandType == Command.CommandType.Kick)
                    return Rules.RollD(ch.GetWeaponSkillLevel(null), 2);
                else
                    return (int)(Rules.RollD(ch.GetWeaponSkillLevel(null), 2) / 1.5);
            }
        }

		public static int CalculateWeaponDamage(Character ch, Item weapon) // weapon min and max damage random, plus strength add
		{
			int minDamage = weapon.minDamage;
			int maxDamage = weapon.maxDamage;

            if (maxDamage <= 0) { maxDamage = (int)ch.Strength / 4; }
            if (minDamage <= 0) { minDamage = 1; }

			if(minDamage > maxDamage)
			{
				minDamage = 1;
				maxDamage = (int)ch.Strength / 4;
			}

            if (weapon.skillType == Globals.eSkillType.Unarmed)
            {
                return CalculateUnarmedDamage(ch) + Rules.dice.Next(minDamage, maxDamage + 1) + weapon.combatAdds;
            }

            return Rules.dice.Next(minDamage, maxDamage + 1) + weapon.combatAdds;
		}

		public static void DoDamage(Character target, Character damager, int damage, bool critical) // all non spell damage
		{
			if(target.IsPeeking || target.IsWizardEye)
			{
                if (target.race != "")
                {
                    target.SendToAllInSight(target.Name + " is slain!");
                }
                else
                {
                    target.SendToAllInSight("The " + target.Name + " is slain!");
                }

                target.WriteToDisplay("You have been slain!");

				Rules.DoDeath(target, null);
                return;
			}

			if(target.IsHidden) // break hide
			{
                target.IsHidden = false;
			}

			if(damage > 0)
			{
                if (target.preppedSpell != null)
                {
                    if (damage >= (int)target.Hits * .50 && damage < target.Hits) // if spell is prepped, and more than 50% of remaining hits are taken in damage, clear the preppedspell
                    {
                        target.preppedSpell = null;
                        target.WriteToDisplay("Your spell has been lost.");
                        target.EmitSound(Sound.GetCommonSound(Sound.CommonSound.SpellFail));
                    }
                }

				target.Hits -= damage;
				target.damageRound = DragonsSpineMain.GameRound;
			}

            if (target.Stunned <= 0 && critical && !target.immuneStun)
			{
                int saveMod = -(target.ZonkResistance);

                // damager used a bash
                if (damager.CommandType == Command.CommandType.Bash)
                {
                    saveMod += Math.Max(target.Level, Skills.GetSkillLevel(damager.GetSkillExperience(Globals.eSkillType.Bash))) -
                        Math.Min(target.Level, Skills.GetSkillLevel(damager.GetSkillExperience(Globals.eSkillType.Bash)));
                }

                

                if (!DND_GetSavingThrow(target, SavingThrow.PetrificationPolymorph, saveMod))
                {
                    // stun for 1 round if bash save failed, 1 - 2 for others
                    if (damager.CommandType == Command.CommandType.Bash)
                        target.Stunned = 1;
                    else target.Stunned += (short)RollD(1, 2);

                    if (target.Hits > 0)
                    {
                        if (target.race != "")
                        {
                            target.SendToAllInSight(target.Name + " is stunned!");
                        }
                        else
                        {
                            target.SendToAllInSight("The " + target.Name + " is stunned!");
                        }

                        target.WriteToDisplay("You have been stunned by the blow!");
                    }
                }
			}
            else if (target.Stunned <= 0 && critical && target.immuneStun)
            {
                if (damager.CommandType == Command.CommandType.Bash)
                {
                    if (target.race != "")
                    {
                        damager.WriteToDisplay(target.Name + " is not affected by your bash stun.");
                    }
                    else
                    {
                        damager.WriteToDisplay("The " + target.Name + " is not affected by your bash stun.");
                    }
                }
                else
                {
                    if (target.race != "")
                    {
                        damager.WriteToDisplay(target.Name + " is not affected by your physical stun.");
                    }
                    else
                    {
                        damager.WriteToDisplay("The " + target.Name + " is not affected by your physical stun.");
                    }
                }
            }

			if(target.Hits <= 0)
			{
                //if(!target.IsDead)
                //{
                    if (target.race != "")
                    {
                        target.SendToAllInSight(target.Name + " is slain!");
                    }
                    else
                    {
                        target.SendToAllInSight("The " + target.Name + " is slain!");
                    }

                    target.WriteToDisplay("You have been slain!");

                    if (damager.IsPC && !damager.IsImmortal) // if slayer is a player
                    {
                        Rules.GiveKillExp(damager, target); // give kill experience
                    }
                    else if (damager.IsSummoned) // if the slayer is summoned (demons / figurines)
                    {
                        Rules.GiveKillExp(damager, target);
                    }

					Rules.DoDeath(target, damager);
				//}

				if(target.IsPC)                 
				{
					target.updateHits = true;
				}
			}
		}

		public static int DoSpellDamage(Character caster, Character target, Item item, int damage, string spellType)
		{
            if ((target != null && target.IsDead) || (caster != null && caster.IsDead)) { return 0; }
            if (target != null && target.Alignment == Globals.eAlignment.Amoral) { return 0; }
            if (target != null && target.merchantType != Merchant.MerchantType.None) { return 0; }
            if (target != null && target.trainerType != Merchant.TrainerType.None) { return 0; }
            if (target != null && !target.IsPC && (target as NPC).IsMobile == false) { return 0; } 

			int protection = 0;
			int lvlDiff = 0;
			int conDiff = 0;
			int strDiff = 0;
			bool stunChance = false;
            int totalDamage = 0;
            string damageType = "";

            SavingThrow savingThrow = SavingThrow.Spell;

            if(item != null)
            {
                savingThrow = SavingThrow.RodStaffWand;
            }

            if (caster != null && caster.IsPC || target != null && target.IsPC)
            {
                Rules.DoFlag(caster, target);
            }

			try
			{

                int casterSL = 0;
                if ((caster != null && target != null) && (caster != target))
                {
                    casterSL = (Skills.GetSkillLevel(caster.magic) * 2) - target.Level;
                }

                switch (spellType.ToLower())
                {
                    case "curse":
                        #region Immune to Curse
                        if (target.immuneCurse)
                        {
                            if (caster != null)
                            {
                                if (target.race != "")
                                {
                                    caster.WriteToDisplay(target.Name + " is immune to curse based damage.");
                                }
                                else
                                {
                                    caster.WriteToDisplay("The " + target.Name + " is immune to curse based damage.");
                                }
                            }
                            damage = 0;
                        }
                        damageType = "curse spell";
                        #endregion
                        //else if (DND_GetSavingThrow(target, savingThrow, 0)) { damage = (int)damage / 2; }
                        break;
                    case "death":
                        #region Immune to Death
                        if (target.immuneDeath)
                        {
                            if (caster != null)
                            {
                                if (target.race != "")
                                {
                                    caster.WriteToDisplay(target.Name + " is immune to death based magic.");
                                }
                                else
                                {
                                    caster.WriteToDisplay("The " + target.Name + " is immune to death based magic.");
                                }
                            }
                            damage = 0;
                        }
                        #endregion
                        else if (DND_GetSavingThrow(target, savingThrow, -(target.DeathResistance) + casterSL)) { damage = (int)damage / 2; }
                        protection = target.DeathProtection;
                        damageType = "death spell";
                        break;
                    case "stun":
                        #region Immune to Stun
                        if (target.immuneStun)
                        {
                            if (caster != null)
                            {
                                if (target.race != "")
                                {
                                    caster.WriteToDisplay(target.Name + " is immune to stun based magic.");
                                }
                                else
                                {
                                    caster.WriteToDisplay("The " + target.Name + " is immune to stun based magic.");
                                }
                            }
                            return 0;
                        }
                        #endregion
                        else if (DND_GetSavingThrow(target, savingThrow, -(target.StunResistance) + casterSL))
                        {
                            if (caster != null)
                            {
                                if (target.race != "")
                                {
                                    caster.WriteToDisplay(target.Name + " resists your Stun spell.");
                                }
                                else
                                {
                                    caster.WriteToDisplay("The " + target.Name + " resists your Stun spell.");
                                }
                            }
                            return 0;
                        }
                        return 1;
                    case "fear":
                        #region Immune to Fear
                        if (target.immuneFear)
                        {
                            if (caster != null)
                            {
                                if (target.race != "")
                                {
                                    caster.WriteToDisplay(target.Name + " is immune to fear based magic.");
                                }
                                else
                                {
                                    caster.WriteToDisplay("The " + target.Name + " is immune to fear based magic.");
                                }
                            }
                            return 0;
                        }
                        #endregion
                        else if (DND_GetSavingThrow(target, savingThrow, -(target.FearResistance) + casterSL)) { return 0; }
                        return 1;
                    case "dragon fear":
                        if (target.Level > caster.Level)
                        {
                            return 0;
                        } // dragon fear won't work on targets higher level than the dragon - they're heros.
                        // otherwise there's a good chance it will work
                        lvlDiff = caster.Level - target.Level;
                        conDiff = (caster.Constitution + caster.TempConstitution) - (target.Constitution + target.TempConstitution);
                        if (Rules.dice.Next(3, caster.Constitution + 1) + lvlDiff + conDiff > target.Constitution + target.TempConstitution)
                        {
                            return 1;
                        }
                        return 0;
                    case "giant stomp":
                        if (target.Level > caster.Level) { return 0; } // giant stomp won't work in this situation
                        // otherwise there's a good chance it will work
                        lvlDiff = caster.Level - target.Level;
                        strDiff = (caster.Strength + caster.TempStrength) - (target.Strength + target.TempStrength);
                        if (Rules.dice.Next(3, caster.Strength + 1) + lvlDiff + strDiff > target.Strength + target.TempStrength) { return 1; }
                        return 0;
                    case "turnundead":
                        #region Turn Undead
                        if (target.IsUndead)
                        {
                            //chance of undead being turned based on target level and caster magic level
                            if (target.Level >= Skills.GetSkillLevel(caster.magic))
                            {
                                switch (target.Level - Skills.GetSkillLevel(caster.magic))
                                {
                                    case 0:
                                        if (Rules.RollD(1, 100) >= 90)
                                        { damage = (int)(damage / 2); }
                                        else { damage = target.Hits; }
                                        break;
                                    case 1:
                                        if (Rules.RollD(1, 100) >= 80)
                                        { damage = (int)(damage / 2); }
                                        else { damage = target.Hits; }
                                        break;
                                    case 2:
                                        if (Rules.RollD(1, 100) >= 70)
                                        { damage = (int)(damage / 2); }
                                        else { damage = target.Hits; }
                                        break;
                                    case 3:
                                        if (Rules.RollD(1, 100) >= 60)
                                        { damage = (int)(damage / 2); }
                                        else { damage = target.Hits; }
                                        break;
                                    case 4:
                                        if (Rules.RollD(1, 100) >= 50)
                                        { damage = (int)(damage / 2); }
                                        else { damage = target.Hits; }
                                        break;
                                    default:
                                        damage = 0;
                                        if (target.race != "")
                                        { caster.WriteToDisplay(target.Name + " is unaffected by your prayers."); }
                                        else { caster.WriteToDisplay("The " + target.Name + " is unaffected by your prayers."); }
                                        break;
                                }
                            }
                            else
                            {
                                damage = target.Hits;
                            }
                        }
                        else
                        {
                            damage = 0;
                        }
                        break; 
                        #endregion
                    case "light":
                        int effectPower = 0;
                        if (target.IsUndead)
                        {
                            switch (caster.BaseProfession)
                            {
                                case Character.ClassType.Thaumaturge:
                                    effectPower = Rules.RollD(1, 6);
                                    break;
                                case Character.ClassType.Knight:
                                    effectPower = Rules.RollD(1, 4);
                                    break;
                                case Character.ClassType.Wizard:
                                    effectPower = Rules.RollD(1, 3);
                                    break;
                                default:
                                    effectPower = Rules.RollD(1, 2);
                                    break;
                            }
                            damage = damage * effectPower;
                        }
                        else if(!target.IsUndead || target.Level > 9)
                        {
                            damage = 0;
                        }
                        break;
                    case "lightning":
                        #region Immune to Lightning
                        if (target.immuneLightning)
                        {
                            if (caster != null)
                            {
                                if (target != caster)
                                {
                                    if (target.race != "")
                                    {
                                        caster.WriteToDisplay(target.Name + " is immune to lightning based damage.");
                                    }
                                    else
                                    {
                                        caster.WriteToDisplay("The " + target.Name + " is immune to lightning based damage.");
                                    }
                                }
                            }
                            damage = 0;
                        }
                        #endregion
                        else if (DND_GetSavingThrow(target, savingThrow, -(target.LightningResistance) + casterSL)) { damage = (int)damage / 2; }
                        else { stunChance = true; }
                        protection = target.LightningProtection - Skills.GetSkillLevel(caster.magic);
                        damageType = "lightning bolt";
                        break;
                    case "firebolt":
                    case "fire":
                        #region Immune to Fire
                        if (target.immuneFire)
                        {
                            if (caster != null)
                            {
                                if (target != caster)
                                {
                                    if (target.race != "")
                                    {
                                        caster.WriteToDisplay(target.Name + " is immune to fire based damage.");
                                    }
                                    else
                                    {
                                        caster.WriteToDisplay("The " + target.Name + " is immune to fire based damage.");
                                    }
                                }
                            }
                            damage = 0;
                        }
                        #endregion
                        else if (DND_GetSavingThrow(target, savingThrow, -(target.FireResistance) + casterSL)) { damage = (int)damage / 2; }
                        if (caster != null)
                        {
                            protection = target.FireProtection - Skills.GetSkillLevel(caster.magic);
                        }
                        else
                        {
                            protection = target.FireProtection;
                        }
                        break;
                    case "dragon's breath fire":
                        if (target.species == Globals.eSpecies.FireDragon) { damage = 0; }
                        // protection from dragon's breath is lessened - it's a more powerful element
                        else if (DND_GetSavingThrow(target, SavingThrow.BreathWeapon, 0)) { damage = (int)damage / 2; }
                        protection = target.FireProtection - Skills.GetSkillLevel(caster.magic);
                        break;
                    case "dragon's breath ice":
                        if (target.species == Globals.eSpecies.IceDragon) { damage = 0; }
                        else if (DND_GetSavingThrow(target, SavingThrow.BreathWeapon, 0)) { damage = (int)damage / 2; }
                        protection = target.ColdProtection - Skills.GetSkillLevel(caster.magic);
                        break;
                    case "ice":
                        #region Immune to Cold
                        if (target.immuneCold)
                        {
                            if (caster != null)
                            {
                                if (target != caster)
                                {
                                    if (target.race != "")
                                    {
                                        caster.WriteToDisplay(target.Name + " is immune to cold based damage.");
                                    }
                                    else
                                    {
                                        caster.WriteToDisplay("The " + target.Name + " is immune to cold based damage.");
                                    }
                                }
                            }
                            damage = 0;
                        }
                        #endregion
                        else if (DND_GetSavingThrow(target, savingThrow, -(target.ColdResistance) + casterSL)) { damage = (int)damage / 2; }
                        if (caster != null)
                        {
                            protection = target.ColdProtection - Skills.GetSkillLevel(caster.magic);
                        }
                        else
                        {
                            protection = target.ColdProtection;
                        }
                        break;
                    case "concussion":
                        if (target.ZonkResistance < damage)
                        {
                            stunChance = true;
                        }
                        if (target.species == Globals.eSpecies.Thisson)
                        {
                            damage = damage / 2;
                            stunChance = false;
                        }
                        break;
                    case "illusion":
                        if (!Rules.CheckPerception(target))
                        {
                            break;
                        }
                        return 0;
                    case "disintegrate":
                        if (DND_GetSavingThrow(target, SavingThrow.PetrificationPolymorph, 0))
                        {
                            damage = (int)damage / 2;
                        }
                        break;
                    case "web":
                        return 0;
                    case "darkness":
                        return 0;
                    case "whirlwind":
                        if (target.species == Globals.eSpecies.WindDragon)
                        {
                            damage = 0;
                        }
                        else
                        {
                            stunChance = true;
                            int tempdam = damage * 3;
                            damage = Rules.dice.Next(damage, tempdam);
                        }
                        break;
                    default:
                        break;
                }

                totalDamage = damage;
                //Protections based on a percentage of the total damage.
                 if(protection > 0)
				{
                    if (protection > 100) { protection = 100; }
                    double dPercent = 100 - protection;
                    dPercent = .01 * dPercent;
                    totalDamage =(int)(totalDamage * dPercent);
					if (damage != 0)
                    {
                        if (totalDamage <= 0) { totalDamage = 1; }
                    }
				}
				
				if(protection < 0) // take into account negative protection (in the form of debuffs and creature limitations?)
				{
					totalDamage += Math.Abs(protection);
				}
                if (caster != null && target != null && caster.IsPC)
                {
                    // display combat damage if applicable
                    if (caster.displayCombatDamage && (System.Configuration.ConfigurationManager.AppSettings["DisplayCombatDamage"].ToLower() == "true" || caster.ImpLevel >= Globals.eImpLevel.DEV))
                        caster.WriteToDisplay("Your " + spellType + " based spell does (" + totalDamage + ") to " + target.Name, Protocol.TextType.SpellHit);
                }
                #region Total Damage > 0
                if (totalDamage > 0)
                {
                    if (target != null && target.IsPC && damageType != "")
                    {
                        target.WriteToDisplay("You have been hit by a "+ damageType+".",Protocol.TextType.SpellHit);
                    }
                    if (target.preppedSpell != null) // if damage is greater than 15% of remaining hits then lose prepped spell
                    {
                        if (totalDamage >= (int)target.Hits * .50 && totalDamage < target.Hits)
                        {
                            target.preppedSpell = null;
                            target.WriteToDisplay("Your spell has been lost.");
                            target.EmitSound(Sound.GetCommonSound(Sound.CommonSound.SpellFail));
                        }
                    }

                    target.Hits -= totalDamage;
                    target.updateHits = true;
                    target.damageRound = DragonsSpineMain.GameRound;

                    #region Damage Logging
                    if (!target.IsPC && caster != null)
                    {
                        Utils.Log(target.GetLogString() + " took (Damage: " + damage + " - Protection: " + protection + ") = " + totalDamage + " from " + caster.GetLogString() + " " + spellType.ToUpper() + " Health: " + target.Hits, Utils.LogType.SpellDamageToCreature);
                    }
                    else if (target.IsPC && caster != null)
                    {
                        Utils.Log(target.GetLogString() + " took (Damage: " + damage + " - Protection: " + protection + ") = " + totalDamage + " from " + caster.GetLogString() + " " + spellType.ToUpper() + " Health: " + target.Hits, Utils.LogType.SpellDamageToPlayer);
                    }
                    else
                    {
                        if(damage > 1)
                            Utils.Log(target.GetLogString() + " took (Damage: " + damage + " - Protection: " + protection + ") = " + totalDamage + " from " + spellType.ToUpper() + " at (" + target.X + ", " + target.Y + ") Health: " + target.Hits, Utils.LogType.SpellDamageFromMapEffect);
                    }
                    #endregion

                    if (target.Hits <= 0)
                    {
                        if (target.race != "")
                        {
                            target.SendToAllInSight(target.Name + " is slain!");
                        }
                        else
                        {
                            target.SendToAllInSight("The " + target.Name + " is slain!");
                        }

                        target.WriteToDisplay("You have been slain!");
                        Rules.DoDeath(target, caster);
                        if (caster != null && caster.IsPC) { return 1; }
                        return 0; // only return 1 if target is not a player (1 = give skill and experience)
                    }

                    if (target.IsHidden) // if target is hidden break the hide spell
                    {
                        target.IsHidden = false;
                    }
                    // if the target is stunned by this spell damage
                    if (stunChance && target.Stunned <= 0)
                    {
                        int modifier = -(target.ZonkResistance);

                        if (spellType.ToLower().IndexOf("lightning") != -1)
                        {
                            modifier = (int)(target.LightningResistance / 2);
                        }

                        if (!DND_GetSavingThrow(target, SavingThrow.PetrificationPolymorph, modifier))
                        {
                            target.Stunned = (short)(Rules.RollD(1, 2));
                            if (target.race != "")
                            {
                                target.SendToAllInSight(target.Name + " is stunned!");
                            }
                            else
                            {
                                target.SendToAllInSight("The " + target.Name + " is stunned!");
                            }
                            target.WriteToDisplay("You are stunned!");
                            if (target.preppedSpell != null)
                            {
                                target.preppedSpell = null;
                                target.WriteToDisplay("Your spell has been lost.");
                            }
                        }
                    }
                    return 0;
                }
                #endregion

                #region Total Damage <= 0
                else
                {
                    if (!target.IsPC && caster != null)
                    {
                        Utils.Log(target.GetLogString() + " took (Damage: " + damage + " - Protection: " + protection + ") = " + totalDamage + " from " + caster.GetLogString() + " " + spellType.ToUpper() + " Health: " + target.Hits, Utils.LogType.SpellDamageToCreature);
                    }
                    else if (target.IsPC && caster != null)
                    {
                        Utils.Log(target.GetLogString() + " took (Damage: " + damage + " - Protection: " + protection + ") = " + totalDamage + " from " + caster.GetLogString() + " " + spellType.ToUpper() + " Health: " + target.Hits, Utils.LogType.SpellDamageToPlayer);
                    }
                    else
                    {
                        if(damage > 1)
                            Utils.Log(target.GetLogString() + " took (Damage: " + damage + " - Protection: " + protection + ") = " + totalDamage + " from " + spellType.ToUpper() + " at (" + target.X + ", " + target.Y + ") Health: " + target.Hits, Utils.LogType.SpellDamageFromMapEffect);
                    }
                }
                return 0; 
                #endregion
			}
			catch(Exception e)
			{
                Utils.LogException(e);
				return 0;
			}
		}

		public static void DoConcussionDamage(Cell cell, int damage, Character cause)
		{
            try
            {
                foreach (Character chr in new List<Character>(cell.Characters))
                {
                    chr.WriteToDisplay("You have been hit by an explosion!");
                    if (Rules.DoSpellDamage(cause, chr, null, damage, "concussion") == 1)
                    {
                        Rules.GiveAEKillExp(cause, chr);
                    }
                }
                
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
		}

        public static void MakeSpecialCorpse(NPC target)
        {
            // do not drop worn items for spectral or summoned creatures
            if (!target.IsSpectral && !target.IsSummoned)
            {
                foreach (Item item in new List<Item>(target.wearing))
                {
                    if (item != null)
                    {
                        target.CurrentCell.Add(item);
                    }
                }
            }

            foreach (Item item in new List<Item>(target.sackList))
            {
                if (item != null)
                {
                    target.CurrentCell.Add(item);
                }
            }

            foreach (Item item in new List<Item>(target.beltList))
            {
                if (item != null)
                {
                    target.CurrentCell.Add(item);
                }
            }
            
            if (target.RightHand != null)
            {
                target.CurrentCell.Add(target.RightHand);
            }

            if (target.LeftHand != null)
            {
                target.CurrentCell.Add(target.LeftHand);
            }

            // decrement the number of this NPC in the list
            if (World.GetFacetByID(target.FacetID).Spawns.ContainsKey(target.SpawnZoneID))
            {
                World.GetFacetByID(target.FacetID).Spawns[target.SpawnZoneID].NumberInZone -= 1;
            }

            target.RemoveFromWorld();
        }

		public static void MakeCorpse(Character target)
		{
            if (target == null)
                return;

			Item corpse = Item.CopyItemFromDictionary(Item.ID_CORPSE);

            if (target.immuneFire)
            {
                corpse.flammable = false;
            }

			if(target.race != "")
			{
                corpse.longDesc = "the corpse of " + target.Name;
            }
			else
            {
                corpse.longDesc = "the corpse of " + target.longDesc;
            }

			corpse.weight = target.Strength * 10;

            if (target.IsPC)
            {
                corpse.itemID = 600000;
                corpse.special = target.Name;
                corpse.longDesc = "the corpse of " + target.Name;
                //removeSack(target,corpse);
                //removeBelt(target,corpse);
                //removeArmor(target,corpse);
                //removeRings(target,corpse);
                //target.sackList.Clear();
                //target.beltList.Clear();
                //target.wearing.Clear();
                target.CurrentCell.Add(corpse);
            }
            else
            {
                NPC npc = target as NPC;

                Rules.CorpseSack(npc as Character, corpse);
                Rules.CorpseBelt(npc as Character, corpse);
                Rules.CorpseWearing(npc as Character, corpse);
                Rules.CorpseRings(npc as Character, corpse);

                if (npc.tanningResult != null)
                {
                    corpse.vRandHigh = 66666;
                    corpse.special = Utils.ConvertIntArrayToString(npc.tanningResult);
                }

                // decrease number of spawns
                if (World.GetFacetByID(npc.FacetID).Spawns.ContainsKey(npc.SpawnZoneID))
                {
                    World.GetFacetByID(npc.FacetID).Spawns[npc.SpawnZoneID].NumberInZone -= 1;
                }

                npc.CurrentCell.Add(corpse); // place corpse on ground
                npc.RemoveFromWorld(); // done with the npc - remove it.
            }
		}

        public static void UnderworldDeadRest(Character chr)
        {
            if (chr.currentKarma > 0)
            {
                chr.CurrentCell = Cell.GetCell(chr.FacetID, chr.LandID, chr.MapID, chr.Map.KarmaResX, chr.Map.KarmaResY, chr.Map.KarmaResZ);
            }
            else
            {
                chr.CurrentCell = Cell.GetCell(chr.FacetID, chr.LandID, chr.MapID, chr.Map.ResX, chr.Map.ResY, chr.Map.ResZ);
            }
            
            chr.IsDead = false;
            chr.Hits = chr.HitsMax;
            chr.updateAll = true;
            chr.Stamina = chr.StaminaMax;
            chr.IsHidden = false;
            chr.IsInvisible = false;
            chr.Stunned = 0;
            chr.Mana = 0;
        }

        public static void DeadRest(Character chr) // called when a dead player uses the rest command
        {
            try
            {
                //remove the player's corpse from the cell 
                //TODO: Instead of removing the corpse, dump players items into corpse - strip player
                foreach (Item item in chr.CurrentCell.Items)
                {
                    if (item.special == chr.Name)
                    {
                        chr.CurrentCell.Remove(item);
                        break;
                    }
                }

                chr.WriteToDisplay("You've gained the attention of a passing Ghod!");

                chr.SendSound(Sound.GetCommonSound(Sound.CommonSound.DeathRevive));

                #region Skill loss at death - top 2 highest skills - DISABLED
                //Globals.eSkillType[] skillsToLose = Skills.GetXHighestSkills(chr, 2);

                //foreach (Globals.eSkillType skillType in skillsToLose)
                //{
                //    Skills.SkillLoss(chr, skillType, Rules.Formula_SkillsLossAtDeath(Skills.GetSkillLevel(chr.GetSkillExperience(skillType))));
                //} 
                #endregion                

                // 50% chance to drop constitution if greater than 3
                if (chr.Constitution > 3 && Rules.RollD(1, 100) < 50)
                {
                    // 5% chance to drop 2 points of constitution if con will stay at 10 or above
                    if (Rules.RollD(1, 20) == 1 && chr.Constitution >= 12)
                    {
                        chr.Constitution -= 2;
                        chr.WriteToDisplay("You have lost 2 constitution points.");
                    }
                    else
                    {
                        chr.Constitution--;
                        chr.WriteToDisplay("You have lost 1 constitution point.");

                    }
                }

                // 25% chance to drop strength if 18 or above
                if (chr.Strength >= 18 && Rules.RollD(1, 100) < 25)
                {
                    chr.Strength--;
                }

                // character has karma or alignment is not lawful
                if (chr.currentKarma > 0 || chr.Alignment != Globals.eAlignment.Lawful)
                {
                    // character is a thief
                    if (chr.BaseProfession == Character.ClassType.Thief)
                    {
                        if (World.GetFacetByID(chr.FacetID).GetLandByID(chr.LandID).GetMapByID(chr.MapID).ThiefResX != -1)
                        {
                            chr.CurrentCell = Cell.GetCell(chr.FacetID, chr.LandID, chr.MapID, 
                                World.GetFacetByID(chr.FacetID).GetLandByID(chr.LandID).GetMapByID(chr.MapID).ThiefResX, 
                                World.GetFacetByID(chr.FacetID).GetLandByID(chr.LandID).GetMapByID(chr.MapID).ThiefResY, 
                                World.GetFacetByID(chr.FacetID).GetLandByID(chr.LandID).GetMapByID(chr.MapID).ThiefResZ);
                        }
                        else
                        {
                            // default
                            chr.CurrentCell = Cell.GetCell(0, 0, 0, 23, 8, 0);
                        }
                    }
                    else
                    {
                        Utils.Log("Round " + DragonsSpineMain.GameRound + ": Character " + chr.Name + " has karma but is not a thief.", Utils.LogType.Unknown);

                        if (World.GetFacetByID(chr.FacetID).GetLandByID(chr.LandID).GetMapByID(chr.MapID).KarmaResX != -1)
                        {
                            Utils.Log("Round " + DragonsSpineMain.GameRound + ": Character " + chr.Name + " goes to the karmic regen spot.", Utils.LogType.Unknown);
                            chr.CurrentCell = Cell.GetCell(chr.FacetID, chr.LandID, chr.MapID, 
                                World.GetFacetByID(chr.FacetID).GetLandByID(chr.LandID).GetMapByID(chr.CurrentCell.MapID).KarmaResX, 
                                World.GetFacetByID(chr.FacetID).GetLandByID(chr.LandID).GetMapByID(chr.MapID).KarmaResY, 
                                 World.GetFacetByID(chr.FacetID).GetLandByID(chr.LandID).GetMapByID(chr.MapID).KarmaResZ);
                        }
                        else
                        {
                            Utils.Log("Round " + DragonsSpineMain.GameRound + ": Character " + chr.Name + " goes to the underworld.", Utils.LogType.Unknown);
                            Rules.EnterUnderworld(chr);
                            return;
                        }
                    }
                }
                else
                {
                    if (World.GetFacetByID(chr.FacetID).GetLandByID(chr.LandID).GetMapByID(chr.MapID).ResX != -1)
                    {
                        chr.CurrentCell = Cell.GetCell(chr.FacetID, chr.LandID, chr.MapID, 
                            World.GetFacetByID(chr.FacetID).GetLandByID(chr.LandID).GetMapByID(chr.MapID).ResX, 
                            World.GetFacetByID(chr.FacetID).GetLandByID(chr.LandID).GetMapByID(chr.MapID).ResY, 
                             World.GetFacetByID(chr.FacetID).GetLandByID(chr.LandID).GetMapByID(chr.MapID).ResZ);
                    }
                    else
                    {
                        chr.CurrentCell = Cell.GetCell(0, 0, 0, 23, 8, 0);
                    }
                }
                chr.IsDead = false;
                chr.Hits = 1;
                chr.updateAll = true;
                chr.Stamina = 0;
                chr.IsHidden = false;
                chr.IsInvisible = false;
                chr.Stunned = 0;
                chr.Mana = 0;
                return;
            }
            catch (Exception e)
            {
                chr.WriteToDisplay("Error during rest function. Please report this.");
                Utils.LogException(e);
            }
        }

        public static void CorpseRings(Character target, Item corpse)
        {
            if (target.RightRing1 != null) { corpse.addToContainer(target.RightRing1); target.RightRing1 = null; }
            if (target.RightRing2 != null) { corpse.addToContainer(target.RightRing2); target.RightRing2 = null; }
            if (target.RightRing3 != null) { corpse.addToContainer(target.RightRing3); target.RightRing3 = null; }
            if (target.RightRing4 != null) { corpse.addToContainer(target.RightRing4); target.RightRing4 = null; }
            if (target.LeftRing1 != null) { corpse.addToContainer(target.LeftRing1); target.LeftRing1 = null; }
            if (target.LeftRing2 != null) { corpse.addToContainer(target.LeftRing2); target.LeftRing2 = null; }
            if (target.LeftRing3 != null) { corpse.addToContainer(target.LeftRing3); target.LeftRing3 = null; }
            if (target.LeftRing4 != null) { corpse.addToContainer(target.LeftRing4); target.LeftRing4 = null; }
        }

		public static void CorpseSack(Character target, Item corpse)
		{
			if(target.sackList.Count > 0)
			{
				foreach(Item item in new List<Item>(target.sackList))
				{
                    if (item != null)
                    {
                        corpse.addToContainer(item);
                    }
				}
			}
		}

		public static void CorpseBelt(Character target, Item corpse)
		{
			if(target.beltList.Count > 0)
			{
				foreach(Item item in new List<Item>(target.beltList))
				{
                    if (item != null)
                    {
                        corpse.addToContainer(item);
                    }
				}
			}
		}

		public static void CorpseWearing(Character target,Item corpse)
		{
			if(target.wearing.Count > 0)
			{
				foreach(Item item in new List<Item>(target.wearing))
				{
					if(item != null)
					{
						// if armor is in the tanningResult it doesn't get added to the corpse
						if(target.tanningResult == null || Array.IndexOf(target.tanningResult, item.itemID) == -1)
						{
							corpse.addToContainer(item);
						}
					}
				}
			}
		}

        public static void DespawnFigurine(NPC target)
        {
            Item fig = null;

            try
            {
                if (target.special.Contains("snake"))
                    fig = Item.CopyItemFromDictionary(Item.ID_SNAKESTAFF);
                else if (target.special.Contains("tiger"))
                    fig = Item.CopyItemFromDictionary(Item.ID_TIGERFIG);
                else if (target.special.Contains("drake"))
                    fig = Item.CopyItemFromDictionary(Item.ID_DRAKEFIG);
                else if (target.special.Contains("firedragon"))
                    fig = Item.CopyItemFromDictionary(Item.ID_DRAGONFIG);
                else if (target.special.Contains("griffin"))
                    fig = Item.CopyItemFromDictionary(Item.ID_GRIFFINFIG);

                if (fig != null)
                {
                    fig.coinValue = (int)(target.Experience / 2);
                    fig.figExp = target.Experience;
                    target.CurrentCell.Add(fig);
                }
                else
                {
                    target.SendToAllInSight("There was error despawning the figurine. The error has been logged.");
                    Utils.Log("Error while despawning figurine with special tag: " + target.special + ".", Utils.LogType.Unknown);
                }

                target.EmitSound(target.deathSound);

                target.RemoveFromWorld();
            }
            catch (Exception)
            {
                Utils.Log("Error while despawning figurine with special tag: " + target.special + ".", Utils.LogType.Unknown);
                target.RemoveFromWorld();
            }
        }

        public static void UnsummonCreature(NPC target)
        {
            // reset demons
            if (target.species == Globals.eSpecies.Demon && target.IsSummoned)
            {
                string[] coords = target.lairCells.Split("|".ToCharArray());
                target.CurrentCell = Cell.GetCell(target.FacetID, World.LAND_UW, World.MAP_HELL, Convert.ToInt32(coords[0]),
                    Convert.ToInt32(coords[1]), Convert.ToInt32(coords[2]));
                target.Hits = target.HitsMax;
                target.Mana = target.ManaMax;
                target.Stamina = target.StaminaMax;
                target.aiType = NPC.AIType.Unknown;
                target.IsSummoned = false;
                target.Name = target.account; // name was temporarily stored here
                target.Alignment = Globals.eAlignment.Chaotic; // all demons return to chaotic alignment
                return;
            }

            if (World.GetFacetByID(target.FacetID).Spawns.ContainsKey(target.SpawnZoneID))
            {
                World.GetFacetByID(target.FacetID).Spawns[target.SpawnZoneID].NumberInZone -= 1;
            }

            target.RemoveFromWorld();
        }

        public static void DoKarma(Character target, Character killer)
        {
            // killer is not null, killer is a player, and killer did not kill itself
            if (killer != null && killer.IsPC && (killer != target))
            {
                // only regular users can get karma
                if (killer.ImpLevel == Globals.eImpLevel.USER)
                {
                    // target is not a player
                    if (!target.IsPC)
                    {
                        // only give karma if the target is lawful
                        if (target.Alignment == Globals.eAlignment.Lawful)
                        {
                            killer.currentKarma++; // add karma

                            killer.WriteToDisplay("You have received 1 karma for the slaying of " + target.Name + ".");

                            Utils.Log(killer.GetLogString() + " has received 1 karma for killing " + target.GetLogString() + ".", Utils.LogType.Karma);
                            
                            // killer will change alignment if they are lawful
                            if (killer.Alignment == Globals.eAlignment.Lawful)
                            {
                                killer.Alignment = Globals.eAlignment.Neutral;
                                killer.WriteToDisplay("Your alignment has changed to " + killer.Alignment.ToString() +".");
                            }
                            else if (killer.Alignment == Globals.eAlignment.Neutral)
                            {
                                // at 4 karma a neutral player will become evil
                                if (killer.currentKarma >= 4)
                                {
                                    killer.Alignment = Globals.eAlignment.Evil;
                                    killer.WriteToDisplay("Your alignment has changed to " + killer.Alignment.ToString() + ".");
                                }
                            }
                        }
                    }
                    else
                    {
                        // if one of the cells is not pvp enabled
                        if (!killer.CurrentCell.IsPVPEnabled || !target.CurrentCell.IsPVPEnabled)
                        {
                            // if target is not flagged by the killer
                            if (!killer.PlayersFlagged.Contains(target.PlayerID))
                            {
                                if (target.Alignment == Globals.eAlignment.Lawful ||
                                    (target.Alignment == Globals.eAlignment.Neutral && target.BaseProfession == Character.ClassType.Thief))
                                {
                                    killer.currentKarma++;
                                    killer.WriteToDisplay("You have received 1 karma for the slaying of " + target.Name + ".");
                                    Utils.Log(killer.GetLogString() + " has received 1 karma for killing " + target.GetLogString() + ".", Utils.LogType.Karma);
                                }
                                killer.currentMarks++; // add a mark
                                killer.PlayersKilled.Add(target.PlayerID); // add to players killed list
                                killer.WriteToDisplay("Your account has received 1 mark for the slaying of " + target.Name + " without provocation."); // inform player
                                Utils.Log(killer.GetLogString() + " has received 1 mark for an unprovoked killing " + target.GetLogString() + ".", Utils.LogType.Mark);
                                target.WriteToDisplay(killer.Name + " has received 1 mark for killing you unprovoked.");
                                if (killer.currentMarks >= Character.MAX_MARKS)
                                {
                                    killer.WriteToDisplay("You have exceeded the maximum amount of marks allowed. You are being disconnected from the server and will need to contact a GameMaster from the website at www.dragonsspine.com.");
                                    Utils.Log(killer.GetLogString() + " has exceeded the maximum amount of marks allowed and is being disconnected and the account suspended. Target was " + target.GetLogString() + ".", Utils.LogType.Mark);
                                    killer.RemoveFromWorld();
                                    killer.RemoveFromServer();
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void DoDeath(Character target, Character killer)
        {
            if (target == null)
            {
                Utils.Log("Rules.DoDeath() : Target was null.", Utils.LogType.Unknown);
                return;
            }

            target.IsDead = true;

            if (target.CurrentCell == null)
            {
                target.CurrentCell = killer.CurrentCell; // this fixes null cell issues when the target vanishes before leaving a corpse
            }

            if (target.IsPC || (killer != null && killer.IsPC))
            {
                Rules.DoKarma(target, killer);
            }

            #region Emit Death Sound
            if (!target.IsPC)
            {
                if (target.Group != null)
                {
                    target.Group.Remove((NPC)target);
                }

                #region Emit Death Sound
                if (target.deathSound != "")
                {
                    target.EmitSound(target.deathSound);
                }
                else
                {
                    if (target.race != "")
                    {
                        if (target.gender == Globals.eGender.Female)
                        {
                            target.EmitSound(Sound.GetCommonSound(Sound.CommonSound.DeathFemale));
                        }
                        else
                        {
                            target.EmitSound(Sound.GetCommonSound(Sound.CommonSound.DeathMale));
                        }
                    }
                } 
                #endregion
            }
            else
            {
                #region Emit Death Sound
                switch (target.gender)
                {
                    case Globals.eGender.Female:
                        target.EmitSound(Sound.GetCommonSound(Sound.CommonSound.DeathFemalePlayer));
                        break;
                    case Globals.eGender.Male:
                        target.EmitSound(Sound.GetCommonSound(Sound.CommonSound.DeathMalePlayer));
                        break;
                } 
                #endregion

                if (target.IsPeeking)
                {
                    target.effectList[Effect.EffectType.Peek].StopCharacterEffect();
                    return;
                }

                if (target.IsWizardEye)
                {
                    target.effectList[Effect.EffectType.Wizard_Eye].StopCharacterEffect();
                    return;
                }
            } 
            #endregion

            if (target.FollowName != "")
            {
                target.BreakFollowMode();
            }

            // remove from pet owners pet list
            if (target.PetOwner != null)
            {
                target.PetOwner.Pets.Remove(target);
                target.PetOwner = null;
            }

            // clear pets
            target.Pets.Clear();

            if (target.IsPC && target.InUnderworld)
            {
                Rules.UnderworldDeadRest(target);
                return;
            }

            #region Figurines - Despawn Only
            if (target.special.Contains("figurine")) 
             {
                Rules.DespawnFigurine(target as NPC);
                Utils.Log("(Figurine) " + target.GetLogString(), Utils.LogType.DeathCreature);
                return;
            } 
            #endregion

            #region Undead - Make Special Corpse
            if (target.IsUndead)
            {
                Rules.MakeSpecialCorpse((NPC)target);
                Utils.Log("(Undead) " + target.GetLogString(), Utils.LogType.DeathCreature);
                return;
            } 
            #endregion

            #region Demons - Unsummon (reset) and Make Special Corpse
            if (target.species == Globals.eSpecies.Demon)
            {
                Rules.UnsummonCreature((NPC)target);
                Rules.MakeSpecialCorpse((NPC)target);
                Utils.Log("(Demon) " + target.GetLogString(), Utils.LogType.DeathCreature);
                return;
            } 
            #endregion            

            #region Summoned - Unsummon Only
            if (target.IsSummoned)
            {
                Rules.UnsummonCreature((NPC)target);
                Utils.Log("(Summoned) " + target.GetLogString(), Utils.LogType.DeathCreature);
                return;
            } 
            #endregion

            if (!target.IsPC) // if statement to disable player dropping items on death
            {
                if (killer != null && target != killer)
                {
                    #region AttuneType.Slain
                    if (target.RightHand != null && target.RightHand.attuneType == Globals.eAttuneType.Slain)
                    {
                        target.RightHand.AttuneItem(killer);
                    }
                    if (target.LeftHand != null && target.LeftHand.attuneType == Globals.eAttuneType.Slain)
                    {
                        target.LeftHand.AttuneItem(killer);
                    }
                    if (target.RightRing1 != null && target.RightRing1.attuneType == Globals.eAttuneType.Slain)
                    {
                        target.RightRing1.AttuneItem(killer);
                    }
                    if (target.RightRing2 != null && target.RightRing2.attuneType == Globals.eAttuneType.Slain)
                    {
                        target.RightRing2.AttuneItem(killer);
                    }
                    if (target.RightRing3 != null && target.RightRing3.attuneType == Globals.eAttuneType.Slain)
                    {
                        target.RightRing3.AttuneItem(killer);
                    }
                    if (target.RightRing4 != null && target.RightRing4.attuneType == Globals.eAttuneType.Slain)
                    {
                        target.RightRing4.AttuneItem(killer);
                    }
                    if (target.LeftRing1 != null && target.LeftRing1.attuneType == Globals.eAttuneType.Slain)
                    {
                        target.LeftRing1.AttuneItem(killer);
                    }
                    if (target.LeftRing2 != null && target.LeftRing2.attuneType == Globals.eAttuneType.Slain)
                    {
                        target.LeftRing2.AttuneItem(killer);
                    }
                    if (target.LeftRing3 != null && target.LeftRing3.attuneType == Globals.eAttuneType.Slain)
                    {
                        target.LeftRing3.AttuneItem(killer);
                    }
                    if (target.LeftRing4 != null && target.LeftRing4.attuneType == Globals.eAttuneType.Slain)
                    {
                        target.LeftRing4.AttuneItem(killer);
                    }
                    foreach (Item wItem in target.wearing)
                    {
                        if (wItem.attuneType == Globals.eAttuneType.Slain)
                        {
                            wItem.AttuneItem(killer);
                        }
                    }
                    foreach (Item sItem in target.sackList)
                    {
                        if (sItem.attuneType == Globals.eAttuneType.Slain)
                        {
                            sItem.AttuneItem(killer);
                        }
                    }
                    foreach (Item bItem in target.beltList)
                    {
                        if (bItem.attuneType == Globals.eAttuneType.Slain)
                        {
                            bItem.AttuneItem(killer);
                        }
                    } 
                    #endregion
                }
                #region Drop RightHand
                try
                {
                    if (target.RightHand != null)
                    {
                        if (target.RightHand.baseType == Globals.eItemBaseType.Bow)
                        {
                            if (target.RightHand.name.IndexOf("crossbow") == -1)
                            {
                                target.RightHand.nocked = false;
                            }
                        }
                        target.CurrentCell.Add(target.RightHand);
                    }
                }
                catch (Exception e)
                {
                    Utils.Log(target.GetLogString() + "Error on drop RightHand of DoDeath()", Utils.LogType.SystemFailure);
                    Utils.LogException(e);
                }
                #endregion
                if (target.LeftHand != null)
                {
                    if (target.LeftHand.baseType == Globals.eItemBaseType.Bow)
                    {
                        if (target.LeftHand.name.IndexOf("crossbow") == -1)
                        {
                            target.LeftHand.nocked = false;
                        }
                    }
                    target.CurrentCell.Add(target.LeftHand);
                }

                NPC npc = (NPC)target;

                if (npc.lairCritter)
                {
                    if (killer != null)
                    {
                        Utils.Log(target.GetLogString() + " was killed by " + killer.GetLogString(), Utils.LogType.DeathLair); // log this non player death if the menu item is checked
                    }
                    else
                    {
                        Utils.Log(target.GetLogString(), Utils.LogType.DeathLair);
                    }
                }
                else
                {
                    if (killer != null)
                    {
                        Utils.Log(target.GetLogString() + " was killed by " + killer.GetLogString(), Utils.LogType.DeathCreature); // log this non player death if the menu item is checked
                    }
                    else
                    {
                        Utils.Log(target.GetLogString(), Utils.LogType.DeathCreature);
                    }
                }
            }
            else
            {
                target.Deaths++;
                target.preppedSpell = null;
                target.IsInvisible = true;
                target.Stunned = 0;
                target.Hits = 0;


                //Effect[] chEffects = new Effect[target.effectList.Count];
                //target.effectList.Values.CopyTo(chEffects, 0);
                foreach(Effect chEffects in new List<Effect>(target.effectList.Values))
                //for (int count = chEffects.Length - 1; count >= 0; count--)
                {
                    //chEffects[count].StopCharacterEffect();
                    chEffects.StopCharacterEffect();
                }

                if (killer != null)
                {
                    Utils.Log(target.GetLogString() + " was killed by " + killer.GetLogString() + " # of Attackers: " + target.numAttackers, Utils.LogType.DeathPlayer);
                }
                else
                {
                    Utils.Log(target.GetLogString(), Utils.LogType.DeathPlayer);
                }
            }
            Rules.MakeCorpse(target);
        }

		public static void EnterUnderworld(Character chr)
		{
            Utils.Log(chr.Name + " is attempting to enter the Underworld.", Utils.LogType.DeathPlayer);

            if (System.Configuration.ConfigurationManager.AppSettings["UnderworldEnabled"].ToLower() == "false")
            {
                chr.WriteToDisplay("This feature is currently disabled.");
                return;
            }

			try
			{
                if (!chr.IsDead) { chr.SendShout("a thunderclap!"); }
				chr.WriteToDisplay("The world dissolves around you.");
                Utils.Log(chr.Name + " is attempting to enter the Underworld 2.", Utils.LogType.DeathPlayer);

				// clear any warmed spells
				if(chr.preppedSpell != null){chr.preppedSpell = null;}

				// drop held items to ground
				if(chr.RightHand != null){chr.CurrentCell.Add(chr.RightHand);chr.UnEquipRightHand(chr.RightHand);}
				if(chr.LeftHand != null){chr.CurrentCell.Add(chr.LeftHand);chr.UnEquipLeftHand(chr.LeftHand);}

                Utils.Log(chr.Name + " is attempting to enter the Underworld 3.", Utils.LogType.DeathPlayer);
                if (chr.wearing.Count > 0) // drop worn items to ground
				{
					foreach(Item wItem in chr.wearing)
					{
						chr.CurrentCell.Add(wItem);
					}
					chr.wearing.Clear();
				}
				// drop belt items to ground
				if(chr.beltList.Count > 0)
				{
					foreach(Item bItem in chr.beltList)
					{
						chr.CurrentCell.Add(bItem);
					}
					chr.beltList.Clear();
				}
				// drop sack contents to ground
				if(chr.sackList.Count > 0)
				{
					foreach(Item sItem in chr.sackList)
					{
						chr.CurrentCell.Add(sItem);
					}
					chr.sackList.Clear();
				}
				//drop rings to ground
				if(chr.RightRing1!=null){chr.CurrentCell.Add(chr.RightRing1);chr.RightRing1=null;}
				if(chr.RightRing2!=null){chr.CurrentCell.Add(chr.RightRing2);chr.RightRing2=null;}
				if(chr.RightRing3!=null){chr.CurrentCell.Add(chr.RightRing3);chr.RightRing3=null;}
				if(chr.RightRing4!=null){chr.CurrentCell.Add(chr.RightRing4);chr.RightRing4=null;}
				if(chr.LeftRing1!=null){chr.CurrentCell.Add(chr.LeftRing1);chr.LeftRing1=null;}
				if(chr.LeftRing2!=null){chr.CurrentCell.Add(chr.LeftRing2);chr.LeftRing2=null;}
				if(chr.LeftRing3!=null){chr.CurrentCell.Add(chr.LeftRing3);chr.LeftRing3=null;}
				if(chr.LeftRing4!=null){chr.CurrentCell.Add(chr.LeftRing4);chr.LeftRing4=null;}
				//remove effects
				chr.effectList.Clear();
				//chr.Poisoned = 0;
				chr.knightRing = false;
                Utils.Log(chr.Name + " is attempting to enter the Underworld 4.", Utils.LogType.DeathPlayer);

				// remove character from current location and place at Underworld entrance
				chr.CurrentCell = Cell.GetCell(chr.FacetID, Globals.UNDERWORLD_LANDID, Globals.PRAETOSEBA_MAPID, 26, 4, 0);

                Utils.Log(chr.Name + " is attempting to enter the Underworld 5.", Utils.LogType.DeathPlayer);

				// record current stats and set Underworld stats
				chr.UW_hitsMax = chr.HitsMax;
                chr.UW_hitsAdjustment = chr.HitsAdjustment;
				chr.HitsMax = 25;
                chr.HitsAdjustment = 0;
				chr.Hits = 25;
				chr.UW_manaMax = chr.ManaMax;
                chr.UW_manaAdjustment = chr.ManaAdjustment;
				chr.ManaMax = 0;
                chr.ManaAdjustment = 0;
				chr.Mana = 0;
				chr.UW_staminaMax = chr.StaminaMax;
                chr.UW_staminaAdjustment = chr.StaminaAdjustment;
				chr.StaminaMax = 10;
				chr.Stamina = 10;
                chr.StaminaAdjustment = 0;
			}
			catch(Exception e)
			{
                Utils.LogException(e);
			}
		}

        public static void ReturnFromUnderworld(Character chr)
		{
			chr.SendShout("a thunderclap!");
			chr.WriteToDisplay("The world dissolves around you.");
			//break hide on a portal
			if(chr.IsHidden){chr.IsHidden = false;}
			//clear any warmed spells
			if(chr.preppedSpell != null){chr.preppedSpell = null;}
			//drop held items to ground
			if(chr.RightHand != null){chr.CurrentCell.Add(chr.RightHand);chr.UnEquipRightHand(chr.RightHand);}
			if(chr.LeftHand != null){chr.CurrentCell.Add(chr.LeftHand);chr.UnEquipLeftHand(chr.LeftHand);}
			//drop worn items to ground
			if(chr.wearing.Count > 0)
			{
				foreach(Item wItem in chr.wearing)
				{
					chr.CurrentCell.Add(wItem);
				}
				chr.wearing.Clear();
			}
			//drop belt items to ground
			if(chr.beltList.Count > 0)
			{
				foreach(Item bItem in chr.beltList)
				{
					chr.CurrentCell.Add(bItem);
				}
				chr.beltList.Clear();
			}
			//drop sack contents to ground
			if(chr.sackList.Count > 0)
			{
				foreach(Item sItem in chr.sackList)
				{
					chr.CurrentCell.Add(sItem);
				}
				chr.sackList.Clear();
			}
			//drop rings to ground
			if(chr.RightRing1!=null){chr.CurrentCell.Add(chr.RightRing1);chr.RightRing1=null;}
			if(chr.RightRing2!=null){chr.CurrentCell.Add(chr.RightRing2);chr.RightRing2=null;}
			if(chr.RightRing3!=null){chr.CurrentCell.Add(chr.RightRing3);chr.RightRing3=null;}
			if(chr.RightRing4!=null){chr.CurrentCell.Add(chr.RightRing4);chr.RightRing4=null;}
			if(chr.LeftRing1!=null){chr.CurrentCell.Add(chr.LeftRing1);chr.LeftRing1=null;}
			if(chr.LeftRing2!=null){chr.CurrentCell.Add(chr.LeftRing2);chr.LeftRing2=null;}
			if(chr.LeftRing3!=null){chr.CurrentCell.Add(chr.LeftRing3);chr.LeftRing3=null;}
			if(chr.LeftRing4!=null){chr.CurrentCell.Add(chr.LeftRing4);chr.LeftRing4=null;}
			//remove effects
			chr.effectList.Clear();
			chr.knightRing = false;
			chr.CurrentCell = Cell.GetCell(chr.FacetID, 0, 0, 39, 7, 0);
			chr.HitsMax = chr.UW_hitsMax;
            chr.HitsAdjustment = chr.UW_hitsAdjustment;
            chr.Hits = chr.HitsFull;
			chr.ManaMax = chr.UW_manaMax;
            chr.ManaAdjustment = chr.UW_manaAdjustment;
            chr.Mana = chr.ManaFull;
			chr.StaminaMax = chr.UW_staminaMax;
            chr.StaminaAdjustment = chr.UW_staminaAdjustment;
            chr.Stamina = chr.StaminaFull;
			chr.Constitution += 8;
            if (chr.Constitution > chr.Land.MaxAbilityScore)
                chr.Constitution = chr.Land.MaxAbilityScore;
		}

        public static void OneWayPortal(Character chr, Land land)
        {
            chr.lockerList.Clear();
            chr.bankGold = 0;
            PC pc = (PC)chr;
            pc.Save();
        }

        public static bool CheckPerception(Character chr)
        {
            if (chr.Intelligence <= 5 || chr.Wisdom <= 5)
                return false;

            if (RollD(2, 20) < chr.Intelligence + chr.Wisdom)
            {
                return true;
            }
            return false;
        }

        public static int CalculateKillsPerHour(Character chr)
        {
            if (chr.RoundsPlayed < 1)
                return chr.Kills;

            long kills = chr.Kills;
            long roundsPlayed = chr.RoundsPlayed;

            if (kills < 1)
                kills = 1;

            if (roundsPlayed < 720)
                roundsPlayed = 720;

            return (int)(kills / (roundsPlayed / 720));
        }
	}
}

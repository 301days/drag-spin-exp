using System;

namespace DragonsSpine.Commands
{
    public interface ICommandHandler
    {
        /// <summary>
        /// Called when a command should be executed.
        /// </summary>
        /// <param name="client">The GameObject executing the command.</param>
        /// <param name="args">Command arguments.</param>
        /// <returns>True if successful.</returns>
        bool OnCommand(GameObjects.GameObject obj, string[] args);
    }
}

using System;
using System.Data;
using System.Data.SqlClient;

namespace DragonsSpine.DAL
{
	/// <summary>
	/// Database routines for logging.
	/// </summary>
	public class DBLog
	{
		public DBLog()
		{

		}

        // ***********************
        // LOG ROUTINES
        // ***********************
        internal static int addLogEntry(string message, string logType)
        {
            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Log_Add", DataAccess.GetSQLConnection());
                sp.AddParameter("@logtype", SqlDbType.NVarChar, 50, ParameterDirection.Input, logType);
                sp.AddParameter("@message", SqlDbType.NVarChar, 250, ParameterDirection.Input, message);

                int rc = sp.ExecuteNonQuery();
                // Console.WriteLine("addLogEntry rows affected: " + rc + ".");

                return rc;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                Console.WriteLine("addLogEntry exception " + e.ToString() + ".");
                return -1;
            }
        }
	}
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DragonsSpine
{
    /// <summary>
    /// Static class world manager
    /// </summary>

    public static class World
    {
        #region Enums
        
        public enum DailyCycle
        {
            Morning,
            Afternoon,
            Evening,
            Night
        }
        public enum LunarCycle
        {
            New,
            Waxing_Crescent,
            Waning_Crescent,
            Full
        } 
        #endregion

        #region Constants
        public const short MAP_HELL = 12;
        public const short LAND_BG = 0;
        public const short LAND_AG = 1;
        public const short LAND_UW = 2; 
        #endregion

        #region Private Data
        private static long[] m_lottery; // each land has a lottery for the coins that were picked up by the janitor
        private static int m_worldNpcID = -2147483641;
        private static int m_worldItemID = -2147483641;
        private static Dictionary<short, Facet> m_facetDict = new Dictionary<short, Facet>();
        private static string m_currentDay = "Nanna";
        private static DailyCycle m_currentDailyCycle = DailyCycle.Morning;
        private static LunarCycle m_currentLunarCycle = LunarCycle.New;
        private static ArrayList m_bannedIPList = new ArrayList();
        private static int m_itemDecayTimer = 240; // 30 minutes for items
        private static int m_playerCorpseDecayTimer = 240; // 30 minutes for a player corpses
        private static int m_npcCorpseDecayTimer = 48; // 4 minutes for npc corpses
        private static int m_attunedItemDecayTimer = 4320; // 6 hours for attuned items
        #endregion

        public static Character.ClassType[] pureMelee = new Character.ClassType[] { Character.ClassType.Fighter, Character.ClassType.Martial_Artist };
        public static Character.ClassType[] hybrids = new Character.ClassType[] { Character.ClassType.Knight };
        public static Character.ClassType[] spellUsers = new Character.ClassType[] { Character.ClassType.Thaumaturge, Character.ClassType.Wizard,
            Character.ClassType.Thief, Character.ClassType.Knight };
        public static Character.ClassType[] intelligenceCasters = new Character.ClassType[] { Character.ClassType.Wizard, Character.ClassType.Thief };
        public static Character.ClassType[] wisdomCasters = new Character.ClassType[] { Character.ClassType.Thaumaturge, Character.ClassType.Knight };

        public static Globals.eSpecies[] wyrmkin = new Globals.eSpecies[] { Globals.eSpecies.FireDragon, Globals.eSpecies.IceDragon, Globals.eSpecies.LightningDrake };
        public static Globals.eSpecies[] hardhitter = new Globals.eSpecies[] { Globals.eSpecies.Thisson, Globals.eSpecies.Ydmos };
        public static Globals.eSpecies[] magicsniffer = new Globals.eSpecies[] { Globals.eSpecies.Thisson };

        public static int[] DoctoredHPLimits = { 0, 425, 350, 300, 375, 325, 400 };
        public static string[] DaysOfTheWeek = { "Nanna", "Enki", "Inanna", "Utu", "Gugalanna", "Enlil", "Ninurta" }; // days of the week
        public static string[] Seasons = { "Emesh", "Enten" };
        public static int[] AgeCycles = { 14400, 28800, 43200, 57600, 72000, 80000 };
        public static string[] age_humanoid = new string[] { "a very young", "a young", "a middle-aged", "an old", "a very old", "an ancient" };
        public static string[] age_wyrmKin = new string[] { "a hatchling", "a baby", "an adolescent", "a mature", "an aged", "an ancient" };
        public static string[] homelands = new string[] { "Illyria", "Mu", "Lemuria", "Leng", "Draznia", "Hovath", "Mnar", "the plains" };

        public static List<string> magicCordThisRound = new List<string>(); // <facet>|<land>|<map>|<ycord>|<xcord>|<zcord>
        public static List<string> magicCordLastRound = new List<string>(); // <facet>|<land>|<map>|<ycord>|<xcord>|<zcord>

        #region Properties
        public static int ItemDecayTimer
        {
            get { return m_itemDecayTimer; }
        }
        public static int PlayerCorpseDecayTimer
        {
            get { return m_playerCorpseDecayTimer; }
        }
        public static int NPCCorpseDecayTimer
        {
            get { return m_npcCorpseDecayTimer; }
        }
        public static int AttunedItemDecayTimer
        {
            get { return m_attunedItemDecayTimer; }
        }
        public static ArrayList BannedIPList
        {
            get { return m_bannedIPList; }
            set { m_bannedIPList = value; }
        }

        public static long[] Lottery
        {
            get { return m_lottery; }
            set { m_lottery = value; }
        }

        public static Dictionary<short, Facet>.ValueCollection Facets
        {
            get { return World.m_facetDict.Values; }
        }

        public static DailyCycle CurrentDailyCycle
        {
            get { return m_currentDailyCycle; }
            set { m_currentDailyCycle = value; }
        }

        public static LunarCycle CurrentLunarCycle
        {
            get { return m_currentLunarCycle; }
            set { m_currentLunarCycle = value; }
        } 
        #endregion

        public static bool LoadWorld()
        {
            if (!World.LoadFacets())
            {
                return false;
            }
            else
            {
                foreach (Facet facet in World.Facets)
                {
                    if (!facet.LoadLands())
                    {
                        return false;
                    }
                    else
                    {
                        foreach (Land land in facet.Lands)
                        {
                            if (!land.FillLand())
                            {
                                Utils.Log("Fatal error while filling Facet: " + facet.Name + " Land: " + land.Name + " with maps.", Utils.LogType.SystemFatalError);
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }

        private static bool LoadFacets()
        {
            return DAL.DBWorld.LoadFacets();
        }

        public static bool LoadBannedIPList() // helper function for filling banned IP list
        {
            try
            {
                BannedIPList = DAL.DBWorld.loadBannedIPList();
                return true;
            }
            catch (Exception e)
            {
                Utils.Log("World.loadBannedIPList() " + e.Message + "  Stack: " + e.StackTrace, Utils.LogType.Exception);
                return false;
            }
        }

        public static void Add(Facet facet)
        {
            World.m_facetDict.Add(facet.FacetID, facet);
            //Utils.Log("Added " + facet.Name + " Facet.", Utils.LogType.SystemGo);
        }

        public static int GetNumberPlayersInLand(short landID)
        {
            int number = 0;
            foreach (Character ch in new List<Character>(Character.pcList))
            {
                if (!ch.IsPC) continue;
                if (ch.LandID == landID)
                {
                    number++;
                }
            }
            return number;
        }
        public static int GetNumberPlayersInMap(short mapID)
        {
            int number = 0;
            foreach (Character ch in new List<Character>(Character.pcList))
            {
                if (!ch.IsPC) continue;
                if (ch.MapID == mapID)
                {
                    number++;
                }
            }
            return number;
        }
        public static Facet GetFacetByIndex(int index)
        {
            int a = 0;
            foreach (Facet facet in World.Facets)
            {
                if (a == index)
                {
                    return facet;
                }
                a++;
            }
            return null;
        }

        public static Facet GetFacetByID(short facetID)
        {
            if (World.m_facetDict.ContainsKey(facetID))
            {
                return World.m_facetDict[facetID];
            }
            return null;
        }

        public static int GetNextWorldNpcID()
        {
            return World.m_worldNpcID++;
        }

        public static int GetNextWorldItemID()
        {
            return m_worldItemID++;
        }

        public static void ShiftDay()
        {
            for (int a = 0; a < DaysOfTheWeek.Length; a++)
            {
                if (DaysOfTheWeek[a] == m_currentDay)
                {
                    if (a + 1 >= DaysOfTheWeek.Length - 1)
                    {
                        m_currentDay = DaysOfTheWeek[0];
                    }
                    else
                    {
                        m_currentDay = DaysOfTheWeek[a + 1];
                    }
                    break;
                }
            }
  
            // resupply the berry bushes
            foreach (Facet facet in World.Facets)
            {
                foreach (Land land in facet.Lands)
                {
                    foreach (Map map in land.Maps)
                    {
                        if (map != null)
                        {
                            foreach (Cell cell in map.cells.Values)
                            {
                                if (cell != null)
                                {
                                    if (cell.balmBerry || cell.manaBerry || cell.poisonBerry || cell.stamBerry)
                                    {
                                        cell.droppedBerry = 0;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void ShiftDailyCycle(Object myObject, System.Timers.ElapsedEventArgs myEventArgs)
        {
            if (m_currentDailyCycle == (DailyCycle)Enum.GetValues(typeof(DailyCycle)).GetValue(Enum.GetValues(typeof(DailyCycle)).Length - 1))
            {
                m_currentDailyCycle = (DailyCycle)Enum.GetValues(typeof(DailyCycle)).GetValue(0);
                ShiftDay();
            }
            else
            {
                m_currentDailyCycle = (DailyCycle)World.m_currentDailyCycle + 1;
            }
        }

        public static void ShiftLunarCycle(Object myObject, System.Timers.ElapsedEventArgs myEventArgs)
        {
            if (m_currentLunarCycle == (LunarCycle)Enum.GetValues(typeof(LunarCycle)).GetValue(Enum.GetValues(typeof(LunarCycle)).Length - 1))
            {
                m_currentLunarCycle = (LunarCycle)Enum.GetValues(typeof(LunarCycle)).GetValue(0);
                // nothing currently happens when the cycle starts over, should there be an event?
            }
            else
            {
                m_currentLunarCycle = (LunarCycle)World.m_currentLunarCycle + 1;
            }
        }

        public static List<string> GetScoresList(string classAbbreviation, short amount, bool devRequest, bool pvp)
        {
            string classFullName = "";
            string playerName = "";
            Character.ClassType classType = Character.ClassType.None;
            List<string> list = new List<string>();

            switch (classAbbreviation.ToLower())
            {
                case "all":
                    classType = Character.ClassType.None;
                    classFullName = "All Classes";
                    break;
                case "k":
                case "ki":
                case "kn":
                case "knight":
                    classType = Character.ClassType.Knight;
                    classFullName = "Knights";
                    break;
                case "f":
                case "fi":
                case "fighter":
                    classType = Character.ClassType.Fighter;
                    classFullName = "Fighters";
                    break;
                case "m":
                case "ma":
                case "martial":
                case "martialartist":
                    classType = Character.ClassType.Martial_Artist;
                    classFullName = "Martial Artists";
                    break;
                case "t":
                case "th":
                case "thaum":
                case "thaumaturge":
                    classType = Character.ClassType.Thaumaturge;
                    classFullName = "Thaumaturges";
                    break;
                case "w":
                case "wi":
                case "wiz":
                case "wizard":
                case "mage":
                    classType = Character.ClassType.Wizard;
                    classFullName = "Wizards";
                    break;
                case "r":
                case "tf":
                case "thief":
                    classType = Character.ClassType.Thief;
                    classFullName = "Thieves";
                    break;
                default:
                    if (PC.PlayerExists(classAbbreviation))
                        playerName = classAbbreviation;
                    break;
            }

            if (playerName == "")
                list.Add("Top " + amount + " " + classFullName + " of " + DragonsSpineMain.APP_NAME);

            list.Add("   Rank Name            Class          Level Experience  KPH  Last Online");

            string anonymous = "";

            try
            {
                ArrayList scores = DAL.DBWorld.GetScores(classType, amount, devRequest, playerName, pvp);

                if (scores.Count < 1)
                {
                    if (playerName != "")
                    {
                        list.Clear();
                        list.Add("No player named \"" + classAbbreviation + "\" found in the scores list.");
                        return list;
                    }

                    list.Clear();
                    list.Add("No scores found that match your search parameters.");
                    return list;
                }

                foreach (PC score in scores)
                {
                    if (devRequest && score.IsAnonymous)
                        anonymous = " [ANON]";
                    else if (devRequest && score.ImpLevel > Globals.eImpLevel.USER)
                    {
                        score.showStaffTitle = true;
                        anonymous = " " + Conference.GetStaffTitle(score);
                    }
                    else
                        anonymous = "";

                    list.Add(score.PlayerID.ToString().PadLeft(6, ' ') +
                        ". " + score.Name.PadRight(16, ' ') +
                        Utils.FormatEnumString(score.BaseProfession.ToString()).PadRight(15, ' ') +
                        score.Level.ToString().PadLeft(5, ' ') +
                        score.Experience.ToString().PadLeft(11, ' ') +
                        Rules.CalculateKillsPerHour(score).ToString().PadLeft(5, ' ') +
                        "  " + score.lastOnline.ToShortDateString().PadLeft(11, ' ') +
                        "  " + anonymous);
                }
                return list;
            }
            catch (Exception e)
            {
                Utils.Log("World.getScoresList("+classType+", "+amount+", "+Convert.ToString(devRequest)+", "+Convert.ToString(pvp)+") " + e.Message + " Stack: " + e.StackTrace, Utils.LogType.Exception);
                return null;
            }
        } 

        public static bool magicWithinRange(NPC npc)
        {
            foreach (String cord in new List<string>(World.magicCordLastRound))
            {
                string [] flmxyz = cord.Split("|".ToCharArray());
                int rf = Convert.ToInt32(flmxyz[0]);
                int rl = Convert.ToInt32(flmxyz[1]);
                int rm = Convert.ToInt32(flmxyz[2]);
                int rx = Convert.ToInt32(flmxyz[3]);
                int ry = Convert.ToInt32(flmxyz[4]);
                int rz = Convert.ToInt32(flmxyz[5]);
                if (rf == npc.FacetID && rl == npc.LandID && rm == npc.MapID && rz == npc.Z
                    && Cell.GetCellDistance(rx, ry, npc.X, npc.Y) <= 15)
                {
                    npc.gotoWarmedMagic = rx + "|" + ry + "|" + rz;
                    return true;
                }
            }
            npc.gotoWarmedMagic = "";
            return false;
        }

        public static void ClearLiveCellData() {
            DAL.DBWorld.ClearLiveCellData();
        }

    }
}

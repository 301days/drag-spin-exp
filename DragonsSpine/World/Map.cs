using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using GameLib.Mathematics.TwoD;

namespace DragonsSpine
{
	public class Map
	{
        public enum ClimateType { None, Temperate, Subtropical, Tropical, Desert, Tundra, Frozen, Subterranean }
        public enum Direction { None, North, South, East, West, Northeast, Southeast, Northwest, Southwest }

        #region Private Data
        short facetID;
        short landID;
        short mapID;
        string name;
        string shortDesc;
        string longDesc;
        int suggestedMaximumLevel;
        int suggestedMinimumLevel;
        bool pvpEnabled;
        double experienceModifier;
        short difficulty;
        ClimateType climate; // map climate
        bool balmBushes = false;
        bool manaBushes = false;
        bool poisonBushes = false;
        bool staminaBushes = false;
        short resX = -1; // normal respawn xcord
        short resY = -1; // normal respawn ycord
        int resZ = -1; // normal respawn zcord
        short thiefResX = -1; // thief respawn xcord
        short thiefResY = -1; // thief respawn ycord
        int thiefResZ = -1; // thief respawn zcord
        short karmaResX = -1; // karma respawn xcord
        short karmaResY = -1; // karma respawn ycord
        int karmaResZ = -1; // karma respawn zcord
        Dictionary<int, int> xcordMax = new Dictionary<int, int>(); // key = zPlane, value = x max
        Dictionary<int, int> xcordMin = new Dictionary<int, int>(); // key = zPlane, value = x min
        Dictionary<int, int> ycordMax = new Dictionary<int, int>(); // key = zPlane, value = y max
        Dictionary<int, int> ycordMin = new Dictionary<int, int>(); // key = zPlane, value = y min
        List<int> zPlanes = new List<int>();
        Dictionary<int, string> zNames = new Dictionary<int, string>();
        bool randomMagicIntensity = false;
        #endregion

        #region Public Properties
        public short FacetID
        {
            get { return this.facetID; }
        }
        public short LandID
        {
            get { return this.landID; }
        }
        public short MapID
        {
            get { return this.mapID; }
        }
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public string ShortDesc
        {
            get { return this.shortDesc; }
            set { this.shortDesc = value; }
        }
        public string LongDesc
        {
            get { return this.longDesc; }
            set { this.longDesc = value; }
        }
        public int SuggestedMaximumLevel
        {
            get { return this.suggestedMaximumLevel; }
            set { this.suggestedMaximumLevel = value; }
        }
        public int SuggestedMinimumLevel
        {
            get { return this.suggestedMinimumLevel; }
            set { this.suggestedMinimumLevel = value; }
        }
        public bool IsPVPEnabled
        {
            get { return this.pvpEnabled; }
            set { this.pvpEnabled = value; }
        }
        public double ExperienceModifier
        {
            get { return this.experienceModifier; }
            set { this.experienceModifier = value; }
        }
        public short Difficulty
        {
            get { return this.difficulty; }
            set { this.difficulty = value; }
        }
        public ClimateType Climate
        {
            get { return this.climate; }
            set { this.climate = value; }
        }
        public bool HasBalmBushes
        {
            get { return this.balmBushes; }
            set { this.balmBushes = value; }
        }
        public bool HasManaBushes
        {
            get { return this.manaBushes; }
            set { this.manaBushes = value; }
        }
        public bool HasPoisonBushes
        {
            get { return this.poisonBushes; }
            set { this.poisonBushes = value; }
        }
        public bool HasStaminaBushes
        {
            get { return this.staminaBushes; }
            set { this.staminaBushes = value; }
        }
        public short ResX
        {
            get { return this.resX; }
            set { this.resX = value; }
        }
        public short ResY
        {
            get { return this.resY; }
            set { this.resY = value; }
        }
        public int ResZ
        {
            get { return this.resZ; }
            set { this.resZ = value; }
        }
        public short ThiefResX
        {
            get { return this.thiefResX; }
            set { this.thiefResX = value; }
        }
        public short ThiefResY
        {
            get { return this.thiefResY; }
            set { this.thiefResY = value; }
        }
        public int ThiefResZ
        {
            get { return this.thiefResZ; }
            set { this.thiefResZ = value; }
        }
        public short KarmaResX
        {
            get { return this.karmaResX; }
            set { this.karmaResX = value; }
        }
        public short KarmaResY
        {
            get { return this.karmaResY; }
            set { this.karmaResY = value; }
        }
        public int KarmaResZ
        {
            get { return this.karmaResZ; }
            set { this.karmaResZ = value; }
        }
        public Dictionary<int, int> XCordMax
        {
            get { return this.xcordMax; }
        }
        public Dictionary<int, int> XCordMin
        {
            get { return this.xcordMin; }
        }
        public Dictionary<int, int> YCordMax
        {
            get { return this.ycordMax; }
        }
        public Dictionary<int, int> YCordMin
        {
            get { return this.ycordMin; }
        }
        public List<int> ZPlanes
        {
            get { return this.zPlanes; }
        }
        public bool HasRandomMagicIntensity
        {
            get { return this.randomMagicIntensity; }
        }
        #endregion

        public Dictionary<string, Cell> cells = new Dictionary<string, Cell>();

        #region Constructors (2)
        public Map()
        {

        }

        public Map(short facetID, System.Data.DataRow dr)
        {
            this.facetID = facetID;
            this.landID = Convert.ToInt16(dr["landID"]);
            this.mapID = Convert.ToInt16(dr["mapID"]);
            this.Name = dr["name"].ToString();
            this.ShortDesc = dr["shortDesc"].ToString();
            this.LongDesc = dr["longDesc"].ToString();
            this.SuggestedMaximumLevel = Convert.ToInt32(dr["suggestedMaximumLevel"]);
            this.SuggestedMinimumLevel = Convert.ToInt32(dr["suggestedMinimumLevel"]);
            this.IsPVPEnabled = Convert.ToBoolean(dr["pvpEnabled"]);
            this.ExperienceModifier = Convert.ToDouble(dr["expModifier"]);
            this.Difficulty = Convert.ToInt16(dr["difficulty"]);
            this.Climate = (Map.ClimateType)Convert.ToInt16(dr["climateType"]);
            this.HasBalmBushes = Convert.ToBoolean(dr["balmBushes"]);
            this.HasPoisonBushes = Convert.ToBoolean(dr["poisonBushes"]);
            this.HasManaBushes = Convert.ToBoolean(dr["manaBushes"]);
            this.HasStaminaBushes = Convert.ToBoolean(dr["staminaBushes"]);
            this.ResX = Convert.ToInt16(dr["resX"]);
            this.ResY = Convert.ToInt16(dr["resY"]);
            this.ResZ = Convert.ToInt16(dr["resZ"]);
            this.ThiefResX = Convert.ToInt16(dr["thiefResX"]);
            this.ThiefResY = Convert.ToInt16(dr["thiefResY"]);
            this.ThiefResZ = Convert.ToInt32(dr["thiefResZ"]);
            this.KarmaResX = Convert.ToInt16(dr["karmaResX"]);
            this.KarmaResY = Convert.ToInt16(dr["karmaResY"]);
            this.KarmaResZ = Convert.ToInt32(dr["karmaResZ"]);
            this.randomMagicIntensity = Convert.ToBoolean(dr["randomMagicIntensity"]);
        } 
        #endregion

        #region ANSI
        public const string CLS = "\x1b[2J\x1b[1;1f";
        public const string CLRLN = "\x1b[K";
        public const string CEND = "\x1b[0m";
        public const string SAVECUR = "\x1b[s";
        public const string LOADCUR = "\x1b[u";

        public const string CGRY = "\x1b[37m";
        public const string CNRM = "\x1B[0;0m";
        public const string CRED = "\x1B[31m";
        public const string CGRN = "\x1B[32m";
        public const string CYEL = "\x1B[33m";
        public const string CBLU = "\x1B[34m";
        public const string CMAG = "\x1B[35m";
        public const string CCYN = "\x1B[36m";
        public const string CWHT = "\x1B[37m";
        public const string CBLK = "\x1B[30m";

        /*              Bold Colors            */
        public const string BGRY = "\x1b[1;30m";
        public const string BRED = "\x1B[1;31m";
        public const string BGRN = "\x1B[1;32m";
        public const string BYEL = "\x1B[1;33m";
        public const string BBLU = "\x1B[1;34m";
        public const string BMAG = "\x1B[1;35m";
        public const string BCYN = "\x1B[1;36m";
        public const string BWHT = "\x1B[1;37m";
        public const string BBLK = "\x1B[1;30m";

        /*             Backgrounds             */
        public const string BKRED = "\x1B[41m";
        public const string BKGRN = "\x1B[42m";
        public const string BKYEL = "\x1B[43m";
        public const string BKBLU = "\x1B[44m";
        public const string BKMAG = "\x1B[45m";
        public const string BKCYN = "\x1B[46m";
        public const string BKWHT = "\x1B[47m";
        public const string BKBLK = "\x1B[40m"; 
        #endregion

        #region Kesmai Protocol
        #region Tiles
        public const char ESC = '\x1B';
        public const char KPDARKNESS = (char)32;
        public const char KPEMPTY = (char)33;
        public const char KPROOM = (char)34;
        public const char KPAIR = (char)35;
        public const char KPWEB = (char)36;
        public const char KPHCDOOR = (char)37;
        public const char KPVCDOOR = (char)38;
        public const char KPHODOOR = (char)39;
        public const char KPVODOOR = (char)40;
        public const char KPFIRE = (char)41;
        public const char KPICE = (char)42;
        public const char KPRUIN1 = (char)43;
        public const char KPRUIN2 = (char)44;
        public const char KPSTAIRUP = (char)45;
        public const char KPSTAIRDN = (char)46;
        public const char KPREEF = (char)47;
        public const char KPWATER = (char)48;
        public const char KPTREE1 = (char)49;
        public const char KPTREE2 = (char)50;
        public const char KPFOREST1 = (char)51;
        public const char KPFOREST2 = (char)52;
        public const char KPDOCK = (char)53;
        public const char KPBRIDGE = (char)54;
        public const char KPIMPTREE = (char)55;
        public const char KPBURNT1 = (char)56;
        public const char KPBURNT2 = (char)57;
        public const char KPPATH = (char)58;
        public const char KPMOUNTAIN = (char)59;
        public const char KPIMPMOUNT = (char)60;
        public const char KPALTAR = (char)61;
        public const char KPSHOPCOUNTER = (char)62;
        public const char KPPIT = (char)63;
        public const char KPGRATE = (char)64;
        public const char KPRING = (char)65;
        public const char KPTRASH = (char)66;
        public const char KPGRASS = (char)67;
        public const char KPSAND = (char)68;
        public const char KPTILE = (char)69;
        public const char KPWALL = (char)70;
        public const char KPGRAVE = (char)71;
        public const char KPIMPDARK = (char)73;
        public const char KPFIELD = (char)82;
        public const char KPLIGHT = (char)83;
        public const char KPLINECOUNTER = (char)84;
        public const char KPNOVIS = (char)85;
        public const char KPEND = (char)127;
        #endregion
        #region KP Strings
        /// <summary>
        /// Cursor Up - (L 1)
        /// </summary>
        public const string KP_MOVE_CURS_UP = "\x1B" + "A";
        /// <summary>
        /// Cursor Down - (L 1)
        /// </summary>
        public const string KP_MOVE_CURS_DN = "\x1B" + "B";
        /// <summary>
        /// Cursor Right - (L 1)
        /// </summary>
        public const string KP_MOVE_CURS_RT = "\x1B" + "C";
        /// <summary>
        /// Cursor Left - (L 1)
        /// </summary>
        public const string KP_MOVE_CURS_LT = "\x1B" + "D";
        /// <summary>
        /// Enter Enhancer Mode - (L 1)
        /// </summary>
        public const string KP_ENHANCER_ENABLE = "\x1B" + "E";
        /// <summary>
        /// Exit Enhancer Mode - (L 1) 
        /// </summary>
        public const string KP_ENHANCER_DISABLE = "\x1B" + "G";
        /// <summary>
        /// Move the cursor to the home position (1,1). - (L 1)
        /// </summary>
        public const string KP_MOVE_CURS_HOME = "\x1B" + "H";
        /// <summary>
        /// Reverse Line Feed - (L 1)
        /// </summary>
        public const string KP_REV_LINE_FEED = "\x1B" + "I";
        /// <summary>
        /// Erase to End of Screen - (L 1)
        /// </summary>
        public const string KP_ERASE_TO_END = "\x1B" + "J";
        /// <summary>
        /// Erase to end of current line - (L 1)
        /// </summary>
        public const string KP_ERASE_END_LINE = "\x1B" + "K";
        /// <summary>
        /// Clear all the lines of text in the display window that 
        /// have not been written on since the last cleanup.
        /// </summary>
        public const string KP_CLEAN_ACTIVE = "\x1B" + "L";
        /// <summary>
        /// Position the text cursor to the next available position in
        /// the text display window and clear the line of text.
        /// </summary>
        public const string KP_NEXT_LINE = "\x1B" + "M";
        /// <summary>
        /// [f][num] [,[f][num]] ; Set Numeric field
        /// Set the numeric field specified by [f] to the value specified by
        /// [num].  The field id is a single ASCII character whose value is
        /// the desired field plus 31.  You can use more than 1 of the pairs
        /// each separated by commas and terminated by a semicolon.
        /// </summary>
        public const string KP_SET_NUM_FIELD = "\x1B" + "N";
        /// <summary>
        /// [tstr] Picture Terrrain Update String
        /// The first byte of [tstr] corresponds
        /// to picture position 1,1; the second byte corresponds to 1,2.
        /// </summary>
        public const string KP_PICTURE_TERRAIN_UPDATE = "\x1B" + "P";
        /// <summary>
        /// Play the sound effect specified by [snd]
        /// </summary>
        public const string KP_PLAY_SOUND = "\x1B" + "S";
        /// <summary>
        /// [dir] [istr] [end] Picture Icon Update String
        /// This function is used to send the critters and treasure data
        /// for the picture.  The [dir] bytes is the player course arrow.
        /// [istr] is composed of an arbitary number of three character
        /// packets that are:
        /// byte 1 : picture position 31 + (1..49)
        /// byte 2 : icon number : (treasure pile = 32) (a critter = 33) (several critters = 34) 
        /// byte 3 : display character A..L
        /// The end of the string [end] is an ascii 127
        /// </summary>
        public const string KP_PICTURE_ICON_UPDATE = "\x1B" + "U";
        /// <summary>
        /// [eff][p] Display Visual Effect (L 4)
        /// Display the specified visual effect [eff] centered at the specified
        /// map position [p].  The effect and position numbers are sent
        /// as ASCII codes whose whose values are the desired number plus
        /// 31.  The position number ranges from 1 to 49 corresponding
        /// to position (1,1) and (7,7) in the picture map.  The numbers
        /// are in row major order.
        /// </summary>
        public const string KP_DISPLAY_VISUAL_EFFECT = "\x1B" + "V";
        /// <summary>
        /// Direct cursor access [l][c]
        /// Move the cursor to the specified line [l] and column [c].  The
        /// line and column numbers are sent as ASCII codes whose values
        /// are the desired position plus 31. e.g. 32 refers to the first
        /// line or column.
        /// </summary>
        public const string KP_DIRECT_CURS = "\x1B" + "Y";
        #endregion
        #endregion       

		public static string[] LETTER = new string[]{"A", "B", "C", "D", "E","F", 
														"G", "H", "I", "J", "K", "L", "M", "N", "o", "P", "Q", "R", "S", "T","U", 
														"V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

        public void Add(Cell cell)
        {
            this.cells.Add("" + cell.X + "," + cell.Y + "," + cell.Z, cell);
        }

        public static Direction GetDirection(Cell from, Cell to)
        {
            if (from.X < to.X && from.Y < to.Y)
            {
                return Direction.Southeast;
            }
            else if (from.X < to.X && from.Y > to.Y)
            {
                return Direction.Northeast;
            }
            else if (from.X > to.X && from.Y < to.Y)
            {
                return Direction.Southwest;
            }
            else if (from.X > to.X && from.Y > to.Y)
            {
                return Direction.Northwest;
            }
            else if (from.X == to.X && from.Y > to.Y)
            {
                return Direction.North;
            }
            else if (from.X == to.X && from.Y < to.Y)
            {
                return Direction.South;
            }
            else if (from.X < to.X && from.Y == to.Y)
            {
                return Direction.East;
            }
            else if (from.X > to.X && from.Y == to.Y)
            {
                return Direction.West;
            }
            else
            {
                return Direction.None; // same cell
            }
        }

		public static void writeAtXY (Character ch, int y, int x, string str)
		{
			ch.Write("\x1b["+x+";"+y+"H");
			printTile(ch, str);
		}

		public static char kesProtoConvertGraphic(string cellgraphic)
		{
			char kGraphic;
			switch(cellgraphic)
			{
				case "o ":
					kGraphic = Map.KPTRASH;
					break;
				case ". ":
					kGraphic = Map.KPEMPTY;
					break;
				case "[]":
					kGraphic = Map.KPWALL;
					break;
				case "_]":
					kGraphic = Map.KPRUIN1;
					break;
				case "[_":
					kGraphic = Map.KPRUIN2;
					break;
				case "ZZ":
					kGraphic = Map.KPSTAIRUP;
					break;
				case "XX":
					kGraphic = Map.KPSTAIRDN;
					break;
				case "%%":								//Air
					kGraphic = Map.KPAIR;
					break;
				case "~~":
					kGraphic = Map.KPWATER;
					break;
				case "WW":								//Unpassable water
					kGraphic = Map.KPREEF;
					break;
				case "~.":
					kGraphic = Map.KPICE;
					break;
				case "^.":
				case "t ":	
				case "tt":								//Burnt tree
					kGraphic = Map.KPBURNT1;
					break;
				case " t":								//Burnt tree
					kGraphic = Map.KPBURNT2;
					break;
				case "t*":								//Burning tree
					kGraphic = Map.KPFIRE;
					break;
				case "*t":								//Burning tree
					kGraphic = Map.KPFIRE;
					break;
				case "@ ":								//Trees
					kGraphic = Map.KPTREE1;
					break;
				case "@@":								//Thick trees
					kGraphic = Map.KPFOREST1;
					break;
				case "TT":
					kGraphic = Map.KPIMPTREE;
					break;
				case "()":
					kGraphic = Map.KPPIT;
					break;
				case "BR":
					kGraphic = Map.KPRING;
					break;
				case " @":
					kGraphic = Map.KPTREE2;
					break;
				case ", ":								//Ashes
					kGraphic = Map.KPEMPTY;
					break;
				case "@$":
					kGraphic = Map.KPTREE1;
					break;
				case "**":
					kGraphic = Map.KPFIRE;
					break;
				case "v*":
					kGraphic = Map.KPFIRE;
					break;
				case "PS":
					kGraphic = Map.KPEMPTY;
					break;
				case "LK":
					kGraphic = Map.KPROOM;
					break;
				case "??":
					kGraphic = Map.KPDARKNESS;
					break;
				case ".$":
					kGraphic = Map.KPEMPTY;
					break;
				case "cd":
					kGraphic = Map.KPEMPTY;
					break;
				case "cu":
					kGraphic = Map.KPEMPTY;
					break;
				case "tp":								//Two way teleporter
					kGraphic = Map.KPTILE;
					break;
				case "pb":
					kGraphic = Map.KPEMPTY;
					break;
				case "MP":									//Map Portal
					kGraphic = Map.KPTILE;
				break;
				case "| ":
				case "VD":
					kGraphic = Map.KPVCDOOR;
					break;
				case "--":
				case "HD":
					kGraphic = Map.KPHCDOOR;
					break;
				case "\\ ":
					kGraphic = Map.KPHODOOR;
					break;
				case "/ ":
					kGraphic = Map.KPVODOOR;
					break;
				case "mm":
				case "MM":
					kGraphic = Map.KPALTAR;
					break;
				case "==":
				case "CC":
					kGraphic = Map.KPSHOPCOUNTER;
					break;
				case ".\\":
					kGraphic = Map.KPSAND;
					break;
				case "/\\":
					kGraphic = Map.KPIMPMOUNT;
					break;
				case "##":
					kGraphic = Map.KPGRATE;
					break;
				case "::":
					kGraphic = Map.KPBRIDGE;
					break;
				case "up":
					kGraphic = Map.KPSTAIRUP;
					break;
				case "dn":
					kGraphic = Map.KPSTAIRDN;
					break;
				case "\"":
					kGraphic = Map.KPGRASS;
					break;
				case "gg":
					kGraphic = Map.KPGRAVE;
					break;
				default:
					kGraphic = Map.KPNOVIS;
					break;
			}
			return kGraphic;
		}

        public static void clearMap(Character ch)
        {
            if (ch.protocol == DragonsSpineMain.APP_PROTOCOL)
            {
                ch.Write(Map.KP_DIRECT_CURS + (char)32 + (char)32 + Map.KP_ERASE_TO_END);
            }
            if (ch.protocol == "old-kesmai")
            {
                ch.Write(Map.KP_DIRECT_CURS + (char)32 + (char)32 + Map.KP_ERASE_TO_END); 
            }
            else
            {
                ch.Write(Map.CLS);
            }
        }

		public static string returnTile(string mapTile)
		{
			switch(mapTile)
			{
				case "  ":	//Vision blocked
				case "[]":	//Impenetrable Wall
				case "_]":	//Ruins
				case "[_":	//Ruins
				case "##":	//Grate
				case "@ ":	//Tree
				case " @":	//Tree
				case "@@":	//Trees (vision blocked)
				case "TT":	//Impassable trees
				case "%%":	//Air
				case "~~":	//Water
				case "WW":	//Impassable water
				case "~.":	//Ice
				case "::":	//Bridge or Dock
				case ".\\":	//Sand
				case "''":	//Grass
					return mapTile;
				case "MM": //Altar
					return "mm";
				case "**":	//Fire
				case "*0":
				case "*1":
				case "*2":
				case "*3":
				case "*4":
				case "*5":
				case "*6":
				case "*7":
				case "*8":
				case "*9":
					return "**";
				case "LK":
					return ". ";
				case "$$":
					return ". ";
				case "ZZ":	//Segueway up
                case "RU":
					return "up";
				case "XX":	//Segueway down
                case "RD":
					return "dn";
				case "cd":	//Climb down
				case "cu":	//Climb up
				case "pb":	//Pit bottom
				case "gg":
					return ". ";
				case "VD":
					return "| ";
				case "HD":
					return "--";
				case "CC":
					return "==";
				case "BR":	//Boxing Ring
					return "()"; 
				case "MP":
					return "::";
                case "??":
                    return "  ";
				default:
					return mapTile;
			}

		}

		public static void printTile(Character ch, string mapTile)
		{
			switch(mapTile)
			{
				case "[]":
				case "_]":
				case "[_":	//Walls and Ruins
					ch.Write(CYEL+mapTile+CEND);
					break;
				case "/\\":	//Mountain
					ch.Write(BBLK+"/\\"+CEND);
					break;
				case "%%":	//Air
					ch.Write(BWHT+"%%"+CEND);
					break;
				case "~~":	//Water
					ch.Write(BKBLU+BWHT+"~~"+CEND);
					break;
				case "ww":	//Web
					ch.Write(CGRN+"ww"+CEND);
					break;
				case "WW":	//Reef
					ch.Write(BKBLU+BBLK+"~~"+CEND);
					break;
				case "~.":	//Ice
					ch.Write(BKCYN+BCYN+mapTile+CEND);
					break;
				case "@ ":	//Trees
				case " @":
				case "@@":
					ch.Write(BGRN+mapTile+CEND);
					break;
				case "TT":	
					ch.Write(BKGRN+BBLK+mapTile+CEND);
					break;
				case "**":	//Fire
				case "*0":
				case "*1":
				case "*2":
				case "*3":
				case "*4":
				case "*5":
				case "*6":
				case "*7":
				case "*8":
				case "*9":
					ch.Write(BRED+"**"+CEND);
					break;
//				case "MP":	//Map Portal
//					ch.Write(BWHT+"::"+CEND);
//					break;
				case "mm":
				case "MM":
					ch.Write(CMAG+"mm"+CEND);
					break;
				case ".\\":	//Sand
					ch.Write(BYEL+mapTile+CEND);
					break;
				case "\"":
					ch.Write(CGRN+mapTile+CEND);
					break;
				default:
					ch.Write(mapTile);
					break;
			}
		}

        public bool LoadMap(string fileName, short facetID, short landID, short mapID)
        {
            short xOffset = 0;
            short x = 0;
            short y = 0;
            int z = 0;
            int a = 0;
            int effectLevel = 0;
            bool isOutdoors = false;
            bool isNoRecall = false;
            bool withinTownLimits = false;
            string forestry = "medium";
            Cell cell = null; // the cell that will be added to this map's dictionary

            try
            {
                if (!File.Exists(fileName))
                    return false;

                StreamReader sr = File.OpenText(fileName);

                string[] mapLines = sr.ReadToEnd().Split("\n".ToCharArray());

                for (a = 0; a < mapLines.Length; a++)
                {
                    mapLines[a] = mapLines[a].Replace("\r", "");
                    // does not start with a comment, is not a header line, and is odd in length
                    if (!mapLines[a].StartsWith("//") && !mapLines[a].StartsWith("<") && mapLines[a].Length % 2 == 1)
                    {
                        mapLines[a] = mapLines[a] + " ";
                        Utils.Log("Map line " + a + " was odd in length for " + this.Name, Utils.LogType.SystemWarning);
                    }
                }

                foreach (string s in mapLines)
                {
                    if (s.Length > 0 && !s.StartsWith("//")) // new
                    {
                        a = 0;
                        x = xOffset;
                        if (s.StartsWith("<"))
                        {
                            isOutdoors = false;
                            isNoRecall = false;
                            withinTownLimits = false;
                            forestry = "medium";

                            // entire z coord is outdoor
                            if (s.IndexOf("outdoor=true") != -1)
                                isOutdoors = true;

                            // entire z coord is no recall
                            if (s.IndexOf("norecall=true") != -1)
                                isNoRecall = true;

                            // entire z coord is within town limits
                            if (s.IndexOf("townlimits=true") != -1)
                                withinTownLimits = true;

                            // forestry level
                            if (s.IndexOf("<f>") != -1 && s.IndexOf("</f>") != -1)
                                forestry = s.Substring(s.IndexOf("<f>") + 3, s.IndexOf("</f>") - (s.IndexOf("<f>") + 3));

                            // x offset
                            if (s.IndexOf("<x>") != -1 && s.IndexOf("</x>") != -1)
                                xOffset = Convert.ToInt16(s.Substring(s.IndexOf("<x>") + 3, s.IndexOf("</x>") - (s.IndexOf("<x>") + 3)));
                            else xOffset = 0;

                            // y offset
                            if (s.IndexOf("<y>") != -1 && s.IndexOf("</y>") != -1)
                                y = Convert.ToInt16(s.Substring(s.IndexOf("<y>") + 3, s.IndexOf("</y>") - (s.IndexOf("<y>") + 3)));
                            else y = 0;

                            // z coord (height)
                            if (s.IndexOf("<z>") != -1 && s.IndexOf("</z>") != -1)
                                z = Convert.ToInt32(s.Substring(s.IndexOf("<z>") + 3, s.IndexOf("</z>") - (s.IndexOf("<z>") + 3)));
                            else z = 0;

                            // name of the z plane
                            if (s.IndexOf("<n>") != -1 && s.IndexOf("</n>") != -1)
                            {
                                if(!zNames.ContainsKey(z))
                                    zNames.Add(z, s.Substring(s.IndexOf("<n>") + 3, s.IndexOf("</n>") - (s.IndexOf("<n>") + 3)));
                            }

                            if (!this.zPlanes.Contains(z))
                                this.zPlanes.Add(z);

                            if (!this.xcordMax.ContainsKey(z))
                                this.xcordMax.Add(z, 0);
                            if (!this.xcordMin.ContainsKey(z))
                                this.xcordMin.Add(z, xOffset);
                            if (!this.ycordMax.ContainsKey(z))
                                this.ycordMax.Add(z, 0);
                            if (!this.ycordMin.ContainsKey(z))
                                this.ycordMin.Add(z, y);
                        }
                        else
                        {
                            while (a < s.Length)
                            {
                                if (a < s.Length)
                                {
                                    if (s.Substring(a, 2) != "  ")
                                    {
                                        cell = new Cell(facetID, landID, mapID, x, y, z, s.Substring(a, 2));

                                        if (!this.cells.ContainsKey("" + x + "," + y + "," + z))
                                            this.Add(cell);
                                        else
                                        {
                                            Utils.Log("Failed to add Cell(first) to " + this.Name + " " + x + "," + y + "," + z, Utils.LogType.SystemFailure);
                                            throw new Exception();
                                        }

                                        cell.IsPVPEnabled = this.IsPVPEnabled;
                                        cell.IsNoRecall = isNoRecall;
                                        cell.IsOutdoors = isOutdoors;
                                        cell.IsWithinTownLimits = withinTownLimits;

                                        #region if Fire Effect "**" "*1" "*9"
                                        if (cell.CellGraphic[0].ToString() == "*")
                                        {
                                            if (cell.CellGraphic == "**" || cell.CellGraphic == "*t")
                                            {
                                                effectLevel = 1;
                                            }
                                            else
                                            {
                                                effectLevel = Convert.ToInt32(cell.CellGraphic.Substring(1, 1));
                                                cell.CellGraphic = "**";
                                            }
                                            Effect effect = new Effect(Effect.EffectType.Fire, "**", effectLevel * 10, -1, null, cell);
                                        }
                                        #endregion

                                        else if (cell.CellGraphic[0].ToString() == ":")
                                        {
                                            switch (cell.CellGraphic)
                                            {
                                                case ":r":
                                                    cell.IsNoRecall = true;
                                                    break;
                                                case ":t":
                                                    cell.IsWithinTownLimits = true;
                                                    break;
                                            }
                                            cell.CellGraphic = "::";
                                        }

                                        else if (cell.CellGraphic[0].ToString() == "|")
                                        {
                                            switch (cell.CellGraphic)
                                            {
                                                case "|r":
                                                    cell.IsNoRecall = true;
                                                    break;
                                                case "|t":
                                                    cell.IsWithinTownLimits = true;
                                                    break;
                                            }
                                            cell.CellGraphic = "| ";
                                        }

                                        else if (cell.CellGraphic[0].ToString() == "-")
                                        {
                                            switch (cell.CellGraphic)
                                            {
                                                case "-r":
                                                    cell.IsNoRecall = true;
                                                    break;
                                                case "-t":
                                                    cell.IsWithinTownLimits = true;
                                                    break;
                                            }
                                            cell.CellGraphic = "--";
                                        }

                                        else if (cell.CellGraphic[0].ToString() == "i")
                                        {
                                            switch (cell.CellGraphic)
                                            {
                                                case "i0":
                                                    cell.IsOneHandClimbDown = true;
                                                    break;
                                                case "i1":
                                                    cell.IsOneHandClimbUp = true;
                                                    break;
                                                case "i2":
                                                    cell.IsTwoHandClimbDown = true;
                                                    break;
                                                case "i3":
                                                    cell.IsTwoHandClimbUp = true;
                                                    break;
                                            }
                                            cell.CellGraphic = "~.";
                                        }

                                        #region else if Ancestoring "a0" "a1"
                                        else if (cell.CellGraphic[0].ToString() == "a")
                                        {
                                            switch (cell.CellGraphic)
                                            {
                                                case "a0":
                                                    cell.IsAncestorStart = true;
                                                    break;
                                                case "a1":
                                                    cell.IsAncestorFinish = true;
                                                    break;
                                            }
                                            cell.CellGraphic = ". ";
                                        }
                                        #endregion

                                        #region else if cellGraphic[0] == "c" set as climb
                                        else if (cell.CellGraphic[0].ToString() == "c")
                                        {
                                            switch (cell.CellGraphic)
                                            {
                                                case "c0":
                                                    cell.IsOneHandClimbDown = true;
                                                    break;
                                                case "c1":
                                                    cell.IsOneHandClimbUp = true;
                                                    break;
                                                case "c2":
                                                    cell.IsTwoHandClimbDown = true;
                                                    break;
                                                case "c3":
                                                    cell.IsTwoHandClimbUp = true;
                                                    break;
                                            }
                                            cell.CellGraphic = ". ";
                                        }
                                        #endregion

                                        #region else if cellGraphic[0] == "~" "{" "." - checks for CellGraphic[1] == "t" for townlimits
                                        else if (cell.CellGraphic[0].ToString() == "~" && cell.CellGraphic[1].ToString() != "~")
                                        {
                                            switch (cell.CellGraphic)
                                            {
                                                case "~0":
                                                    cell.IsOneHandClimbDown = true;
                                                    cell.CellGraphic = "~~";
                                                    break;
                                                case "~1":
                                                    cell.IsOneHandClimbUp = true;
                                                    cell.CellGraphic = "~~";
                                                    break;
                                                case "~2":
                                                    cell.IsTwoHandClimbDown = true;
                                                    cell.CellGraphic = "~~";
                                                    break;
                                                case "~3":
                                                    cell.IsTwoHandClimbUp = true;
                                                    cell.CellGraphic = "~~";
                                                    break;
                                                case "~r":
                                                    cell.IsNoRecall = true;
                                                    cell.CellGraphic = "~~";
                                                    break;
                                                case "~t":
                                                    cell.IsWithinTownLimits = true;
                                                    cell.CellGraphic = "~~";
                                                    break;
                                            }
                                        }

                                        else if (cell.CellGraphic[0].ToString() == "{" && cell.CellGraphic[1].ToString() != "}")
                                        {

                                            switch (cell.CellGraphic)
                                            {
                                                case "{0":
                                                    cell.IsOneHandClimbDown = true;
                                                    cell.CellGraphic = "{}";
                                                    break;
                                                case "{1":
                                                    cell.IsOneHandClimbUp = true;
                                                    cell.CellGraphic = "{}";
                                                    break;
                                                case "{2":
                                                    cell.IsTwoHandClimbDown = true;
                                                    cell.CellGraphic = "{}";
                                                    break;
                                                case "{3":
                                                    cell.IsTwoHandClimbUp = true;
                                                    cell.CellGraphic = "{}";
                                                    break;
                                                case "{r":
                                                    cell.IsNoRecall = true;
                                                    cell.CellGraphic = "{}";
                                                    break;
                                                case "{t":
                                                    cell.IsWithinTownLimits = true;
                                                    cell.CellGraphic = "{}";
                                                    break;
                                            }
                                            int randomTree = Rules.RollD(1, 300);
                                            int randomBerries = Rules.RollD(1, 400);

                                            if (randomTree >= 0 && randomTree < 41)
                                            {
                                                cell.CellGraphic = " @";
                                            }
                                            else if (randomTree > 40 && randomTree < 81)
                                            {
                                                cell.CellGraphic = "@ ";
                                            }
                                            else
                                            {
                                                cell.CellGraphic = "@@";
                                            }

                                            if (randomBerries >= 1 && randomBerries <= 4 && this.HasBalmBushes)
                                            {
                                                cell.balmBerry = true;
                                                cell.dailyBerry = Rules.RollD(3, 20);
                                                if (cell.Description.Length > 0)
                                                {
                                                    cell.Description += " There is a bush covered with bright red berries here.";
                                                }
                                                else
                                                {
                                                    cell.Description = "There is a bush covered with bright red berries here.";
                                                }
                                            }
                                            else if (randomBerries == 10 && randomBerries == 11 && this.HasPoisonBushes)
                                            {
                                                cell.poisonBerry = true;
                                                cell.dailyBerry = Rules.RollD(2, 20);
                                                if (cell.Description.Length > 0)
                                                {
                                                    cell.Description += " There is a bush with pale yellow berries here.";
                                                }
                                                else
                                                {
                                                    cell.Description = "There is a bush with pale yellow berries here.";
                                                }
                                            }
                                            else if (randomBerries == 42 && this.HasManaBushes)
                                            {
                                                cell.manaBerry = true;
                                                cell.dailyBerry = Rules.RollD(1, 6);
                                                if (cell.Description.Length > 0)
                                                {
                                                    cell.Description += " There is a bush with light blue berries here.";
                                                }
                                                else
                                                {
                                                    cell.Description = "There is a bush with light blue berries here.";
                                                }
                                            }
                                            else if (randomBerries == 98 && this.HasStaminaBushes)
                                            {
                                                cell.stamBerry = true;
                                                cell.dailyBerry = Rules.RollD(1, 10);
                                                if (cell.Description.Length > 0)
                                                {
                                                    cell.Description += " There is a bush with dark green berries here.";
                                                }
                                                else
                                                {
                                                    cell.Description = "There is a bush with dark green berries here.";
                                                }
                                            }
                                        }
                                        else if (cell.CellGraphic[0].ToString() == "." && cell.CellGraphic[1].ToString() != " ")
                                        {
                                            switch (cell.CellGraphic)
                                            {
                                                case ".r":
                                                    cell.IsNoRecall = true;
                                                    cell.CellGraphic = ". ";
                                                    break;
                                                case ".t":
                                                    cell.IsWithinTownLimits = true;
                                                    cell.CellGraphic = ". ";
                                                    break;
                                                case ".0":
                                                    cell.IsOneHandClimbDown = true;
                                                    cell.CellGraphic = ". ";
                                                    break;
                                                case ".1":
                                                    cell.IsOneHandClimbUp = true;
                                                    cell.CellGraphic = ". ";
                                                    break;
                                                case ".2":
                                                    cell.IsTwoHandClimbDown = true;
                                                    cell.CellGraphic = ". ";
                                                    break;
                                                case ".3":
                                                    cell.IsTwoHandClimbUp = true;
                                                    cell.CellGraphic = ". ";
                                                    break;
                                                case ".f": // Black Fog
                                                    cell.CellGraphic = ". ";
                                                    Effect effect = new Effect(Effect.EffectType.Black_Fog, ". ", 0, -1, null, cell);
                                                    break;
                                            }

                                        }
                                        #endregion

                                        #region else if Pit "()" set IsTwoHandClimbDown
                                        else if (cell.CellGraphic == "()")
                                        {
                                            cell.IsTwoHandClimbDown = true;
                                        }
                                        #endregion

                                        #region else if Pit Bottom "pb" set IsTwoHandClimbUp
                                        else if (cell.CellGraphic == "pb")
                                        {
                                            cell.IsTwoHandClimbUp = true;
                                            cell.CellGraphic = ". ";
                                        }
                                        #endregion

                                        #region else if Graveyard "gg" set IsUnderworldPortal
                                        else if (cell.CellGraphic == "gg")
                                        {
                                            cell.IsUnderworldPortal = true;
                                        }
                                        #endregion

                                        #region else if permanent Darkness Effect "??"
                                        else if (cell.CellGraphic == "??")
                                        {
                                            cell.IsAlwaysDark = true;
                                            cell.CellGraphic = "??";
                                        }
                                        #endregion

                                        #region else if Web Effect "wb" "w1" "w9"
                                        else if (cell.CellGraphic[0].ToString() == "w")
                                        {
                                            if (cell.CellGraphic == "wb")
                                                effectLevel = 6;
                                            else
                                            {
                                                effectLevel = Convert.ToInt32(cell.CellGraphic.Substring(1, 1)) + 10;
                                                cell.CellGraphic = "ww";
                                            }
                                            Effect effect = new Effect(Effect.EffectType.Web, "ww", effectLevel, -1, null, cell);
                                        }
                                        #endregion

                                        #region else if Magic Dead wall "DD"
                                        else if (cell.CellGraphic == "DD")
                                        {
                                            cell.CellGraphic = "[]";
                                            cell.IsMagicDead = true;
                                        }
                                        #endregion

                                        #region else if Locker "LK"
                                        else if (cell.CellGraphic == "LK")
                                        {
                                            cell.CellGraphic = ". ";
                                            cell.IsLocker = true;
                                        }
                                        #endregion

                                        else if (cell.CellGraphic == "MR")
                                        {
                                            cell.CellGraphic = ". ";
                                            cell.HasMirror = true;
                                            if (cell.Description.Length > 0)
                                                cell.Description += " There is a wall mirror here.";
                                            else cell.Description = "There is a wall mirror here.";
                                        }

                                        else if (cell.CellGraphic == "HD")
                                        {
                                            cell.CellGraphic = "--";
                                            cell.IsLockedHorizontalDoor = true;
                                        }

                                        else if (cell.CellGraphic == "VD")
                                        {
                                            cell.CellGraphic = "| ";
                                            cell.IsLockedVerticalDoor = true;
                                        }
                                        else if (cell.CellGraphic == "ut")
                                        {
                                            cell.CellGraphic = "up";
                                            cell.IsWithinTownLimits = true;
                                        }
                                        else if (cell.CellGraphic == "dt")
                                        {
                                            cell.CellGraphic = "dn";
                                            cell.IsWithinTownLimits = true;
                                        }
                                        #region Secret Door "SD"
                                        else if (cell.CellGraphic == "SD")
                                        {
                                            cell.IsSecretDoor = true;
                                            cell.CellGraphic = "[]";
                                        }
                                        #endregion

                                        #region Secret Rockwall "SM"
                                        else if (cell.CellGraphic == "SM")
                                        {
                                            cell.IsSecretDoor = true;
                                            cell.CellGraphic = "/\\";
                                        }
                                        #endregion

                                        #region Random Trees / Berries "{}" "@@"
                                        else if (cell.CellGraphic == "{}" || cell.CellGraphic == "@@")
                                        {
                                            int randomTree = Rules.RollD(1, 200);
                                            int randomBerries = Rules.RollD(1, 300);

                                            switch (forestry.ToLower())
                                            {
                                                case "very light":
                                                    randomTree -= 175;
                                                    randomBerries = 0;
                                                    break;
                                                case "light":
                                                    randomTree -= 100;
                                                    randomBerries -= 50;
                                                    break;
                                                case "medium":
                                                    randomTree -= 50;
                                                    break;
                                                case "heavy":
                                                    randomTree += 100;
                                                    break;
                                                case "very heavy":
                                                    randomTree += 175;
                                                    break;
                                            }

                                            if (randomTree < 0)
                                                cell.CellGraphic = "\"\"";
                                            else if (randomTree >= 0 && randomTree <= 200)
                                            {
                                                if (Rules.RollD(1, 20) <= 10)
                                                    cell.CellGraphic = " @";
                                                else cell.CellGraphic = "@ ";
                                            }
                                            else
                                                cell.CellGraphic = "@@";

                                            if (randomTree >= 0)
                                            {
                                                if (randomBerries >= 1 && randomBerries <= 4 && this.HasBalmBushes)
                                                {
                                                    cell.balmBerry = true;
                                                    cell.dailyBerry = Rules.dice.Next(3, 60);
                                                    if (cell.Description.Length > 0)
                                                    {
                                                        cell.Description += " There is a bush covered with bright red berries here.";
                                                    }
                                                    else
                                                    {
                                                        cell.Description = "There is a bush covered with bright red berries here.";
                                                    }
                                                }
                                                else if (randomBerries == 10 && randomBerries == 11 && this.HasPoisonBushes)
                                                {
                                                    cell.poisonBerry = true;
                                                    cell.dailyBerry = Rules.dice.Next(2, 20);
                                                    if (cell.Description.Length > 0)
                                                    {
                                                        cell.Description += " There is a bush with pale yellow berries here.";
                                                    }
                                                    else
                                                    {
                                                        cell.Description = "There is a bush with pale yellow berries here.";
                                                    }
                                                }
                                                else if (randomBerries == 42 && this.HasManaBushes)
                                                {
                                                    cell.manaBerry = true;
                                                    cell.dailyBerry = Rules.dice.Next(1, 6);
                                                    if (cell.Description.Length > 0)
                                                    {
                                                        cell.Description += " There is a bush with light blue berries here.";
                                                    }
                                                    else
                                                    {
                                                        cell.Description = "There is a bush with light blue berries here.";
                                                    }
                                                }
                                                else if (randomBerries == 98 && this.HasStaminaBushes)
                                                {
                                                    cell.stamBerry = true;
                                                    cell.dailyBerry = Rules.dice.Next(1, 10);
                                                    if (cell.Description.Length > 0)
                                                    {
                                                        cell.Description += " There is a bush with dark green berries here.";
                                                    }
                                                    else
                                                    {
                                                        cell.Description = "There is a bush with dark green berries here.";
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        #region Random Dead Trees "{_"
                                        else if (cell.CellGraphic == "{_")
                                        {
                                            switch (Rules.RollD(1, 3))
                                            {
                                                case 1:
                                                    cell.CellGraphic = " t";
                                                    break;
                                                case 2:
                                                    cell.CellGraphic = "t ";
                                                    break;
                                                case 3:
                                                    cell.CellGraphic = "tt";
                                                    break;
                                            }
                                        }
                                        #endregion

                                        switch (cell.CellGraphic)
                                        {
                                            case "/\\":
                                            case "WW":
                                            case "==":
                                            case "mm":
                                            case "TT":
                                            case "MM":
                                            case "CC":
                                                cell.IsMagicDead = true;
                                                break;
                                        }

                                        cell.DisplayGraphic = Map.returnTile(cell.CellGraphic);
                                    }
                                    else
                                    {
                                        cell = new Cell(facetID, landID, mapID, x, y, z, "/\\");
                                        cell.IsOutOfBounds = true;
                                        if (!this.cells.ContainsKey("" + x + "," + y + "," + z))
                                        {
                                            this.Add(cell);
                                        }
                                        else
                                        {
                                            Utils.Log("Failed to add Cell(second) to " + this.Name + " " + x + "," + y + "," + z, Utils.LogType.SystemFailure);
                                            throw new Exception();
                                        }
                                    }
                                }
                                a += 2;
                                x++;
                                if (x > this.xcordMax[z])
                                {
                                    this.xcordMax[z] = x;
                                }
                            }
                            y++;
                            if (y > this.ycordMax[z])
                            {
                                this.ycordMax[z] = y;
                            }
                        }
                    }
                }

                // now that the cells are all created, we need to calculate the display on each one
                this.UpdateMapCellVisible();

                // load the cell information from the Cell table
                List<Cell> cellsList = DAL.DBWorld.GetCellList(this.Name);
                string key = "";
                foreach (Cell iCell in cellsList)
                {
                    key = iCell.X.ToString() + "," + iCell.Y.ToString() + "," + iCell.Z.ToString();
                    if (this.cells.ContainsKey(key))
                    {
                        this.cells[key].Segue = iCell.Segue;
                        this.cells[key].Description.Trim();
                        this.cells[key].Description = iCell.Description + " " + this.cells[key].Description;
                        this.cells[key].Description.Trim();
                        this.cells[key].cellLock = iCell.cellLock;
                        this.cells[key].IsMapPortal = iCell.IsMapPortal;
                        this.cells[key].IsTeleport = iCell.IsTeleport;
                        this.cells[key].IsSingleCustomer = iCell.IsSingleCustomer;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return false;
            }
		} 

		public static void LoadLairLoot(NPC laircrit, string[] lairVeryCommonLoot, string[] lairCommonLoot,
			string[] lairRareLoot, string[] lairVeryRareLoot)
		{
			int roll = 0;
			int amount = 1;
			int x = 0;

            Item item = null;

            ArrayList CritterLair = new ArrayList();
            foreach (Cell lcell in laircrit.lairCellsList)
            {
                CritterLair.Add(lcell);
            }

            if (CritterLair.Count > 0)
            {
                Effect effect = new Effect(Effect.EffectType.Fire, "**", 1, 1, laircrit, CritterLair);
            }

			foreach(Cell laircell in laircrit.lairCellsList)
			{
				//reset x to 0
				x = 0;
				//clear this lair cell
				laircell.Items.Clear();
				//always add random coins to lair cell using laircrits gold value
				item = Item.CopyItemFromDictionary(Item.ID_COINS);
				item.coinValue = Rules.dice.Next((int)laircrit.Gold / 2, laircrit.Gold * 2 + 1);
				laircell.Add(item);
				//how many items go on this cell
				amount = ((int)laircrit.Level / 2) + 1;
				while(x < amount)
				{
					roll = Rules.RollD(1, 100);
					// add very common item 50%
					if(roll >= 20 && roll < 70 && lairVeryCommonLoot.Length > 0)
					{
						roll = Rules.dice.Next(lairVeryCommonLoot.Length);
						item = Item.CopyItemFromDictionary(Convert.ToInt32(lairVeryCommonLoot[roll]));
						if(item == null){Utils.Log("loadLairLoot npcID["+laircrit.npcID+"] itemID["+lairVeryCommonLoot[roll], Utils.LogType.SystemFailure);}
						else
						{
							if(item.vRandLow > 0){item.coinValue = Rules.dice.Next(item.vRandLow,item.vRandHigh);}
							laircell.Items.Add(item);
						}
					}
					// add common item 25%
					else if(roll >= 70 && roll < 95 && lairCommonLoot.Length > 0)
					{
						roll = Rules.dice.Next(lairCommonLoot.Length);
						item = Item.CopyItemFromDictionary(Convert.ToInt32(lairCommonLoot[roll]));
						if(item == null){Utils.Log("loadLairLoot npcID["+laircrit.npcID+"] itemID["+lairCommonLoot[roll], Utils.LogType.SystemFailure);}
						else
						{
							if(item.vRandLow > 0){item.coinValue = Rules.dice.Next(item.vRandLow,item.vRandHigh);}
							laircell.Items.Add(item);
						}
					}
					//add rare item 5%
					else if(roll >= 95 && roll < 100 && lairRareLoot.Length > 0)
					{
						roll = Rules.dice.Next(lairRareLoot.Length);
						item = Item.CopyItemFromDictionary(Convert.ToInt32(lairRareLoot[roll]));
						if(item == null){Utils.Log("loadLairLoot npcID["+laircrit.npcID+"] itemID["+lairRareLoot[roll], Utils.LogType.SystemFailure);}
						else
						{
							if(item.vRandLow > 0){item.coinValue = Rules.dice.Next(item.vRandLow,item.vRandHigh);}
							laircell.Items.Add(item);
						}
					}
					//add very rare item 1%
					else if(roll >= 100 && lairVeryRareLoot.Length > 0)
					{
						roll = Rules.dice.Next(lairVeryRareLoot.Length);
						item = Item.CopyItemFromDictionary(Convert.ToInt32(lairVeryRareLoot[roll]));
						if(item == null){Utils.Log("loadLairLoot npcID["+laircrit.npcID+"] itemID["+lairVeryRareLoot[roll], Utils.LogType.SystemFailure);}
						else
						{
							if(item.vRandLow > 0){item.coinValue = Rules.dice.Next(item.vRandLow,item.vRandHigh);}
							laircell.Items.Add(item);
						}
					}
					x++;

					//Utils.Log("Lair loot: "+num+" items added to cell: M:"+lootcell.map+" X:"+lootcell.xcord+" Y:"+lootcell.ycord);
				}
			}
		}

        public void UpdateMapCellVisible()
        {
            //Now that the cells are all created, we need to calculate the display on each one
            int y = 0;
            int x = 0;
            int z = 0;

            try
            {
                for (z = 0; z < this.zPlanes.Count; z++)
                {
                    y = this.YCordMin[this.ZPlanes[z]];

                    while (y <= this.YCordMax[this.ZPlanes[z]])
                    {
                        while (x <= this.XCordMax[this.ZPlanes[z]])
                        {
                            if (this.cells.ContainsKey(x+","+y+","+this.zPlanes[z]))
                            {
                                this.UpdateCellVisible(this.cells[x + "," + y + "," + this.zPlanes[z]]);
                            }
                            x++;
                        }
                        y++;
                        x = this.XCordMin[this.ZPlanes[z]];
                    }
                }
            }
            catch (Exception e)
            {
                Utils.Log("UpdateDisplay Failed: " + e.Message + "  Map = " + this.Name + "  Y = " + y + "  X = " + x, Utils.LogType.SystemFailure);
            }
        }

        public void UpdateVisionCellVisible(Cell cell)
        {
            if (cell == null)
            {
                return;
            }

            int y = cell.Y - 3;
            int x = cell.X - 3;

            try
            {
                while (y <= cell.Y + 3)
                {
                    while (x <= cell.X + 3)
                    {
                        if (this.cells.ContainsKey(x+","+y+","+cell.Z))
                        {
                            this.UpdateCellVisible(this.cells[x + "," + y + "," + cell.Z]);
                        }
                        x++;
                    }
                    y++;
                    x = cell.X - 3;
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        #region Old LOS Code        
        //public void updateCellVisible(Cell cell)
        //{
        //    /* Map position reminder:
        //     * 00010203040506
        //     * 07080910111213
        //     * 14151617181920
        //     * 212223==252627
        //     * 28293031323334
        //     * 35363738494041
        //     * 42434445464748
        //     */
        //    int x, y, z;
        //    try
        //    {
        //        // Reset the visCells to true
        //        if (cell == null)
        //        {
        //            return;
        //        }

        //        // Aliases to make lines more manageable
        //        x = cell.X; y = cell.Y; z = cell.Z;

        //        cell.visCells.SetAll(true); // set all to true

        //        //int i;

        //        //// within range of out of bounds north
        //        //if (y < YCordMin[z])
        //        //{
        //        //    int end = 21 - y * 7;
        //        //    for (i = 0; i < end; i++) cell.visCells[i] = false;
        //        //}

        //        //// within range of OOB south
        //        //if (y > YCordMax[z])// - 3)
        //        //{
        //        //    int start = 28 + (YCordMax[z] - y) * 7;
        //        //    for (i = start; i < 49; i++) cell.visCells[i] = false;
        //        //}

        //        //// within range of OOB west
        //        //if (x < XCordMin[z])
        //        //{
        //        //    for (i = 0; i < 49; i++) if (i % 7 < (3 - x)) cell.visCells[i] = false;
        //        //}

        //        //// within range of OOB east
        //        //if (x > XCordMax[z])// - 3)
        //        //{
        //        //    for (i = 0; i < 49; i++) if (i % 7 > (3 + XCordMax[z] - x)) cell.visCells[i] = false;
        //        //}

        //    /* Map position reminder:
        //     * 00010203040506
        //     * 07080910111213
        //     * 14151617181920
        //     * 212223==252627
        //     * 28293031323334
        //     * 35363738494041
        //     * 42434445464748
        //     */

        //        #region straight, 2 cells deep (exclude adjacent cells)
        //        if (visBlockedYX(y - 1, x, z))
        //        {
        //            cell.visCells[10] = false;
        //            cell.visCells[3] = false;
        //        }
        //        if (visBlockedYX(y, x - 1, z))
        //        {
        //            cell.visCells[22] = false;
        //            cell.visCells[21] = false;
        //        }
        //        if (visBlockedYX(y, x + 1, z))
        //        {
        //            cell.visCells[26] = false;
        //            cell.visCells[27] = false;
        //        }
        //        if (visBlockedYX(y + 1, x, z))
        //        {
        //            cell.visCells[38] = false;
        //            cell.visCells[45] = false;
        //        } 
        //        #endregion

        //        #region straight, 1 cell deep (exclude adjacent cells)
        //        if (visBlockedYX(y - 2, x, z))
        //        {
        //            cell.visCells[3] = false;
        //        }
        //        if (visBlockedYX(y, x - 2, z))
        //        {
        //            cell.visCells[21] = false;
        //        }
        //        if (visBlockedYX(y, x + 2, z))
        //        {
        //            cell.visCells[27] = false;
        //        }
        //        if (visBlockedYX(y + 2, x, z))
        //        {
        //            cell.visCells[45] = false;
        //        } 
        //        #endregion

        //        #region diagonal, adjacent cell is vision blocked so block all
        //        if (visBlockedYX(y - 1, x - 1, z))
        //        {
        //            cell.visCells[0] = false;
        //            cell.visCells[1] = false;
        //            cell.visCells[7] = false;
        //            cell.visCells[8] = false;
        //        }
        //        if (visBlockedYX(y - 1, x + 1, z))
        //        {
        //            cell.visCells[5] = false;
        //            cell.visCells[6] = false;
        //            cell.visCells[12] = false;
        //            cell.visCells[13] = false;
        //        }
        //        if (visBlockedYX(y + 1, x - 1, z))
        //        {
        //            cell.visCells[35] = false;
        //            cell.visCells[36] = false;
        //            cell.visCells[42] = false;
        //            cell.visCells[43] = false;
        //        }
        //        if (visBlockedYX(y + 1, x + 1, z))
        //        {
        //            cell.visCells[40] = false;
        //            cell.visCells[41] = false;
        //            cell.visCells[47] = false;
        //            cell.visCells[48] = false;
        //        } 
        //        #endregion

        //        #region 4 corners
        //        if (visBlockedYX(y - 2, x - 2, z))
        //        {
        //            cell.visCells[0] = false;
        //        }
        //        if (visBlockedYX(y - 2, x + 2, z))
        //        {
        //            cell.visCells[6] = false;
        //        }
        //        if (visBlockedYX(y + 2, x - 2, z))
        //        {
        //            cell.visCells[42] = false;
        //        }
        //        if (visBlockedYX(y + 2, x + 2, z))
        //        {
        //            cell.visCells[48] = false;
        //        } 
        //        #endregion

        //        // multi up down
        //        if (visBlockedYX(y - 1, x, z) && visBlockedYX(y - 1, x - 1, z))
        //        {
        //            cell.visCells[1] = false; cell.visCells[2] = false; cell.visCells[9] = false;
        //        }
        //        if (visBlockedYX(y - 1, x, z) && visBlockedYX(y - 1, x + 1, z))
        //        {
        //            cell.visCells[4] = false; cell.visCells[5] = false; cell.visCells[11] = false;
        //        }

        //        if (visBlockedYX(y + 1, x, z) && visBlockedYX(y + 1, x - 1, z))
        //        {
        //            cell.visCells[43] = false; cell.visCells[44] = false; cell.visCells[37] = false;
        //        }
        //        if (visBlockedYX(y + 1, x, z) && visBlockedYX(y + 1, x + 1, z))
        //        {
        //            cell.visCells[46] = false; cell.visCells[47] = false; cell.visCells[39] = false;
        //        }

        //        // multi left right
        //        if (visBlockedYX(y, x - 1, z) && visBlockedYX(y - 1, x - 1, z))
        //        {
        //            cell.visCells[14] = false; cell.visCells[7] = false; cell.visCells[15] = false;
        //        }
        //        if (visBlockedYX(y, x + 1, z) && visBlockedYX(y + 1, x + 1, z))
        //        {
        //            cell.visCells[33] = false; cell.visCells[34] = false; cell.visCells[41] = false;
        //        }
        //        if (visBlockedYX(y, x - 1, z) && visBlockedYX(y + 1, x - 1, z))
        //        {
        //            cell.visCells[28] = false; cell.visCells[35] = false; cell.visCells[29] = false;
        //        }
        //        if (visBlockedYX(y, x + 1, z) && visBlockedYX(y - 1, x + 1, z))
        //        {
        //            cell.visCells[13] = false; cell.visCells[20] = false; cell.visCells[19] = false;
        //        }

        //        if (visBlockedYX(y - 2, x, z) && visBlockedYX(y - 2, x - 1, z)) { cell.visCells[2] = false; }
        //        if (visBlockedYX(y - 2, x, z) && visBlockedYX(y - 2, x + 1, z)) { cell.visCells[4] = false; }
        //        if (visBlockedYX(y - 1, x + 2, z) && visBlockedYX(y, x + 2, z)) { cell.visCells[20] = false; }
        //        if (visBlockedYX(y + 1, x + 2, z) && visBlockedYX(y, x + 2, z)) { cell.visCells[34] = false; }
        //        if (visBlockedYX(y + 2, x + 1, z) && visBlockedYX(y + 2, x, z)) { cell.visCells[46] = false; }
        //        if (visBlockedYX(y + 2, x - 1, z) && visBlockedYX(y + 2, x, z)) { cell.visCells[44] = false; }
        //        if (visBlockedYX(y, x - 2, z) && visBlockedYX(y - 1, x - 2, z)) { cell.visCells[14] = false; }
        //        if (visBlockedYX(y, x - 2, z) && visBlockedYX(y + 1, x - 2, z)) { cell.visCells[28] = false; }

        //        if (visBlockedYX(y - 1, x - 1, z) && visBlockedYX(y - 1, x, z))
        //        {
        //            cell.visCells[1] = false; cell.visCells[2] = false;
        //        }
        //        if (visBlockedYX(y - 1, x + 1, z) && visBlockedYX(y - 1, x, z))
        //        {
        //            cell.visCells[4] = false; cell.visCells[5] = false;
        //        }
        //        if (visBlockedYX(y + 1, x - 1, z) && visBlockedYX(y + 1, x, z))
        //        {
        //            cell.visCells[43] = false; cell.visCells[44] = false;
        //        }
        //        if (visBlockedYX(y + 1, x + 1, z) && visBlockedYX(y + 1, x, z))
        //        {
        //            cell.visCells[46] = false; cell.visCells[47] = false;
        //        }
        //        if (visBlockedYX(y - 1, x - 1, z) && visBlockedYX(y, x - 1, z))
        //        {
        //            cell.visCells[7] = false; cell.visCells[14] = false;
        //        }
        //        if (visBlockedYX(y + 1, x + 1, z) && visBlockedYX(y, x + 1, z))
        //        {
        //            cell.visCells[34] = false; cell.visCells[41] = false;
        //        }
 
        //    /* Map position reminder:
        //     * 00010203040506
        //     * 07080910111213
        //     * 14151617181920
        //     * 212223==252627
        //     * 28293031323334
        //     * 35363738494041
        //     * 42434445464748
        //     */
            
        //        if (visBlockedYX(y - 1, x - 2, z))
        //        {
        //            cell.visCells[7] = false;
        //        }
        //        if (visBlockedYX(y - 1, x + 2, z))
        //        {
        //            cell.visCells[13] = false;
        //        }
        //        if (visBlockedYX(y + 1, x - 2, z))
        //        {
        //            cell.visCells[35] = false;
        //        }
        //        if (visBlockedYX(y + 1, x + 2, z))
        //        {
        //            cell.visCells[41] = false;
        //        }
        //        if (visBlockedYX(y - 2, x - 1, z))
        //        {
        //            cell.visCells[1] = false;
        //        }
        //        if (visBlockedYX(y - 2, x + 1, z))
        //        {
        //            cell.visCells[5] = false;
        //        }
        //        if (visBlockedYX(y + 2, x - 1, z))
        //        {
        //            cell.visCells[43] = false; 
        //        }
        //        if (visBlockedYX(y + 2, x + 1, z))
        //        {
        //            cell.visCells[47] = false;
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        Utils.LogException(e);
        //    }
        //}
        #endregion
        
        #region Erinoc LOS Code
            /* Map position reminder:
             * 0001xx03xx0506
             * 07080910111213
             * xx1516171819xx
             * 212223==252627
             * xx2930313233xx
             * 35363738494041
             * 4243xx45xx4748
             */

        bool ApplyViewRules(Cell c, int dx, int dy)
        {
            int x = c.X, y = c.Y, z = c.Z;
            int mx = dx + 3, my = dy + 3;
            int ax = Math.Abs(dx), ay = Math.Abs(dy);
            int sx = Math.Sign(dx), sy = Math.Sign(dy);
            int dx2, dy2;

            if (dx == 0 || dy == 0 || ax == ay)
            {
                // Rule 1 - If on direct cross or diag, check the one closer on the same axis
                if (ax <= 1 && ay <= 1)
                {
                    return true; // this fixes the trees around the character
                }
                if (c.visCells[(mx - sx) + 7 * (my - sy)])
                {
                    return !visBlockedYX(y + dy - sy, x + dx - sx, z);
                }
                return false;

            }

            dx2 = ax > ay ? sx : 0;
            dy2 = ay > ax ? sy : 0;

            if (ax == 1 || ay == 1)
            {
                // Rule 2 - If one off of horizontal or vertical, check the two closer via move to diag and move to cross
                if (visBlockedYX(y + sy, x + sx, z) && visBlockedYX(y + dy2, x + dx2, z))
                {
                    return false;
                }
                // Rule 2a - if three away, match the one two away
                if (Math.Max(ax, ay) == 3)
                {
                    if (visBlockedYX(y + dy - sy, x + dx - sx, z) && visBlockedYX(y + dy - dy2, x + dx - dx2, z))
                    {
                        return false;
                    }
                    return true;
                }
                return true;
            }
            // Rule 3 - If at 3,2 (with reflection) then false if either 2,1 or 1,1 is blocked
            // Double the dx's.  They start as +-1, 0 or 0, +-1, so this gives the relative move to 1,1
            dx2 += sx;
            dy2 += sy;
            if (c.visCells[(mx - sx) + 7 * (my - sy)] && c.visCells[(mx - dx2) + 7 * (my - dy2)])
            {
                if (!visBlockedYX(y + dy - sy, x + dx - sx, z) && !visBlockedYX(y + dy - dy2, x + dx - dx2, z))
                {
                    return true;
                }
            }
            return false;
        }

        public void UpdateCellVisible(Cell cell)
        {
            for (int round = 1; round <= 3; round++)
            {
                for (int i = -round; i < round; i++)
                {
                    cell.visCells[(3 - round) + 7 * (3 - i)] = ApplyViewRules(cell, -round, -i);
                    cell.visCells[(3 + round) + 7 * (3 + i)] = ApplyViewRules(cell, round, i);
                    cell.visCells[(3 + i) + 7 * (3 - round)] = ApplyViewRules(cell, i, -round);
                    cell.visCells[(3 - i) + 7 * (3 + round)] = ApplyViewRules(cell, -i, round);
                }
            }

            cell.visCells[24] = true;
        }

        //bool ApplyViewRules(Cell c, int dx, int dy)
        //{
        //    int x = c.X, y = c.Y, z = c.Z;
        //    int mx = dx + 3, my = dy + 3;
        //    int ax = Math.Abs(dx), ay = Math.Abs(dy);
        //    int sx = Math.Sign(dx), sy = Math.Sign(dy);
        //    int dx2, dy2;

        //    if (dx == 0 || dy == 0 || ax == ay)
        //    {
        //        // Rule 1 - If on direct cross or diag, check the one closer on the same axis
        //        if (ax <= 1 && ay <= 1)
        //        {
        //            return true; // this fixes the trees around the character
        //        }
        //        if (c.visCells[(mx - sx) + 7 * (my - sy)])
        //        {
        //            return !visBlockedYX(y + dy - sy, x + dx - sx, z);
        //        }
        //        return false;

        //    }
        //    dx2 = ax > ay ? sx : 0;
        //    dy2 = ay > ax ? sy : 0;
        //    if (ax == 1 || ay == 1)
        //    {
        //        // Rule 2 - If one off of horizontal or vertical, check the two closer via move to diag and move to cross
        //        if (visBlockedYX(y + sy, x + sx, z) && visBlockedYX(y + dy2, x + dx2, z))
        //        {
        //            return false;
        //        }
        //        // Rule 2a - if three away, match the one two away
        //        if (Math.Max(ax, ay) == 3)
        //        {
        //            return !visBlockedYX(y + dy - sy, x + dx - sx, z);
        //        }
        //        return true;
        //    }
        //    // Rule 3 - If at 3,2 (with reflection) then false if either 2,1 or 1,1 is blocked
        //    // Double the dx's.  They start as +-1, 0 or 0, +-1, so this gives the relative move to 1,1
        //    dx2 += sx;
        //    dy2 += sy;
        //    if (c.visCells[(mx - sx) + 7 * (my - sy)] && c.visCells[(mx - dx2) + 7 * (my - dy2)])
        //    {
        //        if (!visBlockedYX(y + dy - sy, x + dx - sx, z) && !visBlockedYX(y + dy - dy2, x + dx - dx2, z))
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //public void UpdateCellVisible(Cell cell)
        //{
        //    for (int round = 1; round <= 3; round++)
        //    {
        //        for (int i = -round; i < round; i++)
        //        {
        //            cell.visCells[(3 - round) + 7 * (3 - i)] = ApplyViewRules(cell, -round, -i);
        //            cell.visCells[(3 + round) + 7 * (3 + i)] = ApplyViewRules(cell, round, i);
        //            cell.visCells[(3 + i) + 7 * (3 - round)] = ApplyViewRules(cell, i, -round);
        //            cell.visCells[(3 - i) + 7 * (3 + round)] = ApplyViewRules(cell, -i, round);
        //        }
        //    }
        //    cell.visCells[24] = true;
        //}
        #endregion
        
        public bool visBlockedYX(int y, int x, int z)
        {
            string key = x + "," + y + "," + z;

            if (this.cells.ContainsKey(key))
                return IsVisionBlocked(this.cells[x + "," + y + "," + z]);

            return false;
        }

        public static bool IsVisionBlocked(Cell cell)
        {
            if (cell.Effects.ContainsKey(Effect.EffectType.Darkness) || cell.IsAlwaysDark)
                return true;

            string cellGraphic = cell.DisplayGraphic;

            switch (cell.DisplayGraphic)
            {
                case "[]":
                case "/\\":
                case "TT":
                case "| ":
                case "--":
                case "WW":
                case "@@": //TODO: future, if Ranger then vision is not blocked
                case "??":
                    return true;
                default:
                    return false;
            }
        }

		public static bool IsSpellPathBlocked(Cell cell)
		{
            if (cell == null)
                return true;

			switch(cell.DisplayGraphic)
			{
				case "[]":
				case "/\\":
				case "TT":
				case "~~":
				case "--":
				case "| ":
				case "%%":
				case "WW":
                case "##":
                case "mm":
                case "==":
                case "MM":
                case "CC":
					return true;
				default:
					return false;
			}
		}

		// This is the function that moves the character around
		public static void MoveChar(Character ch, string direction, string args)
		{
            switch (ch.FirstJoinedCommand)
            {
                case Command.CommandType.Attack:
                case Command.CommandType.Bash:
                case Command.CommandType.Cast:
                case Command.CommandType.Chant:
                case Command.CommandType.Crawl:
                case Command.CommandType.Jumpkick:
                case Command.CommandType.Kick:
                case Command.CommandType.Movement:
                case Command.CommandType.Poke:
                case Command.CommandType.Rest:
                case Command.CommandType.Shoot:
                case Command.CommandType.Throw:
                    if (!ch.IsImmortal)
                        return;
                    break;
                default:
                    break;

            }

            ch.CommandType = Command.CommandType.Movement;

            // don't kill a character this way
            if (ch.Hits == 1 && ch.Stamina == 0 && Rules.GetEncumbrance(ch) != "lightly")
            {
                ch.WriteToDisplay("You are too exhausted to move.");
                return;
            }

			int i = 0;
			short landnum = ch.CurrentCell.LandID;
			short mapnum = ch.CurrentCell.MapID;
			int xcord = ch.X;
			int ycord = ch.Y;
            int zcord = ch.Z;
			int count = 0;

            string[] dir = new string[] { "", "", "" };

            dir[0] = direction;

            if (ch.RightHand != null && ch.RightHand.nocked)
            {
                if (ch.RightHand.name.IndexOf("crossbow") == -1)
                {
                    ch.RightHand.nocked = false;
                }
            }

            if (args != null)
            {
                string[] sArgs = args.Split(" ".ToCharArray());

                if (sArgs[0] != null)
                {
                    dir[1] = sArgs[0];
                }

                if (sArgs.GetUpperBound(0) > 0)
                {
                    if (sArgs[1] != null)
                    {
                        dir[2] = sArgs[1];
                    }
                }
            }

            if (ch.CurrentCell.DisplayGraphic == "ww")
            {
                if(!Rules.FullStatCheck(ch,"dexterity"))
                {
                    if (ch.IsPC)
                    {
                        ch.WriteToDisplay("You are stuck in a web.");
                    }
                    else
                    {
                        NPC npc = (NPC)ch;
                        npc.MoveList.Clear();
                    }
                    return;
                }
            }

            if ((ch.CurrentCell.DisplayGraphic == "**") && !ch.CanFly)
            {
                if (!ch.immuneFire && !ch.IsImmortal)
                {
                    if (ch.IsPC || Rules.CheckPerception(ch))
                    {
                        if (!Rules.FullStatCheck(ch, "dexterity"))
                        {
                            if (ch.IsPC)
                            {
                                ch.WriteToDisplay("You are confused by the fire!");
                            }
                            else
                            {
                                NPC npc = (NPC)ch;
                                npc.MoveList.Clear();
                            }
                            return;
                        }
                    }
                }
            }

            if ((ch.CurrentCell.DisplayGraphic == "~.") && !ch.CanFly)
            {
                if (!ch.immuneCold && !ch.animal && !ch.IsImmortal)
                {
                    if (!Rules.FullStatCheck(ch, "dexterity"))
                    {
                        if (ch.IsPC)
                        {
                            ch.WriteToDisplay("You have slipped on the ice!");
                        }
                        else
                        {
                            NPC npc = (NPC)ch;
                            npc.MoveList.Clear();
                        }
                        return;
                    }
                }
            }
            
            if (ch.CurrentCell.Effects.ContainsKey(Effect.EffectType.Web) && ch.CurrentCell.Effects[Effect.EffectType.Web].effectAmount > ch.Strength + ch.TempStrength)
            {
                if (!Rules.FullStatCheck(ch, "strength") && ch.species != Globals.eSpecies.Arachnid && !ch.IsImmortal)
                {
                    if (ch.IsPC)
                    {
                        ch.WriteToDisplay("You are stuck in the web!");
                    }
                    else
                    {
                        NPC npc = (NPC)ch;
                        npc.MoveList.Clear();
                    }
                    return;
                }
            }

            //#region Limit movement out of water to one cell
            //if (ch.CurrentCell.DisplayGraphic == "~~" && !ch.CanFly && !ch.IsImmortal)
            //{
            //    i = 2;
            //    dir[2] = dir[0];
            //} 
            //#endregion

            while (i < 3)
            {
                switch (dir[i].ToLower())
                {
                    case "n":
                    case "north":
                        Cell.Move(ch, dir, landnum, mapnum, xcord, ycord -= 1, zcord);
                        ch.dirPointer = "^";
                        count++;
                        checkHiddenStatus(ch);
                        break;
                    case "ne":
                    case "northeast":
                        Cell.Move(ch, dir, landnum, mapnum, xcord += 1, ycord -= 1, zcord);
                        ch.dirPointer = ">";
                        count++;
                        checkHiddenStatus(ch);
                        break;
                    case "e":
                    case "east":
                        Cell.Move(ch, dir, landnum, mapnum, xcord += 1, ycord, zcord);
                        ch.dirPointer = ">";
                        count++;
                        checkHiddenStatus(ch);
                        break;
                    case "se":
                    case "southeast":
                        Cell.Move(ch, dir, landnum, mapnum, xcord += 1, ycord += 1, zcord);
                        ch.dirPointer = ">";
                        count++;
                        checkHiddenStatus(ch);
                        break;
                    case "s":
                    case "south":
                        Cell.Move(ch, dir, landnum, mapnum, xcord, ycord += 1, zcord);
                        ch.dirPointer = "v";
                        count++;
                        checkHiddenStatus(ch);
                        break;
                    case "sw":
                    case "southwest":
                        Cell.Move(ch, dir, landnum, mapnum, xcord -= 1, ycord += 1, zcord);
                        ch.dirPointer = "<";
                        count++;
                        checkHiddenStatus(ch);
                        break;
                    case "w":
                    case "west":
                        Cell.Move(ch, dir, landnum, mapnum, xcord -= 1, ycord, zcord);
                        ch.dirPointer = "<";
                        count++;
                        checkHiddenStatus(ch);
                        break;
                    case "nw":
                    case "northwest":
                        Cell.Move(ch, dir, landnum, mapnum, xcord -= 1, ycord -= 1, ch.Z);
                        ch.dirPointer = "<";
                        count++;
                        checkHiddenStatus(ch);
                        break;
                    case "d":
                    case "down":
                    case "climb down":

                        Segue segue = ch.CurrentCell.Segue;

                        if (segue == null)
                        {
                            segue = Segue.GetDownSegue(ch.CurrentCell);
                        }

                        if (segue != null)
                        {
                            Cell.Move(ch, dir, segue.LandID, segue.MapID, segue.X, segue.Y, segue.Z);
                            Map.checkHiddenStatus(ch);
                        }
                        break;
                    case "u":
                    case "up":
                    case "climb up":

                        segue = ch.CurrentCell.Segue;

                        if (segue == null)
                        {
                            segue = Segue.GetUpSegue(ch.CurrentCell);
                        }

                        if (segue != null)
                        {
                            Cell.Move(ch, dir, segue.LandID, segue.MapID, segue.X, segue.Y, segue.Z);
                            Map.checkHiddenStatus(ch);
                        }
                        break;
                    default:
                        break;
                }
                i++;
            }

			try
			{
                if (ch.IsPC) // descriptions are sent to PCs only
                {
                    #region return description messages sent to players
                    if (ch.CurrentCell.Description != "") // 4/19/2006 discovered this was being sent every move.. was != null instead of != ""
                    {
                        ch.WriteToDisplay(ch.CurrentCell.Description);
                    }
                    if (ch.CurrentCell.IsOneHandClimbUp || ch.CurrentCell.IsTwoHandClimbUp && ch.CurrentCell.Description.Length == 0)
                    {
                        ch.WriteToDisplay("It looks possible to climb up here.");
                    }
                    if (ch.CurrentCell.IsOneHandClimbDown || ch.CurrentCell.IsTwoHandClimbDown && ch.CurrentCell.Description.Length == 0)
                    {
                        ch.WriteToDisplay("It looks possible to climb down here.");
                    }

                    switch (ch.CurrentCell.DisplayGraphic)
                    {
                        case "o ":
                            ch.WriteToDisplay("You are standing next to a garbage can.");
                            break;
                        case "dn":
                            ch.WriteToDisplay("You are standing at the top of stairs leading down.");
                            break;
                        case "up":
                            ch.WriteToDisplay("You are standing at the bottom of stairs leading up.");
                            break;
                        case "**":
                            ch.WriteToDisplay("You are standing in fire.");
                            break;
                        default:
                            break;
                    }

                    if ((ch.CurrentCell.Effects.ContainsKey(Effect.EffectType.Darkness) || ch.CurrentCell.IsAlwaysDark) && !ch.HasNightVision)
                    {
                        ch.WriteToDisplay("You are standing in darkness.");
                    }

                    #endregion
                }
			}
			catch(Exception e)
			{
                Utils.LogException(e);
			}

            #region encumbrance
            string encumbrance = Rules.GetEncumbrance(ch);
            if (count == 1)
            {
                if (encumbrance == "severely")
                {
                    ch.Stamina -= 5;
                    ch.updateStam = true;
                    if (ch.Stamina < 1)
                    {
                        ch.Stamina = 0;
                        if (ch.Hits > 1)
                        {
                            Rules.DoDamage(ch, ch, 5, false);
                        }
                        else
                        {
                            ch.Hits = 1;
                        }
                    }
                }
                if (encumbrance == "heavily")
                {
                    ch.Stamina -= 1;
                    ch.updateStam = true;
                    if (ch.Stamina < 1)
                    {
                        ch.Stamina = 0;
                        if (ch.Hits > 1)
                        {
                            Rules.DoDamage(ch, ch, 1, false);
                        }
                        else
                        {
                            ch.Hits = 1;
                        }
                    }
                }
            }
            if (count == 2)
            {
                if (encumbrance == "severely")
                {
                    ch.Stamina -= 10;
                    ch.updateStam = true;
                    if (ch.Stamina < 1)
                    {
                        ch.Stamina = 0;
                        if (ch.Hits > 1)
                        {
                            Rules.DoDamage(ch, ch, 10, false);
                        }
                        else
                        {
                            ch.Hits = 1;
                        }
                    }
                }

                if (encumbrance == "heavily")
                {
                    ch.Stamina -= 2;
                    ch.updateStam = true;
                    if (ch.Stamina < 1)
                    {
                        ch.Stamina = 0;
                        if (ch.Hits > 2)
                        {
                            Rules.DoDamage(ch, ch, 2, false);
                        }
                        else
                        {
                            ch.Hits = 1;
                        }
                    }
                }

                if (encumbrance == "moderately")
                {
                    ch.Stamina -= 1;
                    ch.updateStam = true;
                    if (ch.Stamina < 1)
                    {
                        ch.Stamina = 0;
                        if (ch.Hits > 1)
                        {
                            Rules.DoDamage(ch, ch, 1, false);
                        }
                        else
                        {
                            ch.Hits = 1;
                        }
                    }
                }
            }
            if (count == 3)
            {
                if (encumbrance == "severely")
                {
                    ch.Stamina -= 15;
                    ch.updateStam = true;
                    if (ch.Stamina < 1)
                    {
                        ch.Stamina = 0;
                        if (ch.Hits > 1)
                        {
                            Rules.DoDamage(ch, ch, 15, false);
                        }
                        else
                        {
                            ch.Hits = 1;
                        }
                    }
                }

                if (encumbrance == "heavily")
                {
                    ch.Stamina -= 3;
                    ch.updateStam = true;
                    if (ch.Stamina < 1)
                    {
                        ch.Stamina = 0;
                        if (ch.Hits > 3)
                        {
                            Rules.DoDamage(ch, ch, 3, false);
                        }
                        else
                        {
                            ch.Hits = 1;
                        }
                    }
                }
                if (encumbrance == "moderately")
                {
                    ch.Stamina -= 2;
                    ch.updateStam = true;
                    if (ch.Stamina < 1)
                    {
                        ch.Stamina = 0;
                        if (ch.Hits > 2)
                        {
                            Rules.DoDamage(ch, ch, 2, false);
                        }
                        else
                        {
                            ch.Hits = 1;
                        }
                    }
                }
            } 
            #endregion
		}

        public static void checkHiddenStatus(Character ch)
        {
            if (ch.IsHidden)
            {
                if (!Map.isCharNextToWall(ch))
                {
                    if (ch.effectList.ContainsKey(Effect.EffectType.Hide_In_Shadows))
                    {
                        if (!ch.effectList[Effect.EffectType.Hide_In_Shadows].IsPermanent)
                        {
                            ch.IsHidden = false;
                        }
                    }
                    
                }
            }
        }

        public static Cell GetCellRelevantToCell(Cell cell, string args, bool includeSpellPathBlocked)
        {
            int argCount = 0;
            int i = 0, x = 0, y = 0;

            args = args.Trim();

            string[] sArgs = args.Split(" ".ToCharArray());

            argCount = sArgs.GetUpperBound(0);

            for (i = 0; i <= argCount; i++)
            {
                switch (sArgs[i].ToLower())
                {
                    case "north":
                    case "n":
                        if (!includeSpellPathBlocked && IsSpellPathBlocked(Cell.GetCell(cell.FacetID, cell.LandID, cell.MapID, cell.X + x, cell.Y + y - 1, cell.Z)))
                        {
                            i = argCount + 1;
                            break;
                        }
                        else
                            y--;
                        break;
                    case "northeast":
                    case "ne":
                        if (!includeSpellPathBlocked && IsSpellPathBlocked(Cell.GetCell(cell.FacetID, cell.LandID, cell.MapID, cell.X + x + 1, cell.Y + y - 1, cell.Z)))
                        {
                            i = argCount + 1;
                            break;
                        }
                        else
                        {
                            y--;
                            x++;
                        }
                        break;
                    case "northwest":
                    case "nw":
                        if (!includeSpellPathBlocked && IsSpellPathBlocked(Cell.GetCell(cell.FacetID, cell.LandID, cell.MapID, cell.X + x - 1, cell.Y + y - 1, cell.Z)))
                        {
                            i = argCount + 1;
                            break;
                        }
                        else
                        {
                            y--;
                            x--;
                        }
                        break;
                    case "west":
                    case "w":
                        if (!includeSpellPathBlocked && IsSpellPathBlocked(Cell.GetCell(cell.FacetID, cell.LandID, cell.MapID, cell.X + x - 1, cell.Y + y, cell.Z)))
                        {
                            i = argCount + 1;
                            break;
                        }
                        else
                            x--;
                        break;
                    case "east":
                    case "e":
                        if (!includeSpellPathBlocked && IsSpellPathBlocked(Cell.GetCell(cell.FacetID, cell.LandID, cell.MapID, cell.X + x + 1, cell.Y + y, cell.Z)))
                        {
                            i = argCount + 1;
                            break;
                        }
                        else
                            x++;
                        break;
                    case "southwest":
                    case "sw":
                        if (!includeSpellPathBlocked && IsSpellPathBlocked(Cell.GetCell(cell.FacetID, cell.LandID, cell.MapID, cell.X + x - 1, cell.Y + y + 1, cell.Z)))
                        {
                            i = argCount + 1;
                            break;
                        }
                        else
                        {
                            y++;
                            x--;
                        }
                        break;
                    case "southeast":
                    case "se":
                        if (!includeSpellPathBlocked && IsSpellPathBlocked(Cell.GetCell(cell.FacetID, cell.LandID, cell.MapID, cell.X + x + 1, cell.Y + y + 1, cell.Z)))
                        {
                            i = argCount + 1;
                            break;
                        }
                        else
                        {
                            y++;
                            x++;
                        }
                        break;
                    case "south":
                    case "s":
                        if (!includeSpellPathBlocked && IsSpellPathBlocked(Cell.GetCell(cell.FacetID, cell.LandID, cell.MapID, cell.X + x, cell.Y + y + 1, cell.Z)))
                        {
                            i = argCount + 1;
                            break;
                        }
                        else
                        {
                            y++;
                        }
                        break;
                    default:
                        break;
                }
            }

            // should never be able to do something off screen
            if (y > 3)
                y = 3;
            if (y < -3)
                y = -3;
            if (x > 3)
                x = 3;
            if (x < -3)
                x = -3;

            // return the cell that is pointed to by the args
            return Cell.GetCell(cell.FacetID, cell.LandID, cell.MapID, cell.X + x, cell.Y + y, cell.Z);
        }

        public static Character FindTargetInCell(Character ch, string targetName)
        {
            int SubStrLen = 0;
            Cell cell = Cell.GetCell(ch.FacetID, ch.LandID, ch.MapID, ch.X, ch.Y, ch.Z);
            // look for the target in the seenlist first.
            if (ch.seenList.Count > 0)
            {
                foreach (Character chr in new List<Character>(ch.seenList))
                {
                    if (chr.Name.Length < targetName.Length)
                        continue;
                    SubStrLen = targetName.Length;
                    if (chr.Name.ToLower().Substring(0, SubStrLen) == targetName.ToLower() && chr != ch && chr.CurrentCell == cell)
                        return chr;
                }
            }
            // look for the character in the charlist of the cell
            if (cell.Characters.Count > 0)
            {
                foreach(Character chr in new List<Character>(cell.Characters))
                {
                    if (chr.Name.Length < targetName.Length)
                        continue;
                    SubStrLen = targetName.Length;

                    // compare the substring against the targetName and see if we find a match
                    if (chr.Name.ToLower().Substring(0, SubStrLen) == targetName.ToLower() && chr != ch && !chr.IsInvisible)
                    {
                        return chr;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Find a target in viw using it's ID.
        /// </summary>
        /// <param name="ch">The character that is attempting to find the target.</param>
        /// <param name="id">Either the player ID or the NPC's world ID.</param>
        /// <param name="includeSelf">True if should include self in search.</param>
        /// <param name="includeHidden">True if should include hidden targets.</param>
        /// <returns></returns>
        public static Character FindTargetInView(Character ch, int id, bool includeSelf, bool includeHidden)
        {
            Cell cell = null;
            int bitcount = 0;
            try
            {
                //loop through all visible cells
                for (int ypos = -3; ypos <= 3; ypos += 1)
                {
                    for (int xpos = -3; xpos <= 3; xpos += 1)
                    {
                        if (ch.CurrentCell.visCells[bitcount])
                        {
                            cell = Cell.GetCell(ch.FacetID, ch.LandID, ch.MapID, ch.X + xpos, ch.Y + ypos, ch.Z);

                            // look for the character in the charlist of the cell
                            foreach (Character chr in new List<Character>(cell.Characters))
                            {
                                // If the found character is the target seeker and not including self, iterate.
                                if (chr == ch && !includeSelf)
                                {
                                    continue;
                                }

                                // PCs looking for a player ID
                                if (chr.IsPC && chr.PlayerID == id)
                                {
                                    if (Rules.DetectHidden(chr, ch) && Rules.DetectInvisible(chr, ch))
                                    {
                                        return chr;
                                    }
                                }

                                // NPC's looking for an NPC world ID
                                else if ((!ch.IsPC && chr.worldNpcID == id) && (ch.Alignment == chr.Alignment))
                                {
                                    // NPCs of the same alignment always know where the other NPC is -- is this fair? - Eb
                                    if (ch.Alignment == chr.Alignment)
                                    {
                                        return chr;
                                    }
                                    // NPCs of differing alignments need to use hiding and invis rules
                                    else if(Rules.DetectHidden(chr, ch) && Rules.DetectInvisible(chr, ch))
                                    {
                                        return chr;
                                    }
                                }
                            }
                        }
                        bitcount++;
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return null;
            }
        }

        public static Character FindTargetInView(Character ch, string targetName, bool includeSelf, bool includeHidden)
        {
            int SubStrLen = 0;
            int bitcount = 0;
            if (ch == null) return null;
            Cell cell = null;
            try
            {
                //loop through all visible cells
                for (int ypos = -3; ypos <= 3; ypos += 1)
                {
                    for (int xpos = -3; xpos <= 3; xpos += 1)
                    {
                        if (ch.CurrentCell.visCells[bitcount])
                        {
                            cell = Cell.GetCell(ch.FacetID, ch.LandID, ch.MapID, ch.X + xpos, ch.Y + ypos, ch.Z);
                            if (cell == null)
                                continue;
                            if (ch.seenList.Count > 0)
                            {
                                foreach (Character chr in new List<Character>(ch.seenList))
                                {
                                    if (chr.Name.Length < targetName.Length)
                                        continue;
                                    SubStrLen = targetName.Length;
                                    if (chr.Name.ToLower().Substring(0, SubStrLen) == targetName.ToLower() && chr != ch)
                                    {
                                        return chr;
                                    }
                                }
                            }
                            // look for the character in the charlist of the cell
                            foreach (Character chr in new List<Character>(cell.Characters))
                            {
                                if (chr.Name.Length < targetName.Length)
                                    continue;

                                SubStrLen = targetName.Length;

                                if ((includeHidden && Rules.DetectHidden(chr, ch)) || (!chr.IsHidden && Rules.DetectInvisible(chr, ch)))
                                {
                                    if (includeSelf)
                                    {
                                        if (chr.Name.ToLower().Substring(0, SubStrLen) == targetName.ToLower())
                                        {
                                            return chr;
                                        }
                                    }
                                    else
                                    {
                                        if (chr.Name.ToLower().Substring(0, SubStrLen) == targetName.ToLower() && chr != ch)
                                        {
                                            return chr;
                                        }
                                    }
                                }

                            }//end foreach
                        }
                        bitcount += 1;
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return null;
            }
        }

        public static Character findTargetInNextCells(Character ch, string targetName)
        {
            int SubStrLen = 0;
            int bitcount = 0;

            Cell curCell = null;

            //loop through all visable cells
            for (int ypos = -1; ypos <= 1; ypos += 1)
            {
                for (int xpos = -1; xpos <= 1; xpos += 1)
                {
                    curCell = Cell.GetCell(ch.FacetID, ch.LandID, ch.MapID, ch.X + xpos, ch.Y + ypos, ch.Z);
                    //Look for the character in the charlist of the cell
                    foreach (Character chr in new List<Character>(curCell.Characters))
                    {
                        if (chr.Name.Length < targetName.Length)
                            continue;

                        SubStrLen = targetName.Length;

                        //compare the substring against the targetName and see if we find a match
                        if (chr.Name.ToLower().Substring(0, SubStrLen) == targetName.ToLower() && chr != ch)
                        {
                            return chr;
                        }
                    }
                    bitcount += 1;
                }

            }
            return null;
        }

        /// <summary>
        /// Finds a target in view.
        /// </summary>
        /// <param name="chr">The character attempting to find a target.</param>
        /// <param name="lookTarget">The target name.</param>
        /// <param name="countTo">The number of targets with the same name to skip through.</param>
        /// <returns></returns>
        public static Character FindTargetInView(Character chr, string lookTarget, int countTo)
        {
            try
            {
                if (chr == null) { return null; }
                int itemcount = 0;
                ArrayList crSeen = new ArrayList();

                Cell[] cellArray = Cell.GetVisibleCellArray(chr.CurrentCell, 3);

                int sublen = lookTarget.Length;

                #region Setup Seen Array

                for (int j = 0; j < 49; j++)
                {
                    if (cellArray[j] == null || chr.CurrentCell.visCells[j] == false)
                    {
                        // do nothing
                    }
                    else
                    {
                        // create array of targets
                        if (cellArray[j].Characters.Count > 0)
                        {
                            foreach (Character ch in new List<Character>(cellArray[j].Characters))
                            {
                                if (ch != chr && !ch.IsDead && Rules.DetectHidden(ch, chr) && Rules.DetectInvisible(ch, chr))
                                {
                                    crSeen.Add(ch);
                                }
                            }
                        }
                    }
                }
                #endregion

                foreach (Character tempchar in crSeen)
                {
                    if (tempchar.Name.ToLower().Substring(0, sublen) == lookTarget.ToLower())
                    {
                        if (countTo - 1 == itemcount)
                        {
                            return tempchar;
                        }
                        itemcount++;
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return null;
            }
        }

		public static Item getItemCopyFromCounter(Character ch, int count, string itemName)
		{
            int a = 0;

			Cell counterCell = Map.GetNearestCounterOrAltarCell(ch);

            foreach (Item item in new List<Item>(counterCell.Items))
            {
                if (item.name.ToLower() == itemName.ToLower())
                {
                    if (++a == count)
                    {
                        return item;
                    }
                }
            }
            return null;
		}

        public static bool isCharNextToWall(Character ch)
        {
            Cell cell = null;

            for (int ypos = -1; ypos <= 1; ypos += 1)
            {
                for (int xpos = -1; xpos <= 1; xpos += 1)
                {
                    cell = Cell.GetCell(ch.FacetID, ch.LandID, ch.MapID, ch.X + xpos, ch.Y + ypos, ch.Z);
                    if (cell.Effects.ContainsKey(Effect.EffectType.Darkness) || cell.IsAlwaysDark)
                    {
                        return true;
                    }
                    else if (cell.CellGraphic == "[]" || cell.DisplayGraphic == "[]" ||
                        cell.CellGraphic == "MM" || cell.DisplayGraphic == "mm" ||
                        cell.CellGraphic == "CC" || cell.DisplayGraphic == "==" ||
                        cell.CellGraphic == "/\\" || cell.DisplayGraphic == "/\\" ||
                        cell.CellGraphic == " @" || cell.DisplayGraphic == " @" ||
                        cell.CellGraphic == "@ " || cell.DisplayGraphic == "@ " ||
                        cell.CellGraphic == "@@" || cell.DisplayGraphic == "@@" ||
                        cell.CellGraphic == "TT" || cell.DisplayGraphic == "TT")
                    {
                        return true;
                    }
                }
            }
            return false;
        }

		public static List<Item> getCopyOfAllItemsFromCounter(Character ch)
		{
			Cell counterCell = Map.GetNearestCounterOrAltarCell(ch);

            List<Item> itemsList = new List<Item>();

            if (counterCell != null)
            {
                Item[] counterItems = new Item[counterCell.Items.Count];
                counterCell.Items.CopyTo(counterItems);
                foreach (Item item in counterItems)
                {
                    itemsList.Add(item);
                }
                return itemsList;
            }
			return null;
		}

        public static Item GetItemFromCounter(Character ch, long worldID)
        {
            Cell counterCell = Map.GetNearestCounterOrAltarCell(ch);

            if (counterCell != null)
            {
                foreach (Item item in new List<Item>(counterCell.Items))
                {
                    if (item.worldItemID == worldID)
                    {
                        counterCell.Remove(item);
                        return item;
                    }
                }
            }
            return null;
        }

		public static Item GetItemFromCounter(Character ch, string itemName)
		{
			Cell counterCell = Map.GetNearestCounterOrAltarCell(ch);

            if (counterCell != null)
            {
                foreach (Item item in new List<Item>(counterCell.Items))
                {
                    if (item.name.ToLower().StartsWith(itemName.ToLower()))
                    {
                        counterCell.Remove(item);
                        return item;
                    }
                }
            }
			return null;
		}

		public static void deleteItemFromCounter(Character ch, string itemName)
		{
			Item itm = Map.GetItemFromCounter(ch, itemName);
			itm = null;
		}

        public static bool nearCounter(Character ch)
        {
            Cell curCell;
            for (int ypos = -1; ypos <= 1; ypos += 1)
            {
                for (int xpos = -1; xpos <= 1; xpos += 1)
                {
                    curCell = Cell.GetCell(ch.FacetID, ch.LandID, ch.MapID, ch.X + xpos, ch.Y + ypos, ch.Z);;
                    if (curCell.CellGraphic == "CC" || curCell.CellGraphic == "MM")
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static Item PutItemOnCounter(Character ch, Item item)
        {
            Cell curCell = null;

            //loop through all visable cells
            for (int ypos = -1; ypos <= 1; ypos += 1)
            {
                for (int xpos = -1; xpos <= 1; xpos += 1)
                {
                    curCell = Cell.GetCell(ch.FacetID, ch.LandID, ch.MapID, ch.X + xpos, ch.Y + ypos, ch.Z);
                    if (curCell.CellGraphic == "CC" || curCell.CellGraphic == "MM")
                    {
                        // coins should accumulate value; avoid redundant coins items
                        if (item.itemType == Globals.eItemType.Coin)
                        {
                            foreach (Item itm in curCell.Items)
                            {
                                if (itm.itemType == Globals.eItemType.Coin)
                                {
                                    itm.coinValue += item.coinValue;
                                    return item;
                                }
                            }
                        }

                        curCell.Add(item);
                        return item;
                    }

                }

            }
            return null;
        }

        public static bool CheckSecretDoor(Cell cell)
        {
            if (cell.IsSecretDoor)
            {
                if (cell.DisplayGraphic == "[]")
                {
                    Effect effect = new Effect(Effect.EffectType.Find_Secret_Door, ". ", 0, 8, null, cell);
                    cell.EmitSound(Sound.GetCommonSound(Sound.CommonSound.OpenDoor));
                    return true;
                }
                else if (cell.DisplayGraphic == "/\\")
                {
                    Effect effect = new Effect(Effect.EffectType.Find_Secret_Rockwall, ". ", 0, 8, null, cell);
                    //TODO: rock sliding sound
                    return true;
                }
            }
            return false;
        }

		public static bool UnlockDoor(Cell cell, string key)
		{
			if(key == cell.cellLock.key)
			{
                if (cell.IsLockedHorizontalDoor)
                {
                    Effect effect = new Effect(Effect.EffectType.Unlocked_Horizontal_Door, "\\ ", 0, 5, null, cell);
                    //World.GetFacetByIndex(0).GetLandByID(cell.land).GetMapByID(cell.map).updateVisionCellVisible(cell);
                    return true;
                }
                else if (cell.IsLockedVerticalDoor)
                {
                    Effect effect = new Effect(Effect.EffectType.Unlocked_Vertical_Door, "/ ", 0, 5, null, cell);
                    //World.GetFacetByIndex(0).GetLandByID(cell.land).GetMapByID(cell.map).updateVisionCellVisible(cell);
                    return true;
                }
			}
			return false;
		}

		public static bool LockDoor(Cell cell, string key)
		{
            if (key == cell.cellLock.key)
            {
                if (cell.Effects.ContainsKey(Effect.EffectType.Unlocked_Horizontal_Door))
                {
                    cell.Effects[Effect.EffectType.Unlocked_Horizontal_Door].StopAreaEffect();
                    return true;
                }
                else if (cell.Effects.ContainsKey(Effect.EffectType.Unlocked_Vertical_Door))
                {
                    cell.Effects[Effect.EffectType.Unlocked_Vertical_Door].StopAreaEffect();
                    return true;
                }
            }
            return false;
        }

        public static Cell GetNearestCounterOrAltarCell(Character ch)
        {
            for (int ypos = -1; ypos <= 1; ypos += 1)
            {
                for (int xpos = -1; xpos <= 1; xpos += 1)
                {
                    Cell counterCell = Cell.GetCell(ch.FacetID, ch.LandID, ch.MapID, ch.X + xpos, ch.Y + ypos, ch.Z);
                    if (counterCell.CellGraphic == "CC" || counterCell.CellGraphic == "MM")
                    {
                        return counterCell;
                    }
                }
            }
            return null;
        }

        public static List<Cell> getAdjacentCells(Cell cell, Character ch, int radius, Effect.EffectType effectType)
        {
            List<Cell> cellList = new List<Cell>();
            for (int ypos = -radius; ypos <= radius; ypos++)
            {
                for (int xpos = -radius; xpos <= radius; xpos++)
                {
                    Cell newcell = Cell.GetCell(cell.FacetID, cell.LandID, cell.MapID, cell.X + xpos, cell.Y + ypos, cell.Z);
                    if (newcell != null)
                    {
                        if(newcell.Effects.ContainsKey(effectType))
                        {
                            cellList.Add(newcell);
                        }
                    }
                }
            }
            if (cellList.Count > 0)
            {
                return cellList;
            }
            else
            {
                return null;
            }
        }
        public static List<Cell> getAdjacentCells(Cell cell, int radius)
        {
            List<Cell> cellList = new List<Cell>();
            for (int ypos = -radius; ypos <= radius; ypos++)
            {
                for (int xpos = -radius; xpos <= radius; xpos++)
                {
                    Cell newcell = Cell.GetCell(cell.FacetID, cell.LandID, cell.MapID, cell.X + xpos, cell.Y + ypos, cell.Z);
                    if (newcell != null)
                    {
                        if (!IsSpellPathBlocked(newcell))
                        {
                            cellList.Add(newcell);
                        }
                    }
                }
            }
            if (cellList.Count > 0)
            {
                return cellList;
            }
            else
            {
                return null;
            }
        }
        public static List<Cell> GetAdjacentCells(Cell cell, NPC npc)
        {
            try
            {
                List<Cell> cellList = new List<Cell>();
                for (int ypos = -1; ypos <= 1; ypos++)
                {
                    for (int xpos = -1; xpos <= 1; xpos++)
                    {
                        Cell newcell = Cell.GetCell(cell.FacetID, cell.LandID, cell.MapID, cell.X + xpos, cell.Y + ypos, cell.Z);
                        if (newcell != null)
                        {
                            if (npc.GetCellCost(newcell) <= 2)
                            {
                                cellList.Add(newcell);
                            }
                        }
                    }
                }
                if (cellList.Count > 0)
                {
                    return cellList;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                Utils.Log("Error in NPC GetAdjacentCells - Name: " +npc.GetLogString(), Utils.LogType.SystemFailure);
                Utils.LogException(e);
                return null;
            }
        }
        public static List<Cell> GetAdjacentCells(Cell cell)
        {
            List<Cell> cellList = new List<Cell>();
            for (int ypos = -1; ypos <= 1; ypos++)
            {
                for (int xpos = -1; xpos <= 1; xpos++)
                {
                    Cell newcell = Cell.GetCell(cell.FacetID, cell.LandID, cell.MapID, cell.X + xpos, cell.Y + ypos, cell.Z);
                    if (newcell != null)
                    {
                        if (!IsSpellPathBlocked(newcell))
                        {
                            cellList.Add(newcell);
                        }
                    }
                }
            }
            if (cellList.Count > 0)
            {
                return cellList;
            }
            else
            {
                return null;
            }
        }

        public string GetZName(int zCord)
        {
            if (zNames.ContainsKey(zCord))
                return zNames[zCord];

            return "";
        }

	}
}
